package pcc.addon.crops.util;

import java.util.ArrayList;

public class MultiBlockData {

	private String worldName;
	private int dimID;
	private int x;
	private int y;
	private int z;
	private ArrayList<String> others = new ArrayList<String>();

	public MultiBlockData(String worldName, int dimID, int x, int y, int z) {
		super();
		this.worldName = worldName;
		this.dimID = dimID;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void setWorldName(String worldName) {
		this.worldName = worldName;
	}

	public void setDimID(int dimID) {
		this.dimID = dimID;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public String getWorldName() {
		return worldName;
	}

	public int getDimID() {
		return this.dimID;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public ArrayList<String> getOthers() {
		return others;
	}

	public void add(String data) {
		this.others.add(data);
	}

	public void addAll(ArrayList<String> datas) {
		this.others = datas;
	}

	@Override
	public boolean equals(Object obj) {
		MultiBlockData p = (MultiBlockData) obj;
		if (p.getWorldName().equals(this.worldName)
				&& p.getDimID() == this.dimID
				&& p.getX() == this.x
				&& p.getY() == this.y
				&& p.getZ() == this.z) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return x*100 + y*200 + z*300;
	}

}
