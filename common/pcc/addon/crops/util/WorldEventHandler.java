package pcc.addon.crops.util;

import java.io.IOException;

import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.minecraftforge.event.world.WorldEvent.Save;
import pcc.addon.crops.ChemicalCrops;

public class WorldEventHandler {

	@ForgeSubscribe
	public void event(Load event) {
		try {
			ChemicalCrops.getDataStream().createDataInputStream();
			ChemicalCrops.getDataStream().createDataOutputStream();
			ChemicalCrops.getDataStream().read();
			ChemicalCrops.instance.startAutoSave();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@ForgeSubscribe
	public void event(Save event) {
		try {
			ChemicalCrops.instance.stopAutoSave();
			ChemicalCrops.getDataStream().write();
			ChemicalCrops.getDataStream().clearPlantDataHash();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
