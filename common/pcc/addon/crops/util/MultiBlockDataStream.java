package pcc.addon.crops.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class MultiBlockDataStream {

	private String dirPath;
	private String filePath;

	private BufferedReader input;
	private BufferedWriter output;

	private File file;

	private HashSet<MultiBlockData> plantDataHash = new HashSet<MultiBlockData>();

	public MultiBlockDataStream(String dirPath, String filePath) {
		this.dirPath = dirPath;
		this.filePath = filePath;
	}

	public void createDataInputStream() throws IOException {
		this.file = new File(this.dirPath + this.filePath);
		if (!this.file.exists()) {
			this.file.createNewFile();
		}

		if (this.file.canRead()) {
			if (this.file.canWrite()) {
				this.input = new BufferedReader(
						new FileReader(this.file)
						);
			} else {
				throw new IOException("You don't have Write Permission.");
			}
		} else {
			throw new IOException("You don't have Read Permission.");
		}
	}

	public void createDataOutputStream() throws IOException {
		this.file = new File(this.dirPath + this.filePath);
		if (!this.file.exists()) {
			this.file.createNewFile();
		}

		if (this.file.canRead()) {
			if (this.file.canWrite()) {
				this.output = new BufferedWriter(
						new FileWriter(this.file)
						);
			} else {
				throw new IOException("You don't have Write Permission.");
			}
		} else {
			throw new IOException("You don't have Read Permission.");
		}
	}

	public void read() throws IOException {
		String readData = null;
		while ((readData = this.input.readLine()) != null) {
			String[] datas = readData.split("#");
			if (datas.length < 6) {
				continue;
			}
			String worldName = datas[0];
			int dimID = Integer.parseInt(datas[1]);
			int x = Integer.parseInt(datas[2]);
			int y = Integer.parseInt(datas[3]);
			int z = Integer.parseInt(datas[4]);
			MultiBlockData p = new MultiBlockData(worldName, dimID, x, y, z);
			for (int i = 0; i < datas.length - 5; i++) {
				p.add(datas[7 + i]);
			}
			this.plantDataHash.add(p);
		}
		this.input.close();
	}

	public void write() throws IOException {
		try {
			for (MultiBlockData p: this.plantDataHash) {
				String result = "";
				String worldName = p.getWorldName();
				int dimID = p.getDimID();
				int x = p.getX();
				int y = p.getY();
				int z = p.getZ();
				ArrayList<String> others = p.getOthers();

				result = result + worldName + "#";
				result = result + dimID + "#";
				result = result + x + "#";
				result = result + y + "#";
				result = result + z + "#";
				for (int i = 0; i < others.size(); i++) {
					result = result + others.get(i) + "#";
				}
				this.output.newLine();
				this.output.write(result);
				this.output.flush();
			}
		} catch (IOException e) {
		}
		this.output.close();
	}

	public String get(String worldName, int dimID, int x, int y, int z, int dataID) {
		for (MultiBlockData p: this.plantDataHash) {
			MultiBlockData newPlantData = new MultiBlockData(worldName, dimID, x, y, z);
			if (p.equals(newPlantData)) {
				return p.getOthers().get(dataID);
			}
		}
		System.out.println("Data not found");
		return null;
	}

	public void set(String worldName, int dimID, int x, int y, int z, String data) {
		for (MultiBlockData p: this.plantDataHash) {
			MultiBlockData newPlantData = new MultiBlockData(worldName, dimID, x, y, z);
			if (p.equals(newPlantData)) {
				p.add(data);
				return;
			}
		}
		MultiBlockData newPlantData = new MultiBlockData(worldName, dimID, x, y, z);
		newPlantData.add(data);
		this.plantDataHash.add(newPlantData);
	}

	public void remove(String worldName, int dimID, int x, int y, int z) {
		for (MultiBlockData p: this.plantDataHash) {
			MultiBlockData newPlantData = new MultiBlockData(worldName, dimID, x, y, z);
			if (p.equals(newPlantData)) {
				this.plantDataHash.remove(p);
				return;
			}
		}
	}

	public void clearPlantDataHash() {
		this.plantDataHash.clear();
	}

}
