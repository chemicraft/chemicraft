package pcc.addon.crops.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import pcc.addon.crops.ChemicalCrops;

public class BlockChemicalCrops1 extends BlockFlower implements IPlantable {

	private int plantID;
	private int plantMetadata;
	private Icon[] iconList;

	public BlockChemicalCrops1(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setCreativeTab(CreativeTabs.tabBlock);
		float f = 0.5F;
		this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
		this.setTickRandomly(true);
		this.setHardness(0.0F);
		this.disableStats();
	}

	@Override
	public boolean canBlockStay(World par1World, int par2, int par3, int par4) {
		if (par1World.getBlockId(par2, par3 - 1, par4) == Block.tilledField.blockID) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4,
			int par5, int par6) {
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		ChemicalCrops.getDataStream().remove(par1World.getWorldInfo().getWorldName(), par1World.getWorldInfo().getDimension(), par2, par3, par4);
	}

	@Override
	public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random) {
		super.updateTick(par1World, par2, par3, par4, par5Random);
		final int meta = par1World.getBlockMetadata(par2, par3, par4);
		if (meta < 7) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, meta+1, 0x02);
		} else if (meta > 7 && meta < 15) {
			par1World.setBlockMetadataWithNotify(par2, par3, par4, meta+1, 0x02);
		}
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister) {
		this.iconList = new Icon[16];
		for (int i = 0; i < 8; i++) {
			this.iconList[i] = par1IconRegister.registerIcon(ChemicalCrops.TEXTURE + "Corn" + i);
		}
		for (int i = 8; i < 16; i++) {
			this.iconList[i] = par1IconRegister.registerIcon(ChemicalCrops.TEXTURE + "Tomato" + (i - 8));
		}
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int par1, int par2) {
		return this.iconList[par2];
	}

	@Override
	public int getRenderType() {
		return 6;
	}

    @Override
    public EnumPlantType getPlantType(World world, int x, int y, int z) {
    	return EnumPlantType.Crop;
    }


}
