package pcc.addon.crops;

import java.io.IOException;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import pcc.addon.crops.blocks.BlockChemicalCrops1;
import pcc.addon.crops.util.MultiBlockDataStream;
import pcc.addon.crops.util.WorldEventHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid="ChemiCraftCrops", name="ChemicalCrops(ChemiCraft-Addon)", version="v1.0.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class ChemicalCrops {

	@Instance("ChemiCraftCrops")
	public static ChemicalCrops instance;

	@SidedProxy(clientSide = "pcc.addon.crops.ClientProxy", serverSide = "pcc.addon.crops.CommonProxy")
	public static CommonProxy proxy;

	/**
	 * CropsのBlockID
	 */
	public int crops1ID;

	/**
	 * ChemicalCropsの変数。<br>
	 * ここに入るインスタンスはBlockChemicalCropsと保証されます。
	 */
	private Block blockCrops1;

	/**
	 * プラントデータ書き込み&読み込みのストリーム
	 */
	private static MultiBlockDataStream dataStream;

	/**
	 * Texture Domain.
	 */
	public static final String TEXTURE = "ChemicalCrops:";

	/**
	 * プラントデータ自動セーブ用のスレッド
	 */
	private Thread thread;

	/**
	 * Minecraftのディレクトリパス
	 */
	private static String MINECRAFT_DIR;

	public ChemicalCrops() {
		this.thread = new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						dataStream.createDataOutputStream();
						dataStream.write();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

	}

	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
		this.MINECRAFT_DIR = event.getModConfigurationDirectory().getAbsolutePath().substring(0, event.getModConfigurationDirectory().getAbsolutePath().length()-6) + "saves/";
		this.dataStream = new MultiBlockDataStream(MINECRAFT_DIR, "ChemicalCrops.dat");

		Configuration cfg = new Configuration(event.getSuggestedConfigurationFile());
		cfg.load();
		this.crops1ID = cfg.getBlock("CropsID", 2700).getInt();
		cfg.save();
	}

	@Init
	public void init(FMLInitializationEvent event) {
		//イベント登録
		MinecraftForge.EVENT_BUS.register(new WorldEventHandler());

		//Blockのインスタンス作成
		this.blockCrops1 = new BlockChemicalCrops1(this.crops1ID, Material.plants).setUnlocalizedName("BlockCrops");

		//Minecraftに登録
		GameRegistry.registerBlock(this.blockCrops1, "BlockCrops");
	}

	/**
	 * データストリームを返します
	 * @return データストリーム
	 */
	public static MultiBlockDataStream getDataStream() {
		return dataStream;
	}

	/**
	 * プラントデータ自動セーブを開始します。<br>
	 * すでに停止している場合はNullPointerExceptionをスローします。
	 */
	public void startAutoSave() {
		try {
			this.thread.start();
		} catch (IllegalThreadStateException e) {
		}
	}

	/**
	 * プラントデータ自動セーブを停止します。<br>
	 * 開始していない、もしくはすでに停止している場合はNullPointerExceptionをスローします。
	 */
	public void stopAutoSave() {
		this.thread.stop();
	}

}
