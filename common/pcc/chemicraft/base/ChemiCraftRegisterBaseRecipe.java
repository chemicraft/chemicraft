package pcc.chemicraft.base;

import java.util.Iterator;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import pcc.chemicraft.ChemiCraftData;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.nbt.NBTRecipeGrenade;
import pcc.chemicraft.util.Formula;

/**
 * レシピを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterBaseRecipe extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterBaseRecipe(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {

		//化合物のレシピ
		this.mod.api.addChemicalCombinationRecipe(
				new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, this.mod.api.getCompound("SodiumChloride")),
				new Formula("NaOH"));

		this.mod.api.addChemicalCombinationRecipe(
				new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, this.mod.api.getCompound("ChlorousAcid")),
				new Formula("HO2Cl"));

		//既存物のレシピ
		this.mod.api.addChemicalCombinationRecipe(new ItemStack(Item.diamond),
				new Formula("C4096Si64"));

		this.mod.api.addChemicalCombinationRecipe(new ItemStack(Item.sugar),
				new Formula("C12H22O11"));

		this.mod.api.addReversibleOfElectrolysis(new ItemStack(Item.bucketWater),
				new Formula("H2O"));

		this.mod.api.addReversibleOfElectrolysis(new ItemStack(Item.potion, 1, 0),
				new Formula("H2O"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Item.coal),
				new Formula("C64"));

		this.mod.api.addPyrolysisDecompositionRecipe(new ItemStack(Block.oreCoal),
				new Formula("C"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Item.ingotIron),
				new Formula("Fe"));

		this.mod.api.addPyrolysisDecompositionRecipe(new ItemStack(Block.oreIron),
				new Formula("Fe"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.blockSteel),
				new Formula("9Fe"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Item.ingotGold),
				new Formula("Au"));

		this.mod.api.addPyrolysisDecompositionRecipe(new ItemStack(Block.oreGold),
				new Formula("Au"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.blockGold),
				new Formula("9Au"));

		this.mod.api.addReversibleOfElectrolysis(new ItemStack(Block.wood),
				new Formula("C6H10O5"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.dirt),
				new Formula("SiI2O2"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.glass),
				new Formula("SiCO2"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.stone),
				new Formula("SiO2"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Block.tnt),
				new Formula("C7H5N3O6"));

		this.mod.api.addReversibleOfPyrolysis(new ItemStack(Item.emerald),
				new Formula("Be3Al2Si6O18"));

		this.mod.api.addPyrolysisDecompositionRecipe(new ItemStack(Block.oreEmerald),
				new Formula("Be3Al2Si6O18"));



		//素材制作台のレシピ
		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						null, new ItemStack(Block.stone), null,
						new ItemStack(Block.stone), new ItemStack(Item.gunpowder), new ItemStack(Block.stone),
						null, new ItemStack(Block.stone), null
				},
				new ItemStack(this.mod.itemAtomGrenade, 16, 0),
				new NBTRecipeGrenade());

		this.mod.api.addSharplessMaterialRecipe(
				new ItemStack[] {
						new ItemStack(this.mod.itemAtomGrenade, 1, 0),
				},
				new ItemStack(this.mod.itemAtomGrenade, 1, 0),
				new NBTRecipeGrenade()
				);

		this.mod.api.addSharplessMaterialRecipe(
				new ItemStack[] {
						new ItemStack(Item.dyePowder, 1, 0),
						new ItemStack(Item.egg),
				},
				new ItemStack(this.mod.itemBlackSmoke),
				null
				);

		this.mod.api.addSharplessMaterialRecipe(
				new ItemStack[] {
						new ItemStack(Item.gunpowder),
						new ItemStack(Item.egg),
				},
				new ItemStack(this.mod.itemDust),
				null
				);

		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("Nb")),
						new ItemStack(Item.ingotIron),
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("Nb")),
						new ItemStack(Item.ingotIron),
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("Nb")),
						new ItemStack(Item.ingotIron),
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("He")),
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("Sn")),
						new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, this.mod.chemicalData.toAtoms("He")),
				},
				new ItemStack(this.mod.itemOreSearcher),
				null);

		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						new ItemStack(Block.stone), new ItemStack(Item.ingotIron), new ItemStack(Block.stone),
						new ItemStack(Item.ingotIron), new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.URANIUM), new ItemStack(Item.ingotIron),
						new ItemStack(Block.stone), new ItemStack(Item.redstone), new ItemStack(Block.stone)
				},
				new ItemStack(this.mod.itemRadiationBallet),
				null);

		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						new ItemStack(Block.stone), new ItemStack(Item.ingotIron), new ItemStack(Block.stone),
						new ItemStack(Item.ingotIron), new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.URANIUM), new ItemStack(Item.ingotIron),
						new ItemStack(Block.stone), new ItemStack(Item.redstone), new ItemStack(Block.stone)
				},
				new ItemStack(this.mod.itemRadiationBallet),
				null);

		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						new ItemStack(Block.stone), new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.URANIUM), new ItemStack(Block.stone),
						new ItemStack(Item.ingotIron), new ItemStack(Item.redstone), new ItemStack(Item.ingotIron),
						new ItemStack(Block.stone), new ItemStack(Item.redstone), new ItemStack(Block.stone)
				},
				new ItemStack(this.mod.itemRaditionGunDataChip),
				null);

		this.mod.api.addMaterialRecipe(
				new ItemStack[]{
						new ItemStack(Block.blockSteel), new ItemStack(Block.blockSteel), new ItemStack(this.mod.itemRaditionGunDataChip),
						null, null, new ItemStack(Block.stone),
						null, null, new ItemStack(Item.stick)
				},
				new ItemStack(this.mod.itemRadiationGun),
				null);

		//分解台のレシピ
		this.mod.api.addElectrolysisDecompositionRecipe(
				new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, this.mod.api.getCompound("Water")),
				new Formula("H2O"));

		//鉱石分解の追加
		Iterator<String> arrayOreNames = this.mod.apiBase.getAtomOresFormulas().keySet().iterator();
		for (int i = 0; arrayOreNames.hasNext(); i++) {
			String name = arrayOreNames.next();
			ItemStack ingot;
			ItemStack ore;
			try {
				ingot = OreDictionary.getOres("ingot" + name).get(0);
				ore = OreDictionary.getOres("ore" + name).get(0);
			} catch (IndexOutOfBoundsException e) {
				int var1 = this.mod.apiBase.getAtomOresAtomName().indexOf(name);
				ingot = new ItemStack(this.mod.itemAtomIngots, 1, i);
				ore = new ItemStack(this.mod.blockAtomOres[var1 / 16], 1, var1 % 16);
			}
			this.mod.api.addPyrolysisDecompositionRecipe(
					ingot,
					this.mod.apiBase.getAtomOresFormulas().get(name));
			this.mod.api.addPyrolysisDecompositionRecipe(
					ore,
					this.mod.apiBase.getAtomOresFormulas().get(name));
		}
	}

}
