package pcc.chemicraft.base.client;

import pcc.chemicraft.base.entity.EntityAtomsGrenade;
import pcc.chemicraft.base.entity.EntityDust;
import pcc.chemicraft.base.render.RenderAtomsGrenade;
import pcc.chemicraft.base.render.RenderDust;
import pcc.chemicraft.base.system.CommonProxy;
import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * クライアントプロキシを設定するクラスです
 * @author mozipi,ponkotate,りりごん
 */
public class ClientProxy extends CommonProxy {

	@Override
	public void registerTextures() {

	}

	@Override
	public void registerRenderInformation() {
		RenderingRegistry.registerEntityRenderingHandler(EntityAtomsGrenade.class, new RenderAtomsGrenade(0.5F));
		RenderingRegistry.registerEntityRenderingHandler(EntityDust.class, new RenderDust());
	}
}
