package pcc.chemicraft.base;

/**
 * 化合物を追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterCompounds extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterCompounds(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		this.mod.api.addCompound("ja_JP", "ChlorousAcid", "亜塩素酸");
		this.mod.api.addCompound("ja_JP", "CarbonMonoxide", "一酸化炭素");
		this.mod.api.addCompound("ja_JP", "CarbonDioxide", "二酸化炭素");
		this.mod.api.addCompound("ja_JP", "Water", "水");
		this.mod.api.addCompound("ja_JP", "SodiumChloride", "塩化ナトリウム");
		this.mod.api.addCompound("ja_JP", "SodiumBicarbonate", "重曹");
	}

}
