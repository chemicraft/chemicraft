package pcc.chemicraft.base;

import pcc.chemicraft.ChemiCraftRegister;

/**
 * ChemiCraftBaseの追加の親クラスです
 * @author mozipi,ponkotate
 */
public abstract class ChemiCraftBaseRegister implements ChemiCraftRegister {

	/**
	 * MODのインスタンス
	 */
	protected ChemiCraftBase mod;

	public ChemiCraftBaseRegister(ChemiCraftBase mod) {
		this.mod = mod;
	}

}
