package pcc.chemicraft.base;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.base.item.ItemAtomsGrenade;
import pcc.chemicraft.base.item.ItemBlackSmoke;
import pcc.chemicraft.base.item.ItemDust;
import pcc.chemicraft.base.item.ItemOreSerarcher;
import pcc.chemicraft.base.item.ItemRadiationBullet;
import pcc.chemicraft.base.item.ItemRadiationGun;
import pcc.chemicraft.base.ore.ItemAtomIngots;

/**
 * アイテムを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterItem extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterItem(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		//アイテムを変数に代入
		this.mod.itemAtomIngots = new ItemAtomIngots(this.mod.atomIngotsID).setUnlocalizedName("atomIngots");
		this.mod.itemAtomGrenade = new ItemAtomsGrenade(this.mod.atomGrenadeID).setUnlocalizedName("AtomsGrenade");
		this.mod.itemBlackSmoke = new ItemBlackSmoke(this.mod.blackSmokeID).setUnlocalizedName("BlackSmoke");
		this.mod.itemDust = new ItemDust(this.mod.dustID).setUnlocalizedName("dust");
		this.mod.itemOreSearcher = new ItemOreSerarcher(this.mod.oreSerarcherID).setUnlocalizedName("OreSearcher");
		this.mod.itemRadiationGun = new ItemRadiationGun(this.mod.radiationGunID).setUnlocalizedName("RadiationGun");
		this.mod.itemRadiationBallet = new ItemRadiationBullet(this.mod.radiationBalletID).setUnlocalizedName("RadiationBullet");
		this.mod.itemRaditionGunDataChip = new Item(this.mod.raditionGunDataChipID) {
			@Override
			public void updateIcons(IconRegister par1IconRegister) {
				this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "RaditionGunDataChip");
			}
		}.setUnlocalizedName("RaditionGunDataChip");

		//名前を登録&Minecraftに登録
		this.mod.nameAuxiliary.addName(this.mod.itemAtomGrenade, "AtomGrenade");
		this.mod.nameAuxiliary.addName(this.mod.itemAtomGrenade, "ja_JP", "元素手榴弾");
		this.mod.nameAuxiliary.addName(this.mod.itemBlackSmoke, "BlackSmoke");
		this.mod.nameAuxiliary.addName(this.mod.itemBlackSmoke, "ja_JP", "黒煙");
		this.mod.nameAuxiliary.addName(this.mod.itemDust, "dust");
		this.mod.nameAuxiliary.addName(this.mod.itemDust, "ja_JP", "粉塵");
		this.mod.nameAuxiliary.addName(this.mod.itemOreSearcher, "OreSearcher");
		this.mod.nameAuxiliary.addName(this.mod.itemOreSearcher, "ja_JP", "鉱石情報探知機");
		this.mod.nameAuxiliary.addName(this.mod.itemRadiationGun, "RadiationGun");
		this.mod.nameAuxiliary.addName(this.mod.itemRadiationGun, "ja_JP", "放射線銃");
		this.mod.nameAuxiliary.addName(this.mod.itemRadiationBallet, "RadiationBullet");
		this.mod.nameAuxiliary.addName(this.mod.itemRadiationBallet, "ja_JP", "放射線弾");
		this.mod.nameAuxiliary.addName(this.mod.itemRaditionGunDataChip, "RaditionGunDataChip");
		this.mod.nameAuxiliary.addName(this.mod.itemRaditionGunDataChip, "ja_JP", "放射線銃データチップ");
	}

}
