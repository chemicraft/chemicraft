package pcc.chemicraft.base.gen;

/**
 * 鉱石の出現頻度のEnumです。
 * @author mozipi
 */
public enum EnumOreSpawnFrequency {

	HIGH(8, 10, 256),
	NORMAL(8, 5, 64),
	LOW(8, 3, 30),
	RARE(8, 1, 15);

	/**
	 * 鉱石の塊の大きさ
	 */
	private short size;

	/**
	 * 鉱石の生成頻度
	 */
	private short frequency;

	/**
	 * 鉱石の生成最高高度
	 */
	private short posY;

	/**
	 * @param par1 鉱石の生成頻度
	 */
	private EnumOreSpawnFrequency(int par1, int par2, int par3) {
		this.size = (short) par1;
		this.frequency = (short) par2;
		this.posY = (short) par3;
	}

	/**
	 * 鉱石の塊の大きさを返します
	 * @return 鉱石の塊の大きさ
	 */
	public short getSize() {
		return this.size;
	}

	/**
	 * 鉱石の生成頻度を返します
	 * @return 鉱石の生成頻度
	 */
	public short getFrequency() {
		return this.frequency;
	}

	/**
	 * 鉱石の生成最高高度を変えします
	 * @return 鉱石の生成最高高度
	 */
	public short getPosY() {
		return this.posY;
	}


}
