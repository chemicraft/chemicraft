package pcc.chemicraft.base.gen;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

/**
 * 鉱石生成のインスタンスです
 * @author mozipi,ponkotate,つやぴん
 */
public class WorldGenAtomOres extends WorldGenMinable implements IWorldGenerator {

	/**
	 * 生成率
	 */
	private int frequency;

	/**
	 * 鉱石生成の高度
	 */
	private int posY;

	public WorldGenAtomOres(int id, int meta, int size, int frequency, int posY) {
		super(id, meta, size, Block.stone.blockID);
		this.frequency = frequency;
		this.posY = posY;
	}

	public WorldGenAtomOres(int id, int meta, EnumOreSpawnFrequency frequency) {
		this(id, meta, frequency.getSize(), frequency.getFrequency(), frequency.getPosY());
	}

	@Override
	public void generate(Random par1Random, int par2ChunkX, int par3ChunkZ, World par4World, IChunkProvider par5ChunkGenerator, IChunkProvider par6ChunkProvider) {
		for (int i = 0; i < this.frequency; i++) {
			this.generate(par4World, par1Random, par2ChunkX * 16 + par1Random.nextInt(16), par1Random.nextInt(this.posY), par3ChunkZ * 16 + par1Random.nextInt(16));
		}
	}

}
