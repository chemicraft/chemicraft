package pcc.chemicraft.base.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.core.ChemiCraftCore;

/**
 * 放射線中の弾です
 * @author mozipi
 */
public class ItemRadiationBullet extends Item {

	public ItemRadiationBullet(int par1) {
		super(par1);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
		this.maxStackSize = 16;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "radiation_bullet");
	}
}
