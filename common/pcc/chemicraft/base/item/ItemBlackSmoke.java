package pcc.chemicraft.base.item;

import java.util.HashMap;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.base.entity.EntityBlackSmoke;
import pcc.chemicraft.core.ChemiCraftCore;

/**
 * なんか黒い煙のアイテムです。
 * @author ponkotate
 */
public class ItemBlackSmoke extends Item {

	public HashMap<EntityMob, Integer> invisibilityMobsMap = new HashMap<EntityMob, Integer>();

	public ItemBlackSmoke(int par1) {
		super(par1);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
		double var11 = par4 + 0.5D;
		double var12 = par5;
		double var13 = par6 + 0.5D;
		switch(par7) {
			case 0:
				var12--;
				break;
			case 1:
				var12++;
				break;
			case 2:
				var13--;
				break;
			case 3:
				var13++;
				break;
			case 4:
				var11--;
				break;
			case 5:
				var11++;
				break;
		}
		par3World.spawnEntityInWorld(new EntityBlackSmoke(par2EntityPlayer, par3World, var11, var12, var13));
		return true;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "black_smoke");
	}

}
