package pcc.chemicraft.base.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.base.entity.EntityAtomsGrenade;

/**
 * 手榴弾のアイテムです。
 * @author mozipi
 */
public class ItemAtomsGrenade extends Item {

	public ItemAtomsGrenade(int par1) {
		super(par1);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		if (par1ItemStack.stackSize <= 0) {
			return null;
		}

		boolean var1 = false;
		boolean var2 = false;
		boolean var3 = false;

		if (par1ItemStack.stackTagCompound != null) {
			NBTTagList var4 = par1ItemStack.getTagCompound().getTagList("Effect");

			for (int i = 0;i < 3;i++) {
				if (!((NBTTagCompound)var4.tagAt(0)).getString("GrenadeEffect" + i).equals("")) {
					if (((NBTTagCompound)var4.tagAt(0)).getString("GrenadeEffect" + i).equals("Hydrogen")) {
						var1 = true;
					}
					if (((NBTTagCompound)var4.tagAt(0)).getString("GrenadeEffect" + i).equals("Carbon")) {
						var2 = true;
					}
					if (((NBTTagCompound)var4.tagAt(0)).getString("GrenadeEffect" + i).equals("Uranium")) {
						var3 = true;
					}
				}
			}
		}
		if (!par2World.isRemote) {
			EntityAtomsGrenade entity = new EntityAtomsGrenade(par2World, par3EntityPlayer, var1, var2, var3);
			par2World.spawnEntityInWorld(entity);
		}
		par1ItemStack.stackSize--;
		return par1ItemStack;
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		if (par1ItemStack.hasTagCompound()) {
			NBTTagList var1 = par1ItemStack.getTagCompound().getTagList("Effect");

			for (int i = 0;i < 3;i++) {
				if (!((NBTTagCompound)var1.tagAt(0)).getString("GrenadeEffect" + i).equals("")) {
					par3List.add(
							((NBTTagCompound)var1.tagAt(0)).getString("GrenadeEffect" + i)
							);
				}
			}
		}
	}

	@Override
	public boolean getShareTag()
	{
		return true;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "atoms_grenade");
	}

}
