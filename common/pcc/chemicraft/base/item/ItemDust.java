package pcc.chemicraft.base.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.base.entity.EntityDust;
import pcc.chemicraft.core.ChemiCraftCore;

/**
 * 粉塵のアイテムです
 * @author mozipi
 */
public class ItemDust extends Item {

	public ItemDust(int par1) {
		super(par1);
		this.setCreativeTab(ChemiCraftCore.instance.creativeTabChemiCraft);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {


		if (!par2World.isRemote) {
			par2World.spawnEntityInWorld(new EntityDust(par2World,
					par3EntityPlayer.posX,
					par3EntityPlayer.posY,
					par3EntityPlayer.posZ));
			par1ItemStack.stackSize--;
		}
		return par1ItemStack;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon("egg");
	}

}
