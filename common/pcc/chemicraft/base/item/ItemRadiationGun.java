package pcc.chemicraft.base.item;

import java.util.ArrayList;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.base.ChemiCraftBase;
import pcc.chemicraft.core.ChemiCraftCore;

/**
 * 放射線銃です
 * @author mozipi
 */
public class ItemRadiationGun extends Item {

	/**
	 * 次の発射までの遅延
	 */
	private short delay;

	public ItemRadiationGun(int par1) {
		super(par1);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
		this.maxStackSize = 1;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "radiation_gun");
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {

		boolean isCreative = par3EntityPlayer.capabilities.isCreativeMode;

		if (this.delay <= 0) {
			if (!isCreative) {
				this.field_00001(par1ItemStack, par2World, par3EntityPlayer);
				if (par3EntityPlayer.inventory.hasItem(ChemiCraftBase.instance.itemRadiationBallet.itemID)) {
					par3EntityPlayer.inventory.consumeInventoryItem(ChemiCraftBase.instance.itemRadiationBallet.itemID);
					par2World.playSound(par3EntityPlayer.posX,
							par3EntityPlayer.posY,
							par3EntityPlayer.posZ,
							"mob.endermen.portal",
							1.0F,
							1.3F,
							false);

					par2World.playSound(par3EntityPlayer.posX,
							par3EntityPlayer.posY,
							par3EntityPlayer.posZ,
							"ChemiCraft.raditionGun",
							2.0F,
							1.3F,
							false);
					if (!par2World.isRemote) {
						this.delay = 100;
					}
				}
			} else {
				this.field_00001(par1ItemStack, par2World, par3EntityPlayer);
				par2World.playSound(par3EntityPlayer.posX,
						par3EntityPlayer.posY,
						par3EntityPlayer.posZ,
						"mob.endermen.portal",
						1.0F,
						1.3F,
						false);

				par2World.playSound(par3EntityPlayer.posX,
						par3EntityPlayer.posY,
						par3EntityPlayer.posZ,
						"ChemiCraft.raditionGun",
						2.0F,
						1.3F,
						false);
				if (!par2World.isRemote) {
					this.delay = 100;
				}
			}
		}

		return super.onItemRightClick(par1ItemStack, par2World, par3EntityPlayer);
	}

	private void field_00001(ItemStack par1ItemStack, World par2World,
			EntityPlayer par3EntityPlayer) {

		ArrayList<Entity> collisions = ChemiCraftCore.instance.mathAuxiliary.getTriangleEntitysByPlayer(par2World,
				par3EntityPlayer.posX,
				par3EntityPlayer.posY,
				par3EntityPlayer.posZ,
				par3EntityPlayer.rotationYaw,
				par3EntityPlayer.rotationPitch,
				30,
				15);

		for (int i = 0; i < collisions.size(); i++) {
			if (collisions.get(i) instanceof EntityLiving && collisions.get(i) != par3EntityPlayer) {
				try {
					EntityLiving entity = (EntityLiving) collisions.get(i);
					entity.attackEntityFrom(DamageSource.causePlayerDamage(par3EntityPlayer), (int) (10 + Math.random() * 11));
				} catch (ClassCastException e) {
					break;
				}
			}
		}

	}

	@Override
	public void onUpdate(ItemStack par1ItemStack, World par2World,
			Entity par3Entity, int par4, boolean par5) {
		if (this.delay > 0 && !par2World.isRemote) {
			this.delay--;
		}
	}


}
