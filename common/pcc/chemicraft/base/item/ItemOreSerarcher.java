package pcc.chemicraft.base.item;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.base.ChemiCraftBase;
import pcc.chemicraft.core.ChemiCraftCore;

public class ItemOreSerarcher extends Item {

	public ItemOreSerarcher(int par1) {
		super(par1);
		this.setHasSubtypes(true);
		this.setMaxStackSize(1);
		this.setMaxDamage(0);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
		for (int i = 0; i < ChemiCraftBase.instance.atomOresID.length; i++) {
			if (par3World.getBlockId(par4, par5, par6) == ChemiCraftBase.instance.atomOresID[i]) {
				if (par1ItemStack.getTagCompound() == null) {
					par1ItemStack.stackTagCompound = new NBTTagCompound();
				}
				if (par3World.isRemote) {
					this.changeItem(par2EntityPlayer);
				}
				String blockName = new ItemStack(
						Block.blocksList[par3World.getBlockId(par4, par5, par6)],
						1,
						par3World.getBlockMetadata(par4, par5, par6)).getDisplayName();
				par1ItemStack.getTagCompound().setString("OreName", blockName);
			}
		}
		return true;
	}

	public void changeItem(EntityPlayer par1EntityPlayer) {
		par1EntityPlayer.inventory.inventoryChanged = true;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		this.changeItem(par3EntityPlayer);
		return par1ItemStack;
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		if (par1ItemStack.getTagCompound() != null) {
			return super.getItemDisplayName(par1ItemStack) + ":" + par1ItemStack.getTagCompound().getString("OreName");
		}
		return super.getItemDisplayName(par1ItemStack);
	}

	@Override
	public boolean getShareTag() {
		return true;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "ore_searcher");
	}

}
