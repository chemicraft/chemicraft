package pcc.chemicraft.base;

import net.minecraft.block.Block;
import net.minecraftforge.common.MinecraftForge;
import pcc.chemicraft.base.ore.BlockAtomOres;
import pcc.chemicraft.base.ore.ItemAtomOres;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Blockを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterBlock extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterBlock(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		//鉱石を変数に代入
		for (int i = 0; i < this.mod.blockAtomOres.length; i++) {
			this.mod.blockAtomOres[i] = new BlockAtomOres(this.mod.atomOresID[i]).
					setHardness(3.0F).setResistance(0.0F).
					setStepSound(Block.soundStoneFootstep).setUnlocalizedName("atomOres");
			//鉱石をMinecraftに登録
			GameRegistry.registerBlock(this.mod.blockAtomOres[i], ItemAtomOres.class, "atomOres" + i);
			//回収&最適に破壊できるピッケルのレベルを設定
			MinecraftForge.setBlockHarvestLevel(this.mod.blockAtomOres[i], "pickaxe", 2);
		}
	}

}
