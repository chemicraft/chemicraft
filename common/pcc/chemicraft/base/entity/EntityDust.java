package pcc.chemicraft.base.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

/**
 * 粉塵です。<br>
 * 時間差で爆発します。
 * @author mozipi,ponkotate
 */
public class EntityDust extends Entity {

	private final float explodeSize = 2.0F;
	private final short explodeRad = 5;
	private int delay = 0;
	private int deadDelay = 20;
	private float rad = 0;

	public EntityDust(World par1World) {
		super(par1World);
	}

	public EntityDust(World par1World, double par2, double par3, double par4) {
		super(par1World);
		this.posX = par2;
		this.posY = par3;
		this.posZ = par4;
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		this.delay++;
		if (this.delay > 100) {
			if (this.deadDelay > 0) {
				this.deadDelay--;
				this.rad += 0.5;
				if (this.deadDelay % 4 == 0) {
					this.explode(this.rad, false);
				} else if (this.deadDelay % 6 == 0) {
					this.explode(this.rad, true);
				}
			} else {
				this.setDead();
			}
		}

        float var17 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
        this.rotationYaw = (float)(Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);

        for (this.rotationPitch = (float)(Math.atan2(this.motionY, (double)var17) * 180.0D / Math.PI); this.rotationPitch - this.prevRotationPitch < -180.0F; this.prevRotationPitch -= 360.0F)
        {
            ;
        }

        while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
        {
            this.prevRotationPitch += 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw < -180.0F)
        {
            this.prevRotationYaw -= 360.0F;
        }

        while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
        {
            this.prevRotationYaw += 360.0F;
        }

        this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
        this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;

	}

	@Override
	protected void entityInit() {

	}

	@Override
	public void readEntityFromNBT(NBTTagCompound var1) {

	}

	@Override
	public void writeEntityToNBT(NBTTagCompound var1) {

	}

	private void explode(double r, boolean b) {
		double angle = 0;
		double angleY = 0;
		for (int i = 0; i < r; i++) {
			double y = this.posY - (r/2) + (i * 1F);
			double ry = 0;
			for (int j = 1; j < r; j++) {
				ry = Math.sin((double) i / r * Math.PI) * 8;
				angle += 360/j;
				double x = this.posX - Math.sin(Math.toRadians(angle)) * ry;
				double z = this.posZ + Math.cos(Math.toRadians(angle)) * ry;
				if (!this.worldObj.isRemote){
					this.worldObj.createExplosion(this, x - 6 + Math.random() * 12, y + 3, z - 6 + Math.random() * 12, this.explodeSize, b);
				}
			}
		}
	}

	public int getDelay() {
		return this.delay;
	}

}
