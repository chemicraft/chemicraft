package pcc.chemicraft.base.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * なんか黒い煙みたいなのが出てくるやつです<br>
 * Warning:WIP!!
 * @author ponkotate
 */
public class EntityBlackSmoke extends Entity {

	private final int TIME = 5 * 20;

	public HashMap<EntityMob, Integer> invisibilityMobsMap = new HashMap<EntityMob, Integer>();

	private int restTime;

	public EntityBlackSmoke(EntityPlayer par1EntityPlayer, World par2World, double par3, double par4, double par5) {
		super(par2World);
		this.posX = par3;
		this.posY = par4;
		this.posZ = par5;
		this.restTime = TIME;

		for (Entity var5:(ArrayList<Entity>)par2World.loadedEntityList){
			if (var5 instanceof EntityMob){
				this.invisibilityMobsMap.put((EntityMob)var5, TIME);
			}
		}
	}

	@Override
	protected void entityInit() {

	}

	@Override
	public void onUpdate() {
		if (this.restTime < 0) {
			this.setDead();
		}
		Iterator<EntityMob> var1 = this.invisibilityMobsMap.keySet().iterator();
		while (var1.hasNext()) {
			EntityMob var2 = var1.next();
			var2.setTarget(this);
		}
		for (int i = 0; i < 5; i++){
			this.worldObj.spawnParticle("smoke", this.posX, this.posY, this.posZ, Math.random() * 0.1D - 0.05D, 0.05D, Math.random() * 0.1D - 0.05D);
		}
		this.restTime--;
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound var1) {

	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound var1) {

	}

}
