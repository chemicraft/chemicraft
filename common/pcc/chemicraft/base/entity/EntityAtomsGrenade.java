package pcc.chemicraft.base.entity;

import java.util.Iterator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * 手榴弾です
 * @author mozipi,ponkotate
 */
public class EntityAtomsGrenade extends EntityThrowable {

	private float explodeSize;
	private boolean isExplode;
	private boolean onFire;
	private boolean isNuke;

    public EntityAtomsGrenade(World par1World) {
        super(par1World);
    }

	public EntityAtomsGrenade(World par1World, EntityLiving par2EntityLiving, boolean par3, boolean par4, boolean par5) {
		super(par1World, par2EntityLiving);
		this.isExplode = par3;
		this.onFire = par4;
		this.isNuke = par5;
	}

	public EntityAtomsGrenade(World par1World, double par2, double par3, double par4, boolean par5, boolean par6, boolean par7) {
		super(par1World, par2, par3, par4);
		this.isExplode = par5;
		this.onFire = par6;
		this.isNuke = par7;
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onImpact(MovingObjectPosition par1MovingObjectPosition) {

		if(isExplode) {
			this.explodeSize = 3.0F;
		}

		if(isNuke) {
			this.explodeSize = 35.0F;
		}

		if (par1MovingObjectPosition.entityHit != null) {
			par1MovingObjectPosition.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 2);
			par1MovingObjectPosition.entityHit.setFire(5);
		}

		if (!this.worldObj.isRemote && !isNuke) {
			this.worldObj.newExplosion((Entity) null, this.posX, this.posY, this.posZ, explodeSize, onFire, true);
			this.isDead = true;
		} else if(!this.worldObj.isRemote && isNuke) {
			this.worldObj.newExplosion((Entity) null, this.posX, this.posY, this.posZ, explodeSize, onFire, true);
			Iterator<EntityLiving> itr = this.worldObj.getEntitiesWithinAABB(EntityLiving.class, this.boundingBox.expand(30, 30, 30)).iterator();
			while(itr.hasNext()) {
				EntityLiving entity = itr.next();
				double dx = Math.abs(entity.posX - this.posX);
				double dy = Math.abs(entity.posY - this.posY);
				double dz = Math.abs(entity.posZ - this.posZ);
				int distance = (int) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2) );
				if(30 - distance >= 0) {
					entity.addPotionEffect(new PotionEffect(2, (60 + (30 - distance)) * 20, (30 - distance) / 5));
					entity.addPotionEffect(new PotionEffect(4, (60 + (30 - distance)) * 20, (30 - distance) / 5));
					entity.addPotionEffect(new PotionEffect(18, (60 + (30 - distance)) * 20, (30 - distance) / 5));
					entity.addPotionEffect(new PotionEffect(19, (60 + (30 - distance)) * 20, (30 - distance) / 5));
				}
			}
			this.isDead = true;
		}
	}

}
