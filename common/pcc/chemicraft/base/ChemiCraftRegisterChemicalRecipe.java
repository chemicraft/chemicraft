package pcc.chemicraft.base;

import pcc.chemicraft.base.gen.EnumOreSpawnFrequency;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.util.AtomInfo;
import pcc.chemicraft.util.Formula;

/**
 * 科学的なレシピを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterChemicalRecipe extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterChemicalRecipe(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		// 鉱石を追加
		for (int i = 0; i < this.mod.chemicalData.ATOMSLIST.length; i++) {
			if (AtomInfo.isSolid(i + 1) && !AtomInfo.isLanthanoid(i + 1)
					&& !AtomInfo.isActinoid(i + 1) && !AtomInfo.isOreOfVanilla(i + 1)) {
				//鉱石をAPIに追加
				this.mod.apiBase.addAtomOres(
						ChemiCraftCore.ATOMSNAME[i],
						new Formula(this.mod.chemicalData.ATOMSLIST[i]),
						this.mod.atomOresID[this.mod.apiBase.getAtomOresLastIndex()],
						EnumOreSpawnFrequency.NORMAL
						);
				//日本語名で追加
				this.mod.apiBase.addAtomOresLanguage(
						ChemiCraftCore.ATOMSNAME[i],
						ChemiCraftCore.ATOMSNAMEJP[i] + "鉱石",
						"ja_JP"
						);
				this.mod.apiBase.addAtomIngotsLanguage(
						ChemiCraftCore.ATOMSNAME[i],
						ChemiCraftCore.ATOMSNAMEJP[i] + "インゴット",
						"ja_JP"
						);
			}
		}

		// ランタノイド鉱石
		this.mod.apiBase.addAtomOres(
				"Lanthanoid",
				new Formula("LaCePrNdPmSmEuGdTbDyHoErTmYbLu"),
				this.mod.atomOresID[this.mod.apiBase.getAtomOresLastIndex()],
				EnumOreSpawnFrequency.RARE
				);
		// LaCePrNdPmSmEuGdTbDyHoErTmYbLu
		this.mod.apiBase.addAtomOresLanguage(
				"Lanthanoid",
				"ランタノイド鉱石",
				"ja_JP"
				);
		this.mod.apiBase.addAtomIngotsLanguage(
				"Lanthanoid",
				"ランタノイドインゴット",
				"ja_JP"
				);

		// アクチノイド鉱石
		this.mod.apiBase.addAtomOres(
				"Actinoid",
				new Formula("AcThPaUNpPuAmCmBkCfEsFmMdNoLr"),
				this.mod.atomOresID[this.mod.apiBase.getAtomOresLastIndex()],
				EnumOreSpawnFrequency.RARE);
		// AcThPaUNpPuAmCmBkCfEsFmMdNoLr
		this.mod.apiBase.addAtomOresLanguage(
				"Actinoid",
				"アクチノイド鉱石",
				"ja_JP"
				);
		this.mod.apiBase.addAtomIngotsLanguage(
				"Actinoid",
				"アクチノイドインゴット",
				"ja_JP"
				);

	}

}
