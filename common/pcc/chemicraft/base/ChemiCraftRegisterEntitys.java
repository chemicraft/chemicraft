package pcc.chemicraft.base;

import net.minecraft.src.ModLoader;
import pcc.chemicraft.base.entity.EntityAtomsGrenade;
import pcc.chemicraft.base.entity.EntityDust;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * Entityを登録します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterEntitys extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterEntitys(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		//手榴弾
		LanguageRegistry.instance().addStringLocalization("entity.AtomsGrenade.name", "en_US", "AtomsGrenade");
		EntityRegistry.registerModEntity(EntityAtomsGrenade.class,
				"AtomsGrenade",
				ModLoader.getUniqueEntityId(),
				this.mod,
				250,
				1,
				false);
		//粉塵
		LanguageRegistry.instance().addStringLocalization("entity.Dust.name", "en_US", "Dust");
		EntityRegistry.registerModEntity(EntityDust.class,
				"Dust",
				ModLoader.getUniqueEntityId(),
				this.mod,
				250,
				5,
				true);
	}

}
