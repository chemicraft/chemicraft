package pcc.chemicraft.base;

import pcc.chemicraft.base.compounds.CompoundWater;

/**
 * 化合物のHandlerを設定します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterCompoundsHandler extends ChemiCraftBaseRegister {

	public ChemiCraftRegisterCompoundsHandler(ChemiCraftBase mod) {
		super(mod);
	}

	@Override
	public void start() {
		this.mod.api.settingCompoundHandler("Water", new CompoundWater());
	}

}
