package pcc.chemicraft.base.compounds;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.util.ICompoundHandler;

/**
 * 水の化合物ハンドラーです
 * @author mozipi
 */
public class CompoundWater implements ICompoundHandler {

	@Override
	public ItemStack onItemRightClickHandler(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		return null;
	}

	@Override
	public boolean onItemUseHandler(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
		return false;
	}

	@Override
	public void onUpdateHandler(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {

	}

	@Override
	public String getIconIndexHandler() {
		return "ChemiCraft:Composite";
	}

}
