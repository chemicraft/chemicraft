package pcc.chemicraft.base.ore;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import pcc.chemicraft.base.ChemiCraftBase;
import pcc.chemicraft.base.ChemiCraftBaseAPI;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * 鉱石のクラスです
 * @author mozipi,ponkotate
 */
public class BlockAtomOres extends Block {

	@SideOnly(Side.CLIENT)
    private Icon[] icons;

	public BlockAtomOres(int id) {
		super(id, Material.rock);
		this.setCreativeTab(ChemiCraftBase.instance.creativeTabAtomOres);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister par1IconRegister) {
		ArrayList<String> atomNames = ChemiCraftBaseAPI.instance().getAtomOresAtomName();
		this.icons = new Icon[atomNames.size()];

		for (int i = 0; i < this.icons.length; i++){
			this.icons[i] = par1IconRegister.registerIcon(ChemiCraftBase.ORE + atomNames.get(i));
		}

		// WIP
		this.icons[icons.length - 1] = par1IconRegister.registerIcon(ChemiCraftBase.ORE + "WIP");
		this.icons[icons.length - 2] = par1IconRegister.registerIcon(ChemiCraftBase.ORE + "WIP");
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int par1, int par2) {
		int var3 = this.blockID - ChemiCraftBase.instance.atomOresID[0];
		return this.icons[par2 + var3 * 16];
	}

	@Override
	public int damageDropped(int par1) {
		return par1;
	}

	public static int getBlockFromDye(int par0) {
		return ~par0 & 15;
	}

	public static int getDyeFromBlock(int par0) {
		return ~par0 & 15;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List par3List) {
		int var4 = this.blockID - ChemiCraftBase.instance.atomOresID[0] + 1;
		int var5 = 16;
		if (var4 == ChemiCraftBase.instance.atomOresID.length){
			var5 -= var4 * 16 - ChemiCraftBaseAPI.instance().getAtomOresAtomName().size();
		}
		for (int i = 0; i < var5; i++) {
			par3List.add(new ItemStack(par1, 1, i));
		}
	}

}
