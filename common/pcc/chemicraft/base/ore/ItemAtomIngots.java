package pcc.chemicraft.base.ore;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import pcc.chemicraft.base.ChemiCraftBase;
import pcc.chemicraft.base.ChemiCraftBaseAPI;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemAtomIngots extends Item {

	@SideOnly(Side.CLIENT)
	private Icon[] icons;

	public ItemAtomIngots(int par1) {
		super(par1);
		this.maxStackSize = 64;
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		// this.setCreativeTab(ChemiCraftBase.instance.creativeTabAtomOres);
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List){
		for(int i = 0; i < ChemiCraftBaseAPI.instance().getAtomOresAtomName().size(); i++)
		{
			par3List.add(new ItemStack(par1, 1, i));
		}
	}



	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return this.icons[par1];
	}



	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack){
		return super.getUnlocalizedName() + "." + ChemiCraftBaseAPI.instance().getAtomOresAtomName().get(par1ItemStack.getItemDamage());
	}



	@Override
	@SideOnly(Side.CLIENT)
	public void updateIcons(IconRegister par1IconRegister) {
		ArrayList<String> atomNames = ChemiCraftBaseAPI.instance().getAtomOresAtomName();
		this.icons = new Icon[atomNames.size()];

		for (int i = 0; i < this.icons.length; i++){
			this.icons[i] = par1IconRegister.registerIcon(ChemiCraftBase.INGOT + atomNames.get(i));
		}
	}

}
