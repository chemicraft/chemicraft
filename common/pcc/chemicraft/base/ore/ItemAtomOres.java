package pcc.chemicraft.base.ore;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import pcc.chemicraft.base.ChemiCraftBase;
import pcc.chemicraft.base.ChemiCraftBaseAPI;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * 鉱石のアイテム版です
 * @author mozipi
 */
public class ItemAtomOres extends ItemBlock {

	public ItemAtomOres(int par1) {
		super(par1);
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return ChemiCraftBase.instance.blockAtomOres[0].getBlockTextureFromSideAndMetadata(2, BlockAtomOres.getBlockFromDye(par1));
	}

	@Override
	public int getMetadata(int par1) {
		return par1;
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack) {
		int var2 = this.itemID - (ChemiCraftBase.instance.atomOresID[0]);
		return super.getUnlocalizedName() + "." + ChemiCraftBaseAPI.instance()
				.getAtomOresAtomName()
				.get(par1ItemStack.getItemDamage() + var2 * 16);
	}

}
