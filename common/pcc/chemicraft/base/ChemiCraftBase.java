package pcc.chemicraft.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import net.minecraftforge.oredict.OreDictionary;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.ChemiCraftConfiguration;
import pcc.chemicraft.ChemiCraftRegister;
import pcc.chemicraft.EnumLoggingType;
import pcc.chemicraft.base.creativetab.CreativeTabAtomOres;
import pcc.chemicraft.base.dispenser.DispenserBehaviorAtomsGrenade;
import pcc.chemicraft.base.system.CommonProxy;
import pcc.chemicraft.base.system.PacketHandler;
import pcc.chemicraft.core.ChemiCraftCore;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.LoaderState.ModState;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = "ChemiCraftBase", name = "ChemiCraftBase", version = "Beta1")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, versionBounds = "1.5", channels = "chemicraftbase", packetHandler = PacketHandler.class)
public class ChemiCraftBase extends ChemiCraft {

	/**
	 * this is ChemiCraft instance.
	 */
	@Instance("ChemiCraftBase")
	public static ChemiCraftBase instance;

	/**
	 * proxy of ChemiCraft.
	 */
	@SidedProxy(clientSide = "pcc.chemicraft.base.client.ClientProxy", serverSide = "pcc.chemicraft.base.system.CommonProxy")
	public static CommonProxy proxy;

	/**
	 * API Instance.
	 */
	public ChemiCraftBaseAPI apiBase = ChemiCraftBaseAPI.instance();

	/**
	 * CreativeTab of ChemiCraft.
	 */
	public static final CreativeTabs creativeTabAtomOres = new CreativeTabAtomOres("AtomOres");

	/**
	 * the ItemID.
	 */
	public int atomIngotsID;
	public int atomGrenadeID;
	public int blackSmokeID;
	public int oreSerarcherID;
	public int dustID;
	public int radiationGunID;
	public int radiationBalletID;
	public int raditionGunDataChipID;

	/**
	 * the BlockID.
	 */
	public int[] atomOresID = new int[4];

	/**
	 * Variables of Block type.
	 */
	public Block[] blockAtomOres = new Block[atomOresID.length];

	/**
	 * Variables of Item type.
	 */
	public Item itemAtomIngots;
	public Item itemAtomGrenade;
	public Item itemBlackSmoke;
	public Item itemDust;
	public Item itemRadiationGun;
	public Item itemRadiationBallet;
	public Item itemOreSearcher;
	public Item itemRaditionGunDataChip;

	/**
	 * the Register Instances.
	 */
	public ChemiCraftRegister registerCompounds;
	public ChemiCraftRegister registerCompoundsHandler;
	public ChemiCraftRegister registerItem;
	public ChemiCraftRegister registerChemicalRecipe;
	public ChemiCraftRegister registerBlock;
	public ChemiCraftRegister registerRecipe;
	public ChemiCraftRegister registerEntitys;

	/**
	 * the Textures.
	 */
	public static final String INGOT = ChemiCraft.TEXTURE + "atom_ingots_";
	public static final String ORE = ChemiCraft.TEXTURE + "atom_ores_";

	public ChemiCraftBase() {
		this.registerCompounds = new ChemiCraftRegisterCompounds(this);
		this.registerCompoundsHandler = new ChemiCraftRegisterCompoundsHandler(this);
		this.registerItem = new ChemiCraftRegisterItem(this);
		this.registerChemicalRecipe = new ChemiCraftRegisterChemicalRecipe(this);
		this.registerBlock = new ChemiCraftRegisterBlock(this);
		this.registerRecipe = new ChemiCraftRegisterBaseRecipe(this);
		this.registerEntitys = new ChemiCraftRegisterEntitys(this);
	}

	@PostInit
	public void chemiPostLoadMethod(final FMLPostInitializationEvent event) {
		this.thread = new Thread(this);
		this.event = event;
		this.thread.start();

		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		proxy.registerRenderInformation();
		Thread.yield();
	}

	@Override
	public void run() {
		while (true) {
			if (proxy != null && instance != null) {
				this.settingProcessing((FMLPostInitializationEvent) event);
				break;
			}
		}

		Thread loadCheckThread = new Thread() {
			@Override
			public void run() {
				while (true) {
					List<ModContainer> mod = Loader.instance().getModList();
					ModContainer finalMod = mod.get(mod.size()-1);
					ModState finalModState = Loader.instance().getModState(finalMod);
					if (finalModState == ModState.POSTINITIALIZED) {
						ChemiCraftCore.logger.write("ChemiCraftBase>APIProcessing", EnumLoggingType.INFO);
						apiProcessing((FMLPostInitializationEvent) event);
						break;
					}
				}
			}
		};
		loadCheckThread.start();

	}

	/**
	 * PreInit:<br>
	 * Configをロードします。
	 * @param event アノテーション呼び出しにより呼び出す必要なし
	 */
	@PreInit
	public void chemiPreLoadMethod(FMLPreInitializationEvent event) {
		Configuration cfg = new Configuration(event.getSuggestedConfigurationFile());
		cfg.load();

		Property baseBlockID = cfg.get("BlockID", "Base of Block ID", 2500);
		Property baseItemID = cfg.get("ItemID", "Base of Item ID", 25500);

		ChemiCraftConfiguration ccfgBlock = new ChemiCraftConfiguration(baseBlockID.getInt());
		ChemiCraftConfiguration ccfgItem = new ChemiCraftConfiguration(baseItemID.getInt());

		for (int i = 0; i < atomOresID.length; i++) {
			this.atomOresID[i] = ccfgBlock.additionID();
		}

		this.atomIngotsID = ccfgItem.additionID();
		this.atomGrenadeID = ccfgItem.additionID();
		this.dustID = ccfgItem.additionID();
		this.radiationGunID = ccfgItem.additionID();
		this.radiationBalletID = ccfgItem.additionID();
		this.blackSmokeID = ccfgItem.additionID();
		this.oreSerarcherID = ccfgItem.additionID();

		cfg.save();
	}

	protected void settingProcessing(FMLPostInitializationEvent event) {
		this.registerCompounds.start();
		this.registerCompoundsHandler.start();
		this.registerItem.start();
		this.registerBlock.start();
		this.registerChemicalRecipe.start();
		this.registerRecipe.start();
		this.registerEntitys.start();

		BlockDispenser.dispenseBehaviorRegistry.putObject(this.itemAtomGrenade, new DispenserBehaviorAtomsGrenade());

	}

	private void apiProcessing(final FMLPostInitializationEvent event) {
		// API用の処理
		for (int i = 0; i < this.apiBase.getAtomOres().size(); i++) {
			OreDictionary.registerOre("ore" + this.apiBase.getAtomOresAtomName().get(i), new ItemStack(this.blockAtomOres[i / 16], 1, i % 16));
			OreDictionary.registerOre("ingot" + this.apiBase.getAtomOresAtomName().get(i), new ItemStack(this.itemAtomIngots, 1, i ));
			GameRegistry.registerWorldGenerator(this.apiBase.getAtomOres().get(i));
		}

		Iterator<String> langoresItr = this.apiBase.getAtomOresName().iterator();
		while (langoresItr.hasNext()){
			String lang = langoresItr.next();
			ArrayList<String> oresName = this.apiBase.getAtomOresName().get(lang);
			ArrayList<String> ingotsName = this.apiBase.getAtomIngotsName().get(lang);
			for (int i = 0; i < oresName.size(); i++) {
				ChemiCraftCore.logger.write("AtomOresAddName:" + "IngotName-" + ingotsName.get(i) + "OreName-" + oresName.get(i) + "ID-" + (i / 16)+ "/Damage-" + (i - i / 16 * 16) + "/Lang-" + lang,
						EnumLoggingType.INFO);

				LanguageRegistry.instance().addNameForObject(
						new ItemStack(this.atomOresID[i / 16], 1, i % 16),
						lang,
						oresName.get(i)
						);

				/*
				LanguageRegistry.instance().addNameForObject(
						new ItemStack(this.atomIngotsID, 1, i),
						lang,
						ingotsName.get(i)
						);
						*/

			}
		}

	}

}
