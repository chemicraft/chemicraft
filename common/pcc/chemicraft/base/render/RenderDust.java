package pcc.chemicraft.base.render;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderEntity;
import net.minecraft.entity.Entity;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import pcc.chemicraft.base.entity.EntityDust;
import pcc.chemicraft.core.ChemiCraftCore;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * 粉塵のRenderです
 * @author mozipi
 */
@SideOnly(Side.CLIENT)
public class RenderDust extends RenderEntity {

	@Override
	public void doRender(Entity var1, double var2, double var4, double var6,
			float var8, float var9) {
		EntityDust entity = (EntityDust) var1;
		GL11.glPushMatrix();
		GL11.glTranslatef((float)var2, (float)var4, (float)var6);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		float var10 = entity.getDelay() / 8.0F;
		GL11.glScalef(var10 / 1.0F, var10 / 1.0F, var10 / 1.0F);
		byte var11 = 0;
		this.loadTexture(ChemiCraftCore.instance.ENTITY_PARTICLE_TEXRURE);
		Tessellator var12 = Tessellator.instance;
		float var13 = (float)(var11 % 256 * 256 + 0) / 256F;
		float var14 = (float)(var11 % 256 * 256 + 256) / 256F;
		float var15 = (float)(var11 / 256 * 256 + 0) / 256F;
		float var16 = (float)(var11 / 256 * 256 + 256) / 256F;
		float var17 = 1.0F;
		float var18 = 0.5F;
		float var19 = 0.25F;
		GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		var12.startDrawingQuads();
		var12.setNormal(0.0F, 1.0F, 0.0F);
		var12.addVertexWithUV((double)(0.0F - var18), (double)(0.0F - var19), 0.0D, (double)var13, (double)var16);
		var12.addVertexWithUV((double)(var17 - var18), (double)(0.0F - var19), 0.0D, (double)var14, (double)var16);
		var12.addVertexWithUV((double)(var17 - var18), (double)(1.0F - var19), 0.0D, (double)var14, (double)var15);
		var12.addVertexWithUV((double)(0.0F - var18), (double)(1.0F - var19), 0.0D, (double)var13, (double)var15);
		var12.draw();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

}
