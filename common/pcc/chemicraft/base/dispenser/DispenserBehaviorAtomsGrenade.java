package pcc.chemicraft.base.dispenser;

import net.minecraft.dispenser.BehaviorProjectileDispense;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.IProjectile;
import net.minecraft.world.World;
import pcc.chemicraft.base.entity.EntityAtomsGrenade;

public class DispenserBehaviorAtomsGrenade extends BehaviorProjectileDispense {

	@Override
	protected IProjectile getProjectileEntity(World par1World, IPosition par2IPosition) {
		return new EntityAtomsGrenade(par1World, par2IPosition.getX(), par2IPosition.getY(), par2IPosition.getZ(), true, true, true);
	}

}
