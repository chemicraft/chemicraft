package pcc.chemicraft.base.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import pcc.chemicraft.base.ChemiCraftBase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * ChemiCraftの鉱石のクリエイティブタブです
 * @author mozipi,ponkotate
 */
public class CreativeTabAtomOres extends CreativeTabs {

	public CreativeTabAtomOres(String type) {
		super(type);
	}



	@Override
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex() {
		return ChemiCraftBase.instance.blockAtomOres[0].blockID;
	}



	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel() {
		return "AtomOres";
	}

}
