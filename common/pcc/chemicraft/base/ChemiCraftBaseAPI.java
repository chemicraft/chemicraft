package pcc.chemicraft.base;

import java.util.ArrayList;
import java.util.HashMap;

import pcc.chemicraft.base.gen.EnumOreSpawnFrequency;
import pcc.chemicraft.base.gen.WorldGenAtomOres;
import pcc.chemicraft.util.AtomInfo;
import pcc.chemicraft.util.Formula;
import pcc.chemicraft.util.ListHash;

/**
 * ChemiCraftBaseのAPIを提供するクラスです。<br>
 * ChemiCraftBaseを使用したAPIを作成する場合、このクラスを使用します。
 * @author mozipi,ponkotate
 */
public class ChemiCraftBaseAPI {

	/**
	 * APIのインスタンス
	 */
	private static ChemiCraftBaseAPI instance = new ChemiCraftBaseAPI();


	/**
	 * APIのインスタンスを返します。
	 * @return APIのインスタンス
	 */
	public static ChemiCraftBaseAPI instance(){
		return instance;
	}

	/**
	 * 鉱石別元素数リスト
	 */
	private HashMap<String, Formula> atomOresFormulasHash = new HashMap<String, Formula>();


	/**
	 * 鉱石リスト
	 */
	private ArrayList<WorldGenAtomOres> atomOresList = new ArrayList<WorldGenAtomOres>();


	/**
	 * 鉱石名リスト
	 */
	private ListHash<String, String> atomOresNameListHash = new ListHash<String, String>();


	/**
	 * インゴット名リスト
	 */
	private ListHash<String, String> atomIngotsNameListHash = new ListHash<String, String>();


	/**
	 * 鉱石の英語名リスト
	 */
	private ArrayList<String> atomOresAtomList = new ArrayList<String>();

	/**
	 * 鉱石を追加します。
	 * @param par1Name 鉱石名(Oreは自動で語尾につけられます)
	 * @param par2Formula 化学式
	 * @param par3Id 鉱石のID
	 * @param par4Size 鉱石の塊の大きさ
	 * @param par5Frequency 生成率(frequency/Chunk)
	 * @param par6PosY 鉱石が生成される高度(nowY < PosY == Generate)
	 */
	public void addAtomOres(String par1Name, Formula par2Formula, int par3Id, int par4Size, int par5Frequency, int par6PosY){
		atomOresFormulasHash.put(par1Name, par2Formula);

		if (!AtomInfo.isExisting(par1Name)) {
			atomOresList.add(
					new WorldGenAtomOres(
							par3Id,
							getAtomOresMetaOfLastIndex(),
							par4Size,
							par5Frequency,
							par6PosY));

			atomOresAtomList.add(par1Name);

			String var7 = par1Name;
			if (!par1Name.contains(" Ore")) {
				var7 = par1Name + " Ore";
			}

			addAtomOresLanguage(par1Name, var7, "en_US");

			if (!par1Name.contains(" Ingot")) {
				var7 = par1Name + " Ingot";
			}

			addAtomIngotsLanguage(par1Name, var7, "en_US");
		}
	}



	/**
	 * 鉱石を追加します。
	 * @param par1Name 鉱石名(Oreは自動で語尾につけられます)
	 * @param par2Formula 化学式
	 * @param par3Id 鉱石のID
	 * @param par4Enum 鉱石生成の情報が入ったEnum
	 */
	public void addAtomOres(String par1Name, Formula par2Formula, int par3Id, EnumOreSpawnFrequency par4Enum){
		addAtomOres(
				par1Name,
				par2Formula,
				par3Id,
				par4Enum.getSize(),
				par4Enum.getFrequency(),
				par4Enum.getPosY()
				);
	}



	/**
	 * 既に登録した鉱石の新しい名前・言語を追加します
	 * @param par1KeyName 鉱石追加の際に用いたpar1Nameに当たる文字列
	 * @param par2NewName 新しい名前
	 * @param par3NewLanguage 新しい言語
	 */
	public void addAtomOresLanguage(String par1KeyName, String par2NewName, String par3NewLanguage){
		if (this.atomOresAtomList.contains(par1KeyName)){
			atomOresNameListHash.add(
					par3NewLanguage,
					par2NewName
					);
		}
	}



	/**
	 * 既に登録したインゴットの新しい名前・言語を追加します
	 * @param par1KeyName 鉱石追加の際に用いたpar1Nameに当たる文字列
	 * @param par2NewName 新しい名前
	 * @param par3NewLanguage 新しい言語
	 */
	public void addAtomIngotsLanguage(String par1KeyName, String par2NewName, String par3NewLanguage){
		if (this.atomOresAtomList.contains(par1KeyName)){
			atomIngotsNameListHash.add(
					par3NewLanguage,
					par2NewName
					);
		}
	}



	//以下システム関連//////////////////////////////////////////////////////

	public ArrayList<WorldGenAtomOres> getAtomOres(){
		return atomOresList;
	}

	public HashMap<String, Formula> getAtomOresFormulas(){
		return atomOresFormulasHash;
	}



	public int getAtomOresLastIndex(){
		return atomOresAtomList.size() / 16;
	}



	public int getAtomOresMetaOfLastIndex(){
		return atomOresAtomList.size() - getAtomOresLastIndex() * 16;
	}



	public ListHash<String, String> getAtomOresName(){
		return atomOresNameListHash;
	}



	public ListHash<String, String> getAtomIngotsName(){
		return atomIngotsNameListHash;
	}



	public ArrayList<String> getAtomOresAtomName() {
		return this.atomOresAtomList;
	}

}
