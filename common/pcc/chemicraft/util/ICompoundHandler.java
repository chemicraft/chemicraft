package pcc.chemicraft.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * 化合物のハンドラーのインターフェイスです。
 * @author mozipi
 */
public interface ICompoundHandler {

	/**
	 * 右クリック時のHandler
	 * @param par1ItemStack
	 * @param par2World
	 * @param par3EntityPlayer
	 * @return
	 */
	public ItemStack onItemRightClickHandler(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer);


	/**
	 * Itemを使用したときのHandler
	 * @param par1ItemStack
	 * @param par2EntityPlayer
	 * @param par3World
	 * @param par4
	 * @param par5
	 * @param par6
	 * @param par7
	 * @param par8
	 * @param par9
	 * @param par10
	 * @return
	 */
	public boolean onItemUseHandler(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10);


	/**
	 * 毎Tick呼ばれるHandler
	 * @param par1ItemStack
	 * @param par2World
	 * @param par3Entity
	 * @param par4
	 * @param par5
	 */
	public void onUpdateHandler(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5);

	/**
	 * iconIndexの指定
	 * @return iconIndex
	 */
	public String getIconIndexHandler();

}
