package pcc.chemicraft.util;

import pcc.chemicraft.ChemiCraftData;

/**
 * 元素情報を格納するクラスです
 * @author mozipi
 */
public class FormulaPart {

	private String atom;
	private int amount;

	public FormulaPart(String par1, int par2){
		this.atom = par1;
		this.amount = par2;
	}

	public String getAtom(){
		return this.atom;
	}

	public int getAmount(){
		return this.amount;
	}

	@Override
	public boolean equals(Object par1) {
		try {
			FormulaPart var1 = (FormulaPart)par1;
			if (var1.atom.equals(this.atom) && var1.amount == this.amount) {
				return true;
			}
		} catch (ClassCastException e) {
			return false;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return ChemiCraftData.toAtoms(this.atom) + this.amount * 200;
	}

}
