package pcc.chemicraft.util;

import java.util.ArrayList;

import pcc.chemicraft.ChemiCraftData;

/**
 * 化学式から元素と数を取り出してArrayListに格納します。
 * @author ponkotate
 */
public class Formula {

	private ArrayList<String> atoms = new ArrayList<String>();
	private ArrayList<Integer> amonts = new ArrayList<Integer>();

	private int magnification;

	private String lastSpell;
	private int lastIndex;

	public Formula(String par1) {
		this.magnification = 1;
		this.run(par1);
	}

	public Formula(String[] par1, Integer[] par2) {
		this.setAtoms(par1, par2);
	}

	public Formula(ArrayList<String> par1, ArrayList<Integer> par2) {
		this.atoms = par1;
		this.amonts = par2;
	}

	// 括弧対応はまだ未完成です。 --WIP--
	public void run(String par1) {
		char[] var2 = par1.toCharArray();

		for (int i = 0; i < var2.length; i++) {
			if (var2[i] == '・') {
				if (this.lastSpell != null) {
					setAtom(this.lastSpell, 1);
				}
				setAtoms(new Formula(new String(var2, ++i, var2.length - i)));
				return;
			} else if (var2[i] == '(') {
				// 括弧対応はまだ未完成です。 --WIP--
			} else if (Character.isDigit(var2[i])) {
				if (this.lastIndex > 0) {
					this.amonts.set(this.amonts.size() - 1,
							this.lastIndex = this.lastIndex * 10 + Integer.valueOf(Character.toString(var2[i])));
				} else if (this.lastSpell != null) {
					setAtom(this.lastSpell, this.lastIndex = Integer.valueOf(Character.toString(var2[i])));
				} else {
					this.magnification = Integer.valueOf(Character.toString(var2[i]));
				}
				this.lastSpell = null;
			} else if (Character.isLetter(var2[i])) {
				if (Character.isUpperCase(var2[i])) {
					if (this.lastSpell != null) {
						setAtom(this.lastSpell, 1);
					}
					this.lastSpell = Character.toString(var2[i]);
				} else if (Character.isLowerCase(var2[i])) {
					if (this.lastSpell == null) {
						throw new IllegalArgumentException();
					}
					this.lastSpell = this.lastSpell.concat(Character.toString(var2[i]));
				}
				this.lastIndex = 0;
			} else {
				throw new IllegalArgumentException();
			}
		}
		if (this.lastSpell != null) {
			setAtom(this.lastSpell, 1);
		}
	}

	public boolean isAtom(String par1) {
		for (String var2:ChemiCraftData.ATOMSLIST) {
			if (par1.equalsIgnoreCase(var2)) {
				return true;
			}
		}
		return false;
	}

	public void setAtom(String par1,  int par2) {
		if (isAtom(par1)) {
			par2 *= this.magnification;
			int var3 = this.atoms.indexOf(par1);
			if (var3 == -1) {
				this.atoms.add(par1);
				this.amonts.add(par2);
			} else {
				this.amonts.set(var3, this.amonts.get(var3) + par2);
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void setAtom(String par1,  char par2) {
		setAtom(par1, Integer.valueOf(Character.toString(par2)));
	}

	public void setAtoms(String[] par1, Integer[] par2) {
		setAtoms(par1, par2, 1);
	}
	public void setAtoms(String[] par1, Integer[] par2, int par3) {
		if (par1.length != par2.length) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = 0; i < par1.length; i++) {
			setAtom(par1[i], par2[i] * par3);
		}
	}

	public void setAtoms(Formula par1) {
		setAtoms(par1, 1);
	}

	public void setAtoms(Formula par1, int par2) {
		setAtoms(par1.getAtoms(), par1.getAmonts(), par2);
	}

	public String[] getAtoms() {
		return atoms.toArray(new String[atoms.size()]);
	}

	public Integer[] getAmonts() {
		return amonts.toArray(new Integer[amonts.size()]);
	}

}
