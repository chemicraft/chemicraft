package pcc.chemicraft.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Ponkotate version:1.0.0 dev
 *
 *         Create ArrayList in HashMap.
 */
public class ListHash<K, E> {

	protected ArrayList<K> keysList = new ArrayList<K>();
	protected HashMap<K, ArrayList<E>> elementsHash = new HashMap<K, ArrayList<E>>();

	public boolean add(K par1Key, E par2Element) {
		try {
			return this.elementsHash.get(par1Key).add(par2Element);
		} catch (Exception e) {
			return this.put(par1Key, par2Element);
		}
	}

	public boolean addAll(K par1Key, Collection<? extends E> par2Collection) {
		return this.elementsHash.get(par1Key).addAll(par2Collection);
	}

	public boolean addAll(K par1Key, int par2Index, Collection<? extends E> par3Collection) {
		return this.elementsHash.get(par1Key).addAll(par2Index, par3Collection);
	}

	public boolean createHash(K par1Key) {
		if (this.elementsHash.put(par1Key, new ArrayList<E>()) != null) {
			return false;
		}
		this.keysList.add(par1Key);
		return true;
	}

	public void clear() {
		this.elementsHash.clear();
	}

	public boolean containsKey(K par1Key) {
		return this.elementsHash.containsKey(par1Key);
	}

	public boolean containsValue(K par1Key) {
		return this.elementsHash.containsValue(par1Key);
	}

	public boolean contains(K par1Key, E par2Element) {
		return this.elementsHash.get(par1Key).contains(par2Element);
	}

	public boolean containsAll(K par1Key, Collection<?> par2Collection) {
		return this.elementsHash.get(par1Key).containsAll(par2Collection);
	}

	public Set<Entry<K, ArrayList<E>>> entrySet() {
		return this.elementsHash.entrySet();
	}

	public K getKeyList(int par1Index) {
		return this.keysList.get(par1Index);
	}

	public E get(K par1Key, int par2Index) {
		return this.elementsHash.get(par1Key).get(par2Index);
	}

	public ArrayList<E> get(K par1Key) {
		return this.elementsHash.get(par1Key);
	}

	public int indexOf(K par1Key, E par2Element) {
		return this.elementsHash.get(par1Key).indexOf(par2Element);
	}

	public boolean isKeysListEmpty(K par1Key) {
		return this.elementsHash.get(par1Key).isEmpty();
	}

	public boolean isElementsHashEmpty() {
		return this.elementsHash.isEmpty();
	}

	public Iterator<K> iterator() {
		return this.keysList.iterator();
	}

	public Iterator<E> iterator(K par1Key) {
		return this.elementsHash.get(par1Key).iterator();
	}

	public int lastIndexOf(K par1Key, E par2Element) {
		return this.elementsHash.get(par1Key).lastIndexOf(par2Element);
	}

	public ListIterator<E> listIterator(K par1Key) {
		return this.elementsHash.get(par1Key).listIterator();
	}

	public ListIterator<E> listIterator(K par1Key, int par2Index) {
		return this.elementsHash.get(par1Key).listIterator(par2Index);
	}

	public Set<K> keySet() {
		return this.elementsHash.keySet();
	}

	public boolean put(K par1Key, E par2Element) {
		this.keysList.add(par1Key);

		ArrayList<E> elementList = new ArrayList<E>();
		this.elementsHash.put(par1Key, elementList);
		return this.add(par1Key, par2Element);
	}

	public void putAll(Map<? extends K, ? extends ArrayList<E>> par1Map) {
		this.elementsHash.putAll(par1Map);
	}

	public ArrayList<E> remove(K par1Key) {
		return this.elementsHash.remove(par1Key);
	}

	public boolean remove(K par1Key, E par2Element) {
		return this.elementsHash.get(par1Key).remove(par2Element);
	}

	public E remove(K par1Key, int par2Index) {
		return this.elementsHash.get(par1Key).remove(par2Index);
	}

	public boolean removeAll(K par1Key, Collection<?> par2Collection) {
		return this.elementsHash.get(par1Key).removeAll(par2Collection);
	}

	public boolean retainAll(K par1Key, Collection<?> par2Collection) {
		return this.elementsHash.get(par1Key).retainAll(par2Collection);
	}

	public E set(K par1Key, int par2Index, E par3Element) {
		return this.elementsHash.get(par1Key).set(par2Index, par3Element);
	}

	public int sizeElementsHash() {
		return this.elementsHash.size();
	}

	public int sizeKeysList() {
		return this.keysList.size();
	}

	public int sizeElementsList(K par1Key) {
		return this.elementsHash.get(par1Key).size();
	}

	public List<E> subList(K par1Key, int par2FromIndex, int par3ToIndex) {
		return this.elementsHash.get(par1Key).subList(
				par2FromIndex,
				par3ToIndex);
	}

	public E[] toArray(K par1Key) {
		return (E[])this.elementsHash.get(par1Key).toArray();
	}

	public <T> T[] toArray(K par1Key, T[] par2Array) {
		return this.elementsHash.get(par1Key).toArray(par2Array);
	}

	public Collection<ArrayList<E>> values() {
		return this.elementsHash.values();
	}

}
