package pcc.chemicraft.util;

import java.util.ArrayList;
import java.util.Arrays;

import net.minecraft.item.ItemStack;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingMaterial;
import pcc.chemicraft.core.nbt.ChemicalNBTRecipe;
import pcc.chemicraft.util.Auxiliary.ArrayAuxiliary;

/**
 * 素材制作台のレシピを格納するクラスです
 * @author mozipi
 */
public class MaterialRecipe {

	private ChemicalNBTRecipe nbtRecipe;
	private ItemStack result;
	private ItemStack[] material;
	private boolean isSharpless = true;

	public MaterialRecipe(ItemStack par1ItemStack, ItemStack[] par2ItemStacks, ChemicalNBTRecipe par3NBTRecipe, boolean par4){
		this.result = par1ItemStack;
		this.material = par2ItemStacks.clone();
		this.nbtRecipe = par3NBTRecipe;
		this.isSharpless = par4;
	}

	/**
	 * インベントリの中にあるアイテムとレシピの内容が一致するかどうか判定します。<br>
	 * @param par1IInventory Inventory
	 * @return 一致したばあいはthis.resultを。それ以外はnull
	 */
	@SuppressWarnings("unchecked")
	public ItemStack match(InventoryChemicalCraftingMaterial par1IInventory){
		ArrayList<ItemStack> invItemsArray = new ArrayList<ItemStack>();
		ItemStack[] invItems;
		for (int i = 0;i < par1IInventory.getSizeInventory();i++) {
			invItemsArray.add(par1IInventory.getStackInSlot(i));
		}
		invItems = invItemsArray.toArray(new ItemStack[invItemsArray.size()]);

		if (isSharpless) {
			Arrays.sort(invItems, new ComparatorItemStack());
			Arrays.sort(this.material, new ComparatorItemStack());
			invItems = (ItemStack[]) ArrayAuxiliary.deleteNull(invItems);
			if (invItems.length != this.material.length) return null;
			for (int i = 0;i < this.material.length;i++) {
				if (this.material[i].itemID != invItems[i].itemID) return null;
				if (this.material[i].getItemDamage() != invItems[i].getItemDamage()) return null;
			}
			return this.result;
		} else {
			if (this.material.length != invItems.length) return null;
			for (int i = 0;i < this.material.length;i++) {
				if (this.material[i] == null && invItems[i] != null) return null;
				if (this.material[i] != null && invItems[i] == null) return null;
				if (this.material[i] != null && invItems[i] != null){
					if (this.material[i].itemID != invItems[i].itemID) return null;
					if (this.material[i].getItemDamage() != invItems[i].getItemDamage()) return null;
				}
			}
			return this.result;
		}
	}

	/**
	 * NBTが一致するかどうか判定します
	 * @param par1IInventory Inventory
	 * @return 一致したらthis.nbt。それ以外はnull
	 */
	@SuppressWarnings("unchecked")
	public ChemicalNBTRecipe nbtMatch(InventoryChemicalCraftingMaterial par1IInventory){
		ArrayList<ItemStack> invItemsArray = new ArrayList<ItemStack>();
		ItemStack[] invItems;
		for (int i = 0;i < par1IInventory.getSizeInventory();i++) {
			invItemsArray.add(par1IInventory.getStackInSlot(i));
		}
		invItems = invItemsArray.toArray(new ItemStack[invItemsArray.size()]);

		if (isSharpless) {
			Arrays.sort(invItems, new ComparatorItemStack());
			Arrays.sort(this.material, new ComparatorItemStack());
			invItems = (ItemStack[]) ArrayAuxiliary.deleteNull(invItems);
			if (invItems.length != this.material.length) return null;
			for (int i = 0;i < this.material.length;i++) {
				if (this.material[i].itemID != invItems[i].itemID) return null;
				if (this.material[i].getItemDamage() != invItems[i].getItemDamage()) return null;
			}
			return this.nbtRecipe;
		}else{
			if (this.material.length != invItems.length) return null;
			for (int i = 0;i < this.material.length;i++) {
				if (this.material[i] == null && invItems[i] != null) return null;
				if (this.material[i] != null && invItems[i] == null) return null;
				if (this.material[i] != null && invItems[i] != null) {
					if (this.material[i].itemID != invItems[i].itemID) return null;
					if (this.material[i].getItemDamage() != invItems[i].getItemDamage()) return null;
				}
			}
			return this.nbtRecipe;
		}
	}

}
