package pcc.chemicraft.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import pcc.chemicraft.util.Auxiliary.Probability;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * いろいろなデータを格納しメソッドによってチェックするクラス。<br>
 * 使用する場合はItemAtomInfoContainerを継承することを推奨します。
 * @author ponkotate
 */
public final class AtomInfo {

	/**
	 * 気体
	 */
	public static final Integer[] GASES = new Integer[] {
		1, 2, 7, 8, 9, 10, 17, 18, 36, 54, 86
	};


	/**
	 * 液体
	 */
	public static final Integer[] LIQUIDS = new Integer[] {
		35, 80
	};


	/**
	 * 不明
	 */
	public static final Integer[] UNKNOWN = new Integer[] {
		98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118
	};


	public static final Integer[] LANTHANOID = new Integer[] {
		57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71
	};


	public static final Integer[] ACTINOID = new Integer[] {
		89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103
	};


	public static final Integer[] UNOFFICIAL = new Integer[] {
		113, 114, 115, 116, 117, 118
	};


	public static final Integer[] OREOFVANILLA = new Integer[] {
		6, 26, 79
	};


	/**
	 * X, Y, Z座標
	 */
	private  int posX;
	private  int posY;
	private  int posZ;


	/**
	 * biomeの名前
	 */
	private  String biomeName;


	/**
	 * 天候
	 */
	private  String weather;


	/**
	 * EntityPlayerのインスタンス
	 */
	private EntityPlayer entityPlayer;


	/**
	 * Worldのインスタンス
	 */
	private World world;



	/**
	 * AtomInfoのデータをupdateします
	 * @param par1World Worldのインスタンス
	 * @param par2EntityPlayer EntityPlayerのインスタンス
	 */
	public void update(World par1World, EntityPlayer par2EntityPlayer){
		this.posX = (int) par2EntityPlayer.posX;
		this.posY = (int) par2EntityPlayer.posY;
		this.posZ = (int) par2EntityPlayer.posZ;

		this.biomeName = par1World.getBiomeGenForCoords(posX, posZ).biomeName;

		if (par1World.isThundering()) {
			this.weather = "Thunder";
		} else if(par1World.isRaining()) {
			this.weather = "Rain";
		} else {
			this.weather = "Sun";
		}
	}



	public static boolean isGas(int par1) {
		for (int var2:GASES) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isLiquid(int par1) {
		for (int var2:LIQUIDS) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isSolid(int par1) {
		if (!isGas(par1) && !isLiquid(par1) && !isUnknown(par1)) {
			return true;
		}
		return false;
	}



	public static boolean isUnknown(int par1) {
		for (int var2:UNKNOWN) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isLanthanoid(int par1) {
		for (int var2:LANTHANOID) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isActinoid(int par1) {
		for (int var2:ACTINOID) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isUnofficial(int par1) {
		for (int var2:UNOFFICIAL) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isRI(int par1) {
		return collapseUraniumSeries(par1) != -1;
	}



	public static int collapseUraniumSeries(int par1) {
		int var1 = 0;
		Probability var2 = new Probability();
		switch (par1) {
		case 92:
			var1 = 90;
			break;
		case 91:
			var1 = 92;
			break;
		case 90:
			switch (var2.getProbability(1, 1)) {
			case 0:
				var1 = 91;
				break;
			case 1:
				var1 = 88;
				break;
			}
			break;
		case 88:
			var1 = 86;
			break;
		case 86:
			var1 = 84;
			break;
		case 85:
			switch (var2.getProbability(1, 1)) {
			case 0:
				var1 = 86;
				break;
			case 1:
				var1 = 83;
				break;
			}
			break;
		case 84:
			switch (var2.getProbability(1, 1)) {
			case 0:
				var1 = 85;
				break;
			case 1:
				var1 = 82;
				break;
			}
			break;
		case 83:
			switch (var2.getProbability(1, 1)) {
			case 0:
				var1 = 84;
				break;
			case 1:
				var1 = 81;
				break;
			}
			break;
		case 82:
			switch (var2.getProbability(1, 1)) {
			case 0:
				var1 = 83;
				break;
			case 1:
				var1 = 80;
				break;
			}
			break;
		case 81:
			var1 = 82;
			break;
		case 80:
			var1 = 81;
			break;
		default:
			return -1;
		}
		return var1;
	}



	public static boolean isOreOfVanilla(int par1) {
		for (int var2:OREOFVANILLA) {
			if (par1 == var2) {
				return true;
			}
		}
		return false;
	}



	public static boolean isExisting(String par1) {
		for (String var2:OreDictionary.getOreNames()) {
			if ("ore" + par1 == var2) {
				return true;
			}
		}
		return false;
	}



	/**
	 * 引数に指定されたBiomeと同等か比較します
	 * @param biomeName 比較するBiomeの名前
	 * @return Biomeが一致しているか
	 */
	public boolean isEquivalentBiome(String biomeName) {
		if(this.biomeName != null){
			if(this.biomeName.equals(biomeName)){
				return true;
			}else{
				return false;
			}
		}else{
			System.err.println("AtonInfo:データが入っていません。updateメソッドでデータを入れてください");
			return false;
		}
	}



	/**
	 * 引数に指定された天候と同等か比較します
	 * 晴れ:Sun, 雨:Rain, 雷雨:Thunder
	 * @param weather 比較する天候
	 * @return 天候が一致しているか
	 */
	public boolean isEquivalentWeather(String weather) {
		if (this.weather != null) {
			if (this.weather.equals(weather)) {
				return true;
			} else {
				return false;
			}
		} else {
			System.err.println("AtonInfo:データが入っていません。updateメソッドでデータを入れてください");
			return false;
		}
	}



	/**
	 * 引数に指定されたY軸より高いか判定します
	 * @param par1
	 * @return 指定されたY軸より高いか
	 */
	@SideOnly(Side.CLIENT)
	public boolean isOverY(int par1) {
		return this.posY >= par1;
	}



	/**
	 * 引数に指定されたY軸と同等か判定します
	 * @param par1
	 * @return 指定されたY軸と同等かどうか
	 */
	@SideOnly(Side.CLIENT)
	public boolean isEquivalentY(int par1) {
		return this.posY == par1;
	}



	/**
	 * 引数に指定されたY軸より低いか判定します
	 * @param par1
	 * @return 指定されたY軸より低いか
	 */
	@SideOnly(Side.CLIENT)
	public boolean isBelowY(int par1) {
		return this.posY <= par1;
	}

}
