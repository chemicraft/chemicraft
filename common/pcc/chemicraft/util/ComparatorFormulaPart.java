package pcc.chemicraft.util;

import java.util.Comparator;

/**
 * 元素情報を比較します。
 * @author mozipi
 */
public class ComparatorFormulaPart implements Comparator<FormulaPart> {

	@Override
	public int compare(FormulaPart o1, FormulaPart o2) {
		int i = 0;
		byte[] var1Byte = o1.getAtom().getBytes();
		byte[] var2Byte = o2.getAtom().getBytes();
		while (var1Byte.length > i && var2Byte.length > i) {
			if (var1Byte[i] == var2Byte[i]) {
				i++;
				continue;
			}
			if (var1Byte[i] < var2Byte[i]) {
				return -1;
			}
			if (var1Byte[i] > var2Byte[i]) {
				return 1;
			}
		}
		return 0;
	}

}
