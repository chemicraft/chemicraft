package pcc.chemicraft.util;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * 補助クラス
 * @author mozipi,ponkotate
 */
public class Auxiliary {

	public static class NameAuxiliary {

		/**
		 * 名前を設定します
		 * @param object
		 * @param Name
		 */
		public void addName(Object object,Object name){
			LanguageRegistry.addName(object, (String) name);
		}



		/**
		 * 名前を設定します(ItemStack版)
		 * @param object
		 * @param name
		 * @param meta
		 */
		public void addName(ItemStack itemstack,Object name){
			LanguageRegistry.addName(itemstack, (String) name);
		}



		/**
		 * 指定言語にそって名前を追加します
		 * @param object
		 * @param lang
		 * @param name
		 */
		public void addName(Object object,String lang,Object name){
			LanguageRegistry.instance().addNameForObject(object, lang, (String) name);
		}



		/**
		 * 指定言語にそって名前を追加します(ItemStack版)
		 * @param object
		 * @param lang
		 * @param name
		 */
		public void addName(ItemStack object,String lang,Object name){
			LanguageRegistry.instance().addNameForObject(object, lang, (String) name);
		}



		/**
		 * メタデータごとに名前を追加します(Block版)
		 * @param object
		 * @param name
		 */
		public void addName(Block object,Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.addName(new ItemStack(object,0,i), (String) name[i]);
			}
		}



		/**
		 * メタデータごとに名前を追加します(Item版)
		 * @param object
		 * @param name
		 */
		public void addName(Item object,Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.addName(new ItemStack(object,0,i), (String) name[i]);
			}
		}



		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Block版)
		 * @param object
		 * @param name
		 */
		public void addName(Block object,String lang,Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, (String) name[i]);
			}
		}



		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Item版)
		 * @param object
		 * @param name
		 */
		public void addName(Item object,String lang, Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, (String) name[i]);
			}
		}

		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Block版)
		 * また、配列の前に任意の文字列を追加できます。
		 * @param object
		 * @param name
		 */
		public void addName(Block object,String lang, String string, Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, string + (String) name[i]);
			}
		}



		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Item版)
		 * また、配列の前に任意の文字列を追加できます。
		 * @param object
		 * @param name
		 */
		public void addName(Item object,String lang, String string, Object[] name){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, string + (String) name[i]);
			}
		}

		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Block版)
		 * また、配列の後ろに任意の文字列を追加できます。
		 * @param object
		 * @param name
		 */
		public void addName(Block object,String lang, Object[] name, String string){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, (String) name[i] + string);
			}
		}



		/**
		 * 指定された言語にそってメタデータごとに名前を設定します(Item版)
		 * また、配列の後ろに任意の文字列を追加できます。
		 * @param object
		 * @param name
		 */
		public void addName(Item object,String lang, Object[] name, String string){
			for(int i=0;i<name.length;i++){
				LanguageRegistry.instance().addNameForObject(new ItemStack(object,0,i), lang, (String) name[i] + string);
			}
		}

	}

	public static class ArrayAuxiliary{

		public static ItemStack[] deleteNull(ItemStack[] array){
			int count = 0;
			ItemStack[] arrayCopy;
			for(int i = 0;i < array.length;i++){
				if(array[array.length - 1 - i] == null){
					count++;
				}else{
					break;
				}
			}
			arrayCopy = new ItemStack[array.length - count];
			for(int i = 0;i < arrayCopy.length;i++){
				arrayCopy[i] = array[i];
			}
			array = arrayCopy.clone();
			return array;
		}

	}

	public static class Probability {

		/**
		 * Return probability(0,1,2,etc...).
		 */
		public int getProbability(double... par1){
			ArrayList<Double> var1 = new ArrayList<Double>();
			for (int var2 = 0; var2 < par1.length; var2++) {
				var1.add(par1[var2]);
			}
			return getProbability(var1);
		}

		public int getProbability(ArrayList<Double> par1){

			double var1 = 0;

			for (int i = 0; i < par1.size(); i++){
				var1 += par1.get(i);
			}

			double var2 = 0;
			double var3 = Math.random();

			for (int j = 0; j < par1.size(); j++){
				double var4 = par1.get(j) / var1;
				var2 += var4;
				if (var2 >= var3){
					return j;
				}else{
					continue;
				}
			}
			return -1;
		}

	}

	public static class MathAuxiliary {

		public static ArrayList<Entity> getTriangleEntitysByPlayer(World par1World,
				double par2, double par3, double par4,
				double yaw,
				double pitch,
				double angle,
				double rad) {

			ArrayList<Entity> entitys = (ArrayList<Entity>) par1World.loadedEntityList;
			ArrayList<Entity> result = new ArrayList<Entity>();
			for (int i = 0; i < entitys.size(); i++) {
				String a = "";
				Entity entity = entitys.get(i);
				double x = entity.posX;
				double y = entity.posY;
				double z = entity.posZ;
				double bx = par2;
				double by = par3;
				double bz = par4;
				double look = Math.sin(Math.toRadians(yaw)) * 180;
				double lookPitch = Math.abs(pitch);
				double playerToEntityAngleXZ = Math.sin(Math.atan2(bx - x, bz - z)) * 180;
				double playerToEntityAngleY = Math.toDegrees(Math.atan(y - by));
				double length =
						Math.sqrt(
								Math.pow(Math.abs(bx - x), 2) +
								Math.pow(Math.abs(by - y), 2) +
								Math.pow(Math.abs(bz - z), 2));

				if (look + angle/2 > playerToEntityAngleXZ && look - angle/2 < playerToEntityAngleXZ) {
					if (length < rad) {
						result.add(entity);
					}
				}

			}

			return result;
		}

		public static ArrayList<Entity> getTriangleEntitys(World par1World,
				double par2, double par3, double par4,
				double yaw,
				double pitch,
				double angle,
				double rad) {

			ArrayList<Entity> entitys = (ArrayList<Entity>) par1World.loadedEntityList;
			ArrayList<Entity> result = new ArrayList<Entity>();
			for (int i = 0; i < entitys.size(); i++) {
				String a = "";
				Entity entity = entitys.get(i);
				double x = entity.posX;
				double y = entity.posY;
				double z = entity.posZ;
				double bx = par2;
				double by = par3;
				double bz = par4;
				double playerToEntityAngleXZ = Math.sin(Math.atan2(bx - x, bz - z)) * 180;
				double playerToEntityAngleY = Math.toDegrees(Math.atan(y - by));
				double length =
						Math.sqrt(
								Math.pow(Math.abs(bx - x), 2) +
								Math.pow(Math.abs(by - y), 2) +
								Math.pow(Math.abs(bz - z), 2));

				if (playerToEntityAngleXZ + angle/2 > playerToEntityAngleXZ && playerToEntityAngleXZ - angle/2 < playerToEntityAngleXZ) {
					if (length < rad) {
						result.add(entity);
					}
				}

			}

			return result;
		}

	}

}
