package pcc.chemicraft.util;

import java.util.Comparator;

import net.minecraft.item.ItemStack;

/**
 * ItemStackを比較します。
 * @author mozipi
 */
public class ComparatorItemStack implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		ItemStack i1 = (ItemStack) o1;
		ItemStack i2 = (ItemStack) o2;

		if(i1 != null && i2 != null){
			if(i1.itemID == i2.itemID){
				return i1.getItemDamage() > i2.getItemDamage() ? 1 : -1;
			}
			return i1.itemID > i2.itemID ? 1 : -1;
		}
		if(i1 == null && i2 == null) return 0;
		if(i1 != null && i2 == null) return -1;
		if(i1 == null && i2 != null) return 1;
		return 0;
	}

}
