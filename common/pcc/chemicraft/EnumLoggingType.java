package pcc.chemicraft;

public enum EnumLoggingType {

	INFO,
	ERROR,
	WARNING,
	NORMAL;

}
