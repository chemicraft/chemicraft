package pcc.chemicraft;

import java.util.ArrayList;

/**
 * @author P.C.C
 */
public final class ChemiCraftData {

	public static final int HYDROGEN = 0;
	public static final int HELIUM = 1;
	public static final int LITHIUM = 2;
	public static final int BERYLLIUM = 3;
	public static final int BORON = 4;
	public static final int CARBON = 5;
	public static final int NITROGEN = 6;
	public static final int OXYGEN = 7;
	public static final int FLUORINE = 8;
	public static final int NEON = 9;
	public static final int SODIUM = 10;
	public static final int MAGNESIUM = 11;
	public static final int ALMINIUM = 12;
	public static final int SILICON = 13;
	public static final int PHOSPHORUS = 14;
	public static final int SULFUR = 15;
	public static final int CHLORINE = 16;
	public static final int ARGON = 17;
	public static final int POTASSIUM = 18;
	public static final int CALCIUM = 19;
	public static final int SCANDIUM = 20;
	public static final int TITANIUM = 21;
	public static final int VANADIUM = 22;
	public static final int CHROMIUM = 23;
	public static final int MANGANESE = 24;
	public static final int IRON = 25;
	public static final int COBALT = 26;
	public static final int NICKEL = 27;
	public static final int COPPER = 28;
	public static final int ZINC = 29;
	public static final int GALLIUM = 30;
	public static final int GERMANIUM = 31;
	public static final int ARSENIC = 32;
	public static final int SELENIUM = 33;
	public static final int BROMINE = 34;
	public static final int KRYPTON = 35;
	public static final int RUBIDIUM = 36;
	public static final int STRONTIUM = 37;
	public static final int YTTORIUM = 38;
	public static final int ZIRCONIUM = 39;
	public static final int NIOBIUM = 40;
	public static final int MOLYBDENUM = 41;
	public static final int TECHNETIUM = 42;
	public static final int RUTHENIUM = 43;
	public static final int RHODIUM = 44;
	public static final int PALLADIUM = 45;
	public static final int SILVER = 46;
	public static final int CADMIUM = 47;
	public static final int INDIUM = 48;
	public static final int TIN = 49;
	public static final int ANTIMONY = 50;
	public static final int TELLURIUM = 51;
	public static final int IODINE = 52;
	public static final int XENON = 53;
	public static final int CAESIUM = 54;
	public static final int BARIUM = 55;
	public static final int LANTHANUM = 56;
	public static final int CERIUM = 57;
	public static final int PRASEODYMIUM = 58;
	public static final int NEODYMIUM = 59;
	public static final int PROMETHIUM = 60;
	public static final int SAMARIUM = 61;
	public static final int EUROPIUM = 62;
	public static final int GADOLINIUM = 63;
	public static final int TERBIUM = 64;
	public static final int DYSPROSIUM = 65;
	public static final int HOLMIUM = 66;
	public static final int ERBIUM = 67;
	public static final int THULIUM = 68;
	public static final int YTTERBIUM = 69;
	public static final int LUTETIUM = 70;
	public static final int HAFNIUM = 71;
	public static final int TANTALUM = 72;
	public static final int TUNGSTEN = 73;
	public static final int RHENIUM = 74;
	public static final int OSMIUM = 75;
	public static final int IRIDIUM = 76;
	public static final int PLATINUM = 77;
	public static final int GOLD = 78;
	public static final int MERCURY = 79;
	public static final int THALLIUM = 80;
	public static final int LEAD = 81;
	public static final int BISMUTH = 82;
	public static final int POLONIUM = 83;
	public static final int ASTATINE = 84;
	public static final int RADON = 85;
	public static final int FRANCIUM = 86;
	public static final int RADIUM = 87;
	public static final int ACTINIUM = 88;
	public static final int THORIUM = 89;
	public static final int PROTACTINIUM = 90;
	public static final int URANIUM = 91;
	public static final int NEPTUNIUM = 92;
	public static final int PLUTONIUM = 93;
	public static final int AMERICIUM = 94;
	public static final int CURIUM = 95;
	public static final int BERKELIUM = 96;
	public static final int CALIforNIUM = 97;
	public static final int EINSTEINIUM = 98;
	public static final int FERMIUM = 99;
	public static final int MENDILEVIUM = 100;
	public static final int NOBELIUM = 101;
	public static final int LAWRENCIUM = 102;
	public static final int RUTHERforDIUM = 103;
	public static final int DUBNIUM = 104;
	public static final int SEABORGIUM = 105;
	public static final int BOHRIUM = 106;
	public static final int HASSIUM = 107;
	public static final int MEITNERIUM = 108;
	public static final int DARMSTADTIUM = 109;
	public static final int ROENTGENIUM = 110;
	public static final int COPERNICIUM = 111;
	public static final int UNUNTRIUM = 112;
	public static final int UNUNQUADIUM = 113;
	public static final int UNUNPENTIUM = 114;
	public static final int UNUNHEXIUM = 115;
	public static final int UNUNSEPTIUM = 116;
	public static final int UNUNOCTIUM = 117;

	public static final String HYDROGEN_SIGN = "H";
	public static final String HELIUM_SIGN = "He";
	public static final String LITHIUM_SIGN = "Li";
	public static final String BERYLLIUM_SIGN = "Be";
	public static final String BORON_SIGN = "B";
	public static final String CARBON_SIGN = "C";
	public static final String NITROGEN_SIGN = "N";
	public static final String OXYGEN_SIGN = "O";
	public static final String FLUORINE_SIGN = "F";
	public static final String NEON_SIGN = "Ne";
	public static final String SODIUM_SIGN = "Na";
	public static final String MAGNESIUM_SIGN = "Mg";
	public static final String ALMINIUM_SIGN = "Al";
	public static final String SILICON_SIGN = "Si";
	public static final String PHOSPHORUS_SIGN = "P";
	public static final String SULFUR_SIGN = "S";
	public static final String CHLORINE_SIGN = "Cl";
	public static final String ARGON_SIGN = "Ar";
	public static final String POTASSIUM_SIGN = "K";
	public static final String CALCIUM_SIGN = "Ca";
	public static final String SCANDIUM_SIGN = "Sc";
	public static final String TITANIUM_SIGN = "Ti";
	public static final String VANADIUM_SIGN = "V";
	public static final String CHROMIUM_SIGN = "Cr";
	public static final String MANGANESE_SIGN = "Mn";
	public static final String IRON_SIGN = "Fe";
	public static final String COBALT_SIGN = "Co";
	public static final String NICKEL_SIGN = "Ni";
	public static final String COPPER_SIGN = "Cu";
	public static final String ZINC_SIGN = "Zn";
	public static final String GALLIUM_SIGN = "Ga";
	public static final String GERMANIUM_SIGN = "Ge";
	public static final String ARSENIC_SIGN = "As";
	public static final String SELENIUM_SIGN = "Se";
	public static final String BROMINE_SIGN = "Br";
	public static final String KRYPTON_SIGN = "Kr";
	public static final String RUBIDIUM_SIGN = "Rb";
	public static final String STRONTIUM_SIGN = "Sr";
	public static final String YTTORIUM_SIGN = "Y";
	public static final String ZIRCONIUM_SIGN = "Zr";
	public static final String NIOBIUM_SIGN = "Nb";
	public static final String MOLYBDENUM_SIGN = "Mo";
	public static final String TECHNETIUM_SIGN = "Tc";
	public static final String RUTHENIUM_SIGN = "Ru";
	public static final String RHODIUM_SIGN = "Rh";
	public static final String PALLADIUM_SIGN = "Pd";
	public static final String SILVER_SIGN = "Ag";
	public static final String CADMIUM_SIGN = "Cd";
	public static final String INDIUM_SIGN = "In";
	public static final String TIN_SIGN = "Sn";
	public static final String ANTIMONY_SIGN = "Sb";
	public static final String TELLURIUM_SIGN = "Te";
	public static final String IODINE_SIGN = "I";
	public static final String XENON_SIGN = "Xe";
	public static final String CAESIUM_SIGN = "Cs";
	public static final String BARIUM_SIGN = "Ba";
	public static final String LANTHANUM_SIGN = "La";
	public static final String CERIUM_SIGN = "Ce";
	public static final String PRASEODYMIUM_SIGN = "Pr";
	public static final String NEODYMIUM_SIGN = "Nd";
	public static final String PROMETHIUM_SIGN = "Pm";
	public static final String SAMARIUM_SIGN = "Sm";
	public static final String EUROPIUM_SIGN = "Eu";
	public static final String GADOLINIUM_SIGN = "Gd";
	public static final String TERBIUM_SIGN = "Tb";
	public static final String DYSPROSIUM_SIGN = "Dy";
	public static final String HOLMIUM_SIGN = "Ho";
	public static final String ERBIUM_SIGN = "Er";
	public static final String THULIUM_SIGN = "Tm";
	public static final String YTTERBIUM_SIGN = "Yb";
	public static final String LUTETIUM_SIGN = "Lu";
	public static final String HAFNIUM_SIGN = "Hf";
	public static final String TANTALUM_SIGN = "Ta";
	public static final String TUNGSTEN_SIGN = "W";
	public static final String RHENIUM_SIGN = "Re";
	public static final String OSMIUM_SIGN = "Os";
	public static final String IRIDIUM_SIGN = "Ir";
	public static final String PLATINUM_SIGN = "Pt";
	public static final String GOLD_SIGN = "Au";
	public static final String MERCURY_SIGN = "Hg";
	public static final String THALLIUM_SIGN = "Tl";
	public static final String LEAD_SIGN = "Pb";
	public static final String BISMUTH_SIGN = "Bi";
	public static final String POLONIUM_SIGN = "Po";
	public static final String ASTATINE_SIGN = "At";
	public static final String RADON_SIGN = "Rn";
	public static final String FRANCIUM_SIGN = "Fr";
	public static final String RADIUM_SIGN = "Ra";
	public static final String ACTINIUM_SIGN = "Ac";
	public static final String THORIUM_SIGN = "Th";
	public static final String PROTACTINIUM_SIGN = "Pa";
	public static final String URANIUM_SIGN = "U";
	public static final String NEPTUNIUM_SIGN = "Np";
	public static final String PLUTONIUM_SIGN = "Pu";
	public static final String AMERICIUM_SIGN = "Am";
	public static final String CURIUM_SIGN = "Cm";
	public static final String BERKELIUM_SIGN = "Bk";
	public static final String CALIforNIUM_SIGN = "Cf";
	public static final String EINSTEINIUM_SIGN = "Es";
	public static final String FERMIUM_SIGN = "Fm";
	public static final String MENDILEVIUM_SIGN = "Md";
	public static final String NOBELIUM_SIGN = "No";
	public static final String LAWRENCIUM_SIGN = "Lr";
	public static final String RUTHERforDIUM_SIGN = "Rf";
	public static final String DUBNIUM_SIGN = "Db";
	public static final String SEABORGIUM_SIGN = "Sg";
	public static final String BOHRIUM_SIGN = "Bh";
	public static final String HASSIUM_SIGN = "Hs";
	public static final String MEITNERIUM_SIGN = "Mt";
	public static final String DARMSTADTIUM_SIGN = "Ds";
	public static final String ROENTGENIUM_SIGN = "Rg";
	public static final String COPERNICIUM_SIGN = "Cn";
	public static final String UNUNTRIUM_SIGN = "Uut";
	public static final String UNUNQUADIUM_SIGN = "Uuq";
	public static final String UNUNPENTIUM_SIGN = "Uup";
	public static final String UNUNHEXIUM_SIGN = "Uuh";
	public static final String UNUNSEPTIUM_SIGN = "Uus";
	public static final String UNUNOCTIUM_SIGN = "Uuo";

	public static final String[] ATOMSLIST = {
		"H",  "He",  "Li",  "Be",  "B",  "C",  "N",  "O",  "F",  "Ne",
		"Na",  "Mg",  "Al",  "Si",  "P",  "S",  "Cl",  "Ar",  "K",  "Ca",
		"Sc",  "Ti",  "V",  "Cr",  "Mn",  "Fe",  "Co",  "Ni",  "Cu",  "Zn",
		"Ga",  "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr",
		"Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
		"Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd",
		"Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb",
		"Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
		"Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th",
		"Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm",
		"Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds",
		"Rg", "Cn", "Uut", "Uuq", "Uup", "Uuh", "Uus", "Uuo"
	};

	public static int toAtoms(String sign){
		for (int i = 0;i < ATOMSLIST.length;i++){
			if(ATOMSLIST[i].equals(sign)){
				return i;
			}
		}
		throw new IllegalArgumentException(String.format("Illegal toAtoms %s",sign));
	}

	public static Integer[] toAtoms(String[] signs){
		ArrayList<Integer> atoms = new ArrayList<Integer>();
		for (int i = 0;i < signs.length; i++)
		{
			atoms.add((Integer)toAtoms(signs[i]));
		}
		return atoms.toArray(new Integer[0]);
	}

	public static String toSign(int atoms){
		return ATOMSLIST[atoms];
	}

}
