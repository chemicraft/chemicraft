/*
package pcc.chemicraft.testcase;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import pcc.chemicraft.util.Formula;

public class FormulaTest extends Formula {

	public FormulaTest() {
		super("");
	}

	@Test
	public void test8() {
		Formula formula_Together = new Formula("Si12Si3C2O8C4");
		check(formula_Together.getAtoms(), formula_Together.getAmonts(), new String[] {"Si", "C", "O"}, new Integer[] {12+3, 2+4, 8});
	}

	@Test
	public void test7() {
		Formula formula_first_no_ten__other_ten = new Formula("Si6C11O56");
		check(formula_first_no_ten__other_ten.getAtoms(), formula_first_no_ten__other_ten.getAmonts(), new String[] {"Si", "C", "O"}, new Integer[] {6, 11, 56});
	}

	@Test
	public void test6() {
		Formula formula_multiple_and_ten = new Formula("Si13O47");
		check(formula_multiple_and_ten.getAtoms(), formula_multiple_and_ten.getAmonts(), new String[] {"Si", "O"}, new Integer[] {13, 47});
	}

	@Test
	public void test5() {
		Formula formula_ten = new Formula("Si13");
		check(formula_ten.getAtoms(), formula_ten.getAmonts(), new String[] {"Si"}, new Integer[] {13});
	}

	@Test
	public void test4() {
		Formula formula_firstSimple__other_In_Numer = new Formula("SiC2O2");
		check(formula_firstSimple__other_In_Numer.getAtoms(), formula_firstSimple__other_In_Numer.getAmonts(), new String[] {"Si", "C", "O"}, new Integer[] {1, 2, 2});
	}

	@Test
	public void test3() {
		Formula formula_multiple_and_in_number = new Formula("Si2C2O2");
		check(formula_multiple_and_in_number.getAtoms(), formula_multiple_and_in_number.getAmonts(), new String[] {"Si", "C", "O"}, new Integer[] {2, 2, 2});
	}

	@Test
	public void test2() {
		Formula formula_multiple = new Formula("SiCO");
		check(formula_multiple.getAtoms(), formula_multiple.getAmonts(), new String[] {"Si", "C", "O"}, new Integer[] {1, 1, 1});
	}

	@Test
	public void test1() {
		Formula formula_in_number = new Formula("Si3");
		check(formula_in_number.getAtoms(), formula_in_number.getAmonts(), new String[] {"Si"}, new Integer[] {3});
	}

	@Test
	public void test() {
		Formula formula_simple = new Formula("Si");
		check(formula_simple.getAtoms(), formula_simple.getAmonts(), new String[] {"Si"}, new Integer[] {1});
	}

	public void check(String[] atoms, Integer[] amounts, String[] expectaionAtoms, Integer[] expectationAmounts) {
		assertEquals("変換がおかしいです(元素)",
				atoms,
				expectaionAtoms);

		assertEquals("変換がおかしいです(元素数)",
				amounts,
				expectationAmounts);
	}

}
*/
