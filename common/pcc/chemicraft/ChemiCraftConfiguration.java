package pcc.chemicraft;

/**
 * @author mozipi
 */
public class ChemiCraftConfiguration {

	/**
	 * the Now ID.
	 */
	private int nowID;

	public ChemiCraftConfiguration(int baseID) {
		this.nowID = baseID;
	}

	/**
	 * 現在のIDを取得します。
	 * @return 現在のID
	 */
	public int getNowID() {
		return this.nowID;
	}

	/**
	 * IDを1つ追加します
	 * @return 追加後のID
	 */
	public int additionID() {
		return ++this.nowID;
	}

}
