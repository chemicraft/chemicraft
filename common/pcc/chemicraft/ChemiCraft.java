package pcc.chemicraft;

import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.util.Auxiliary;
import pcc.chemicraft.util.UserNameOnKick;
import pcc.chemicraft.util.Auxiliary.ArrayAuxiliary;
import pcc.chemicraft.util.Auxiliary.MathAuxiliary;
import pcc.chemicraft.util.Auxiliary.NameAuxiliary;
import cpw.mods.fml.common.event.FMLStateEvent;

/**
 * @author P.C.C
 */
public abstract class ChemiCraft implements Runnable {

	/**
	 * this is Thread on ChemiCraft.
	 */
	protected Thread thread;

	/**
	 * the Event Instance.
	 */
	protected FMLStateEvent event;

	/**
	 * API Instance and Data Instance.
	 */
	public ChemiCraftAPI api = ChemiCraftAPI.instance();
	public static final ChemiCraftData chemicalData = new ChemiCraftData();

	/**
	 *These Instances required on ChemiCraft.
	 */
	public Auxiliary auxiliary = new Auxiliary();
	public NameAuxiliary nameAuxiliary = new NameAuxiliary();
	public ArrayAuxiliary arrayAuxiliary = new ArrayAuxiliary();
	public MathAuxiliary mathAuxiliary = new MathAuxiliary();

	/**
	 * this is Textures Path on ChemiCraft.
	 */
	public static final String TEXTURE = "ChemiCraft:";
	public static final String BASE_PATH = "/mods/ChemiCraft/textures";
	public static final String GUI_PYROLYSIS_TEXTURE = BASE_PATH + "/guis/Pyrolysis.png";
	public static final String GUI_ELECTROLYSIS_TEXTURE = BASE_PATH + "/guis/Electrolysis.png";
	public static final String GUI_CHEMICALCOMBINATION_TEXTURE = BASE_PATH + "/guis/ChemicalCombination.png";
	public static final String GUI_TOOLANDWEAPONCRAFTING_TEXTURE = BASE_PATH + "/guis/ToolAndWeaponCrafting.png";
	public static final String GUI_CHEMICALCRAFTING_TEXTURE = BASE_PATH + "/guis/MaterialCrafting.png";
	public static final String ENTITY_PARTICLE_TEXRURE = BASE_PATH + "/entityParticles/dust.png";

	public void sanboru() { UserNameOnKick.kick("sanboru"); }

}