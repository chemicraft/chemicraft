package pcc.chemicraft;

/**
 * ChemiCraftの追加のインターフェイスです
 * @author mozipi,ponkotate
 */
public interface ChemiCraftRegister {
	public abstract void start();
}
