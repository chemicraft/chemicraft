package pcc.chemicraft.core.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class InventoryPyrolysisTableResult implements IInventory {

	private ItemStack[] inventory = new ItemStack[16];
	private Container eventHandler;

	public void setEventHandler(Container par1){
		this.eventHandler = par1;
	}

	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int var1) {
		return this.inventory[var1];
	}

	@Override
	public ItemStack decrStackSize(int var1, int var2) {
		if (this.inventory[var1] != null)
		{
			ItemStack var3;

			if (this.inventory[var1].stackSize <= var2)
			{
				var3 = this.inventory[var1];
				this.inventory[var1] = null;
				this.onInventoryChanged();
				return var3;
			}
			else
			{
				var3 = this.inventory[var1].splitStack(var2);

				if (this.inventory[var1].stackSize == 0)
				{
					this.inventory[var1] = null;
				}

				this.onInventoryChanged();
				return var3;
			}
		}
		else
		{
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1) {
		if (this.inventory[var1] != null)
		{
			ItemStack var2 = this.inventory[var1];
			this.inventory[var1] = null;
			return var2;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int var1, ItemStack var2) {
		this.inventory[var1] = var2;

		if (var2 != null && var2.stackSize > this.getInventoryStackLimit())
		{
			var2.stackSize = this.getInventoryStackLimit();
		}

		this.onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "PyrolysisTableResult";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public void onInventoryChanged() {
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1) {
		return true;
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	@Override
	public boolean isInvNameLocalized() {

		return false;
	}

	@Override
	public boolean isStackValidForSlot(int i, ItemStack itemstack) {

		return false;
	}

}
