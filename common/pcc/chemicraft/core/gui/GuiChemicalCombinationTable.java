package pcc.chemicraft.core.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.container.ContainerChemicalCombinationTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import cpw.mods.fml.common.network.PacketDispatcher;

public class GuiChemicalCombinationTable extends GuiContainer {

	private GuiTextField atomList;

	private TileEntityChemicalCombinationTable tileentity;

	public GuiChemicalCombinationTable(EntityPlayer par1EntityPlayer, TileEntityChemicalCombinationTable par2) {
		super(new ContainerChemicalCombinationTable(par1EntityPlayer, par2));
		this.tileentity = par2;
		this.ySize = 204;
	}

	@Override
	public void initGui(){
		super.initGui();
		this.atomList = new GuiTextField(this.fontRenderer, 9, 103, 160, 15);
		this.atomList.setFocused(false);
		this.atomList.setMaxStringLength(25);
		this.buttonList.add(new GuiButton(0, this.guiLeft + 172 - 41, this.guiTop + 80 - 21, 40, 20, "Add"));
		this.buttonList.add(new GuiButton(1, this.guiLeft + 172 - 41, this.guiTop + 80, 40, 20, "Eject"));
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		this.mc.renderEngine.bindTexture(ChemiCraftCore.instance.GUI_CHEMICALCOMBINATION_TEXTURE);
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}

	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
		if(par1GuiButton.id == 0){
			this.tileentity.field_00001();
			PacketDispatcher.sendPacketToServer(this.tileentity.getDescriptionPacket());
		}else if(par1GuiButton.id == 1){
			this.tileentity.field_00002();
			PacketDispatcher.sendPacketToServer(this.tileentity.getDescriptionPacket());
		}
		return;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2) {
		this.atomList.setText(this.tileentity.getAtoms());
		this.atomList.drawTextBox();
	}

}
