package pcc.chemicraft.core.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.container.ContainerToolAndWeaponCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;

public class GuiToolAndWeaponCraftingTable extends GuiContainer {

	public GuiToolAndWeaponCraftingTable(EntityPlayer par1EntityPlayer, TileEntityToolAndWeaponCraftingTable par2) {
		super(new ContainerToolAndWeaponCraftingTable(par1EntityPlayer, par2));
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		this.mc.renderEngine.bindTexture(ChemiCraftCore.instance.GUI_TOOLANDWEAPONCRAFTING_TEXTURE);
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}


}
