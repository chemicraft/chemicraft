package pcc.chemicraft.core.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.container.ContainerChemicalCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiChemicalCraftingTable extends GuiContainer {

	public GuiChemicalCraftingTable(EntityPlayer par1EntityPlayer, TileEntityChemicalCraftingTable par2){
		super(new ContainerChemicalCraftingTable(par1EntityPlayer, par2));
	}



	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3){
		this.mc.renderEngine.bindTexture(ChemiCraftCore.instance.GUI_CHEMICALCRAFTING_TEXTURE);
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}

}
