package pcc.chemicraft.core.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.container.ContainerElectrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;

public class GuiElectrolysisTable extends GuiContainer
{

	private TileEntityElectrolysisTable tileentity;

	public GuiElectrolysisTable(EntityPlayer par1EntityPlayer, TileEntityElectrolysisTable par2)
	{
		super(new ContainerElectrolysisTable(par1EntityPlayer, par2));
		this.tileentity = par2;
		this.ySize = 204;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3)
	{
		this.mc.renderEngine.bindTexture(ChemiCraftCore.instance.GUI_ELECTROLYSIS_TEXTURE);
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}

}
