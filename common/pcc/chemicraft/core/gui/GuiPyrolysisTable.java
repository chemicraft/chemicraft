package pcc.chemicraft.core.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.container.ContainerPyrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;

public class GuiPyrolysisTable extends GuiContainer{

	private TileEntityPyrolysisTable tileentity;

	public GuiPyrolysisTable(EntityPlayer par1EntityPlayer, TileEntityPyrolysisTable par2) {
		super(new ContainerPyrolysisTable(par1EntityPlayer, par2));
		this.tileentity = par2;
		this.ySize = 204;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		this.mc.renderEngine.bindTexture(ChemiCraftCore.instance.GUI_PYROLYSIS_TEXTURE);
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
		this.drawTexturedModalRect(this.guiLeft + 28, this.guiTop + 48, 176, 0, 13, (int) (13F / this.tileentity.getFuelRestTime()));

	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2) {
		this.fontRenderer.drawString(String.valueOf("Temp:" + (int)this.tileentity.getHeat()), 110, 100, 0x000000);
	}

}
