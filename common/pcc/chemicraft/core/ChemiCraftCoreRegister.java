package pcc.chemicraft.core;

import pcc.chemicraft.ChemiCraftRegister;

/**
 * ChemiCraftCoreの追加の親クラスです
 * @author mozipi,ponkotate
 */
public abstract class ChemiCraftCoreRegister implements ChemiCraftRegister {

	protected ChemiCraftCore mod;

	public ChemiCraftCoreRegister(ChemiCraftCore mod) {
		this.mod = mod;
	}

}
