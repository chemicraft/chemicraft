package pcc.chemicraft.core;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import pcc.chemicraft.ChemiCraftData;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * レシピを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterCoreRecipe extends ChemiCraftCoreRegister {

	public ChemiCraftRegisterCoreRecipe(ChemiCraftCore mod) {
		super(mod);
	}

	@Override
	public void start() {
		GameRegistry.addRecipe(new ItemStack(this.mod.itemGasCollectingBottle),
				new Object[]{
			" X ", "X X", "XXX",
			Character.valueOf('X'), new ItemStack(Item.ingotIron),
		});

		//科学作業台のレシピ
		GameRegistry.addRecipe(new ItemStack(this.mod.blockPyrolysisTable),
				new Object[]{
			"XXX", "ZAZ", "ZYZ",
			Character.valueOf('X'), new ItemStack(Block.stone),
			Character.valueOf('Y'), new ItemStack(Item.bucketLava),
			Character.valueOf('Z'), new ItemStack(Item.ingotGold),
			Character.valueOf('A'), new ItemStack(this.mod.blockElectrolysisTable),
		});

		GameRegistry.addRecipe(new ItemStack(this.mod.blockElectrolysisTable),
				new Object[]{
			"XAX", "YZY", "ZZZ",
			Character.valueOf('X'), new ItemStack(Item.redstone),
			Character.valueOf('Y'), new ItemStack(this.mod.itemChemicalCells),
			Character.valueOf('Z'), new ItemStack(Item.ingotGold),
			Character.valueOf('A'), new ItemStack(Block.workbench),
		});

		GameRegistry.addRecipe(new ItemStack(this.mod.blockChemicalCombinationTable),
				new Object[]{
			"XYX", "ZAZ", "XYX",
			Character.valueOf('X'), new ItemStack(Item.ingotGold),
			Character.valueOf('Y'), new ItemStack(Item.ingotIron),
			Character.valueOf('Z'), new ItemStack(this.mod.itemAtoms, 1, this.mod.chemicalData.toAtoms("U")),
			Character.valueOf('A'), new ItemStack(Block.hopperBlock),
		});

		/*
		GameRegistry.addRecipe(new ItemStack(this.mod.blockToolAndWeaponCraftingTable),
				new Object[]{
			"XYX", "ZAZ", "ZZZ",
			Character.valueOf('X'), new ItemStack(Item.ingotIron),
			Character.valueOf('Y'), new ItemStack(Block.workbench),
			Character.valueOf('Z'), new ItemStack(Block.stone),
			Character.valueOf('A'), new ItemStack(Item.pickaxeSteel),
		});
		 */

		GameRegistry.addRecipe(new ItemStack(this.mod.blockChemicalCraftingTable),
				new Object[]{
			"XXX", "ZAZ", "ZZZ",
			Character.valueOf('X'), new ItemStack(this.mod.itemGasCollectingBottle),
			Character.valueOf('Y'), new ItemStack(Item.dyePowder, 1, 4),
			Character.valueOf('Z'), new ItemStack(Block.stone),
			Character.valueOf('A'), new ItemStack(Block.workbench),
		});

		GameRegistry.addRecipe(new ItemStack(this.mod.itemChemicalCells),
				new Object[]{
			" X ", "ZYZ", "ZYZ",
			Character.valueOf('X'), new ItemStack(Item.redstone),
			Character.valueOf('Y'), new ItemStack(Item.ingotGold),
			Character.valueOf('Z'), new ItemStack(Item.ingotIron),
		});

		//素材制作台のレシピ
		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						null,
						null,
						null,
						new ItemStack(this.mod.itemAtoms, 1, this.mod.chemicalData.ZINC),
						null,
						new ItemStack(this.mod.itemAtoms, 1, this.mod.chemicalData.COPPER),
						new ItemStack(Item.bucketWater),
						new ItemStack(Item.bucketWater),
						new ItemStack(Item.bucketWater),
				},
				new ItemStack(this.mod.itemChemicalCells),
				null);

		this.mod.api.addMaterialRecipe(
				new ItemStack[] {
						new ItemStack(this.mod.itemAtoms, 1, ChemiCraftData.toAtoms("Na")),
						new ItemStack(this.mod.itemAtoms, 1, ChemiCraftData.toAtoms("C")),
						new ItemStack(this.mod.itemAtoms, 1, ChemiCraftData.toAtoms("I")),
						null,
						new ItemStack(Item.appleRed),
						null,
						null,
						null,
						null,
				},
				new ItemStack(this.mod.itemPear),
				null
				);

	}

}
