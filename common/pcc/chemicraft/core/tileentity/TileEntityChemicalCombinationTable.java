package pcc.chemicraft.core.tileentity;

import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;
import pcc.chemicraft.ChemiCraftData;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.inventory.InventoryChemicalCombinationTableMaterial;
import pcc.chemicraft.core.inventory.InventoryChemicalCombinationTableResult;
import pcc.chemicraft.core.system.PacketHandler;
import pcc.chemicraft.util.ComparatorItemStack;

import com.google.common.io.ByteArrayDataInput;

import cpw.mods.fml.common.network.PacketDispatcher;

public class TileEntityChemicalCombinationTable extends TileEntity {

	private InventoryChemicalCombinationTableMaterial invm = new InventoryChemicalCombinationTableMaterial();
	private InventoryChemicalCombinationTableResult invr = new InventoryChemicalCombinationTableResult();

	private ArrayList<String> atomsList = new ArrayList<String>();
	private ArrayList<Integer> atomsAmountList = new ArrayList<Integer>();
	private int atomsListSize = 0;

	private ItemStack dropItems;

	private boolean isProvidePower;

	@Override
	public void updateEntity(){
		super.updateEntity();
		if (this.isProvidePower && !this.worldObj.isRemote) {
			this.field_00001();
			this.isProvidePower = false;
			PacketDispatcher.sendPacketToAllPlayers(this.getDescriptionPacket());
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound par1){
		super.readFromNBT(par1);
		NBTTagList var2 = par1.getTagList("Items");
		for (int var3 = 0; var3 < var2.tagCount(); ++var3)
		{
			NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
			int var5 = var4.getByte("Slot") & 255;

			if  (var5 >= 0 && var5 < this.invm.getSizeInventory())
			{
				this.invm.setInventorySlotContents(var5, ItemStack.loadItemStackFromNBT(var4));
			}
		}

		NBTTagList var6 = par1.getTagList("Items2");
		for (int var3 = 0; var3 < var6.tagCount(); ++var3)
		{
			NBTTagCompound var7 = (NBTTagCompound)var6.tagAt(var3);
			int var8 = var7.getByte("Slot2") & 255;

			if  (var8 >= 0 && var8 < this.invr.getSizeInventory())
			{
				this.invr.setInventorySlotContents(var8, ItemStack.loadItemStackFromNBT(var7));
			}
		}

		this.atomsListSize = par1.getInteger("atomsListSize");
		for(int i = 0;i < this.atomsListSize;i++){
			this.atomsList.add(par1.getString("atomsList" + i));
		}
		for(int i = 0;i < this.atomsListSize;i++){
			this.atomsAmountList.add(par1.getInteger("atomsAmountList" + i));
		}

		this.isProvidePower = par1.getBoolean("isProvidePower");
	}

	@Override
	public void writeToNBT(NBTTagCompound par1){
		super.writeToNBT(par1);
		NBTTagList var2 = new NBTTagList();
		for (int var3 = 0; var3 < this.invm.getSizeInventory(); ++var3)
		{
			if  (this.invm.getStackInSlot(var3) != null)
			{
				NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte)var3);
				this.invm.getStackInSlot(var3).writeToNBT(var4);
				var2.appendTag(var4);
			}
		}
		par1.setTag("Items", var2);

		NBTTagList var5 = new NBTTagList();
		for (int var6 = 0; var6 < this.invr.getSizeInventory(); ++var6)
		{
			if  (this.invr.getStackInSlot(var6) != null)
			{
				NBTTagCompound var7 = new NBTTagCompound();
				var7.setByte("Slot2", (byte)var6);
				this.invr.getStackInSlot(var6).writeToNBT(var7);
				var5.appendTag(var7);
			}
		}
		par1.setTag("Items2", var5);

		this.atomsList.trimToSize();
		this.atomsAmountList.trimToSize();
		for(int i = 0;i < this.atomsList.size();i++){
			par1.setString("atomsList" + i, this.atomsList.get(i));
		}
		for(int i = 0;i < this.atomsAmountList.size();i++){
			par1.setInteger("atomsAmountList" + i, this.atomsAmountList.get(i));
		}

		this.atomsListSize = this.atomsList.size();
		par1.setInteger("atomsListSize", this.atomsListSize);

		par1.setBoolean("isProvidePower", this.isProvidePower);
	}

	@Override
	public Packet getDescriptionPacket() {
		return PacketHandler.getPacket(this);
	}

	public void readPacket(ByteArrayDataInput data) {
		try {
			for(int i = 0;i < this.invm.getSizeInventory();i++){
				int id = data.readInt();
				int size = data.readByte();
				int damage = data.readInt();
				if (id != 0 && size != 0){
					this.invm.setInventorySlotContents(i, new ItemStack(id, size, damage));
				}else{
					this.invm.setInventorySlotContents(i, null);
				}
			}

			for(int i = 0;i < this.invr.getSizeInventory();i++){
				int id = data.readInt();
				int size = data.readByte();
				int damage = data.readInt();
				if (id != 0 && size != 0){
					this.invr.setInventorySlotContents(i, new ItemStack(id, size, damage));
				}else{
					this.invr.setInventorySlotContents(i, null);
				}
			}

			this.atomsListSize = data.readInt();
			this.atomsList.clear();
			this.atomsAmountList.clear();
			for(int i = 0;i < this.atomsListSize;i++){
				this.atomsList.add(data.readUTF());
			}
			for(int i = 0;i < this.atomsListSize;i++){
				this.atomsAmountList.add(data.readInt());
			}

			ItemStack itemstack = new ItemStack(data.readInt(), data.readByte(), data.readInt());
			if(itemstack.itemID != 0 && !this.worldObj.isRemote){
				this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.xCoord, this.yCoord + 1, this.zCoord, itemstack));
			}
			this.dropItems = null;

			this.isProvidePower = data.readBoolean();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void writePacket(DataOutputStream dos){
		try {
			for(int i = 0;i < this.invm.getSizeInventory();i++){
				int id = 0;
				int size = 0;
				int damage  = 0;
				ItemStack itemstack = this.invm.getStackInSlot(i);
				if (itemstack != null){
					id = itemstack.itemID;
					size = itemstack.stackSize;
					damage = itemstack.getItemDamage();
					dos.writeInt(id);
					dos.writeByte(size);
					dos.writeInt(damage);
				}else{
					dos.writeInt(0);
					dos.writeByte(0);
					dos.writeInt(0);
				}
			}

			for(int i = 0;i < this.invr.getSizeInventory();i++){
				int id = 0;
				int size = 0;
				int damage  = 0;
				ItemStack itemstack = this.invr.getStackInSlot(i);
				if (itemstack != null){
					id = itemstack.itemID;
					size = itemstack.stackSize;
					damage = itemstack.getItemDamage();
					dos.writeInt(id);
					dos.writeByte(size);
					dos.writeInt(damage);
				}else{
					dos.writeInt(0);
					dos.writeByte(0);
					dos.writeInt(0);
				}
			}

			dos.writeInt(this.atomsList.size());
			for(int i = 0;i < this.atomsList.size();i++){
				dos.writeUTF(this.atomsList.get(i));
			}
			for(int i = 0;i < this.atomsAmountList.size();i++){
				dos.writeInt(this.atomsAmountList.get(i));
			}

			if (this.dropItems != null){
				int id = this.dropItems.itemID;
				int size = this.dropItems.stackSize;
				int damage = this.dropItems.getItemDamage();
				dos.writeInt(id);
				dos.writeByte(size);
				dos.writeInt(damage);
			}else{
				dos.writeInt(0);
				dos.writeByte(0);
				dos.writeInt(0);
			}
			this.dropItems = null;

			dos.writeBoolean(this.isProvidePower);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setAtoms(String par1, int par2){
		if (this.atomsList.indexOf(par1) != -1){
			int var1 = this.atomsList.indexOf(par1);
			this.atomsAmountList.set(var1, this.atomsAmountList.get(var1) + par2);
		}else{
			this.atomsList.add(par1);
			this.atomsAmountList.add(par2);
		}
		PacketDispatcher.sendPacketToAllPlayers(this.getDescriptionPacket());
	}

	public void setDropItems(ItemStack par1ItemStack){
		this.dropItems = par1ItemStack;
	}

	public String getAtoms(){
		StringBuffer var1 = new StringBuffer();
		for(int i = 0;i < this.atomsList.size();i++){
			if (this.atomsAmountList.get(i) != 1){
				var1.append(this.atomsList.get(i) + this.atomsAmountList.get(i));
			}else{
				var1.append(this.atomsList.get(i));
			}
		}
		return var1.toString();
	}

	public ArrayList<String> getAtomsList(){
		return this.atomsList;
	}

	public ArrayList<Integer> getAtomsAmountList(){
		return this.atomsAmountList;
	}

	public void emptytoAtomsList(){
		this.atomsList.clear();
		this.atomsAmountList.clear();
	}

	public void setProvidePower(boolean par1) {
		this.isProvidePower = par1;
	}

	public boolean getProvidePower() {
		return this.isProvidePower;
	}

	public void field_00001() {
		IInventory inv = this.getInvMaterial();
		ItemStack[] stacks = new ItemStack[inv.getSizeInventory()];
		for(int i = 0;i < stacks.length;i++){
			if(inv.getStackInSlot(i) != null){
				stacks[i] = inv.getStackInSlot(i);
			}
		}
		Arrays.sort(stacks, new ComparatorItemStack());
		stacks = ChemiCraftCore.instance.arrayAuxiliary.deleteNull(stacks);

		for(int i = 0;i < stacks.length;i++){
			if(stacks[i].stackSize > 1){
				this.setAtoms(ChemiCraftData.ATOMSLIST[stacks[i].getItemDamage()], stacks[i].stackSize);
			}else{
				this.setAtoms(ChemiCraftData.ATOMSLIST[stacks[i].getItemDamage()], 1);
			}
		}

		for(int i = 0;i < inv.getSizeInventory();i++){
			inv.setInventorySlotContents(i, null);
		}
	}

	public void field_00002() {
		ArrayList<String> var1 = this.getAtomsList();
		ArrayList<Integer> var2 = this.getAtomsAmountList();

		for (int i = 0;i < var1.size();i++) {
			for (int j = 0;j < ChemiCraftData.ATOMSLIST.length;j++) {
				if (var1.get(i).equals(ChemiCraftData.ATOMSLIST[j])) {
					int var3 = var2.get(i);
					while(var3 > 0){
						if(var3 > 64){
							ItemStack itemstack = new ItemStack(ChemiCraftCore.instance.itemAtoms, 64, j);
							this.setDropItems(itemstack);
							PacketDispatcher.sendPacketToServer(this.getDescriptionPacket());
							var3 -= 64;
						}else{
							ItemStack itemstack = new ItemStack(ChemiCraftCore.instance.itemAtoms, var3, j);
							this.setDropItems(itemstack);
							PacketDispatcher.sendPacketToServer(this.getDescriptionPacket());
							var3 = 0;
						}
					}
					break;
				}
			}
		}
		IInventory invr = this.getInvResult();
		if (invr.getStackInSlot(0) != null) {
			invr.setInventorySlotContents(0, null);
		}
		this.emptytoAtomsList();
	}

	protected boolean isNumber(String par1){
		try {
			int var1 = Integer.valueOf(new String(par1));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public InventoryChemicalCombinationTableMaterial getInvMaterial(){
		return this.invm;
	}

	public InventoryChemicalCombinationTableResult getInvResult(){
		return this.invr;
	}

}
