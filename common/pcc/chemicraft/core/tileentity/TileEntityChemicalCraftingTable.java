package pcc.chemicraft.core.tileentity;

import java.io.DataOutputStream;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingMaterial;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingNBT;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingResult;
import pcc.chemicraft.core.system.PacketHandler;

import com.google.common.io.ByteArrayDataInput;

public class TileEntityChemicalCraftingTable extends TileEntity {

	public IInventory chemicalCraftingInvMaterial = new InventoryChemicalCraftingMaterial();
	public IInventory chemicalCraftingInvResult = new InventoryChemicalCraftingResult();
	public IInventory chemicalCraftingInvNBT = new InventoryChemicalCraftingNBT();


	public TileEntityChemicalCraftingTable() {
		super();
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
	}


	@Override
	public void readFromNBT(NBTTagCompound par1) {
		super.readFromNBT(par1);

		NBTTagList var2 = par1.getTagList("Items");
		for (int var3 = 0; var3 < var2.tagCount(); ++var3)
		{
			NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
			int var5 = var4.getByte("Slot") & 255;

			if (var5 >= 0 && var5 < this.chemicalCraftingInvMaterial.getSizeInventory())
			{
				this.chemicalCraftingInvMaterial.setInventorySlotContents(var5, ItemStack.loadItemStackFromNBT(var4));
			}
		}

		NBTTagList var6 = par1.getTagList("Items2");
		for (int var7 = 0; var7 < var6.tagCount(); ++var7)
		{
			NBTTagCompound var4 = (NBTTagCompound)var6.tagAt(var7);
			int var5 = var4.getByte("Slot2") & 255;

			if (var5 >= 0 && var5 < this.chemicalCraftingInvResult.getSizeInventory())
			{
				this.chemicalCraftingInvResult.setInventorySlotContents(var5, ItemStack.loadItemStackFromNBT(var4));
			}
		}

		NBTTagList var8 = par1.getTagList("Items3");
		for (int var9 = 0; var9 < var8.tagCount(); ++var9)
		{
			NBTTagCompound var11 = (NBTTagCompound)var8.tagAt(var9);
			int var10 = var11.getByte("Slot3") & 255;

			if (var10 >= 0 && var10 < this.chemicalCraftingInvNBT.getSizeInventory())
			{
				this.chemicalCraftingInvNBT.setInventorySlotContents(var10, ItemStack.loadItemStackFromNBT(var11));
			}
		}
	}


	@Override
	public void writeToNBT(NBTTagCompound par1) {
		super.writeToNBT(par1);

		NBTTagList var2 = new NBTTagList();
		for (int var3 = 0; var3 < this.chemicalCraftingInvMaterial.getSizeInventory(); ++var3)
		{
			if (this.chemicalCraftingInvMaterial.getStackInSlot(var3) != null)
			{
				NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte)var3);
				this.chemicalCraftingInvMaterial.getStackInSlot(var3).writeToNBT(var4);
				var2.appendTag(var4);
			}
		}
		par1.setTag("Items", var2);

		NBTTagList var5 = new NBTTagList();
		for (int var6 = 0; var6 < this.chemicalCraftingInvResult.getSizeInventory(); ++var6)
		{
			if (this.chemicalCraftingInvResult.getStackInSlot(var6) != null)
			{
				NBTTagCompound var7 = new NBTTagCompound();
				var7.setByte("Slot2", (byte)var6);
				this.chemicalCraftingInvResult.getStackInSlot(var6).writeToNBT(var7);
				var5.appendTag(var7);
			}
		}
		par1.setTag("Items2", var5);

		NBTTagList var8 = new NBTTagList();
		for (int var9 = 0; var9 < this.chemicalCraftingInvNBT.getSizeInventory(); ++var9)
		{
			if (this.chemicalCraftingInvNBT.getStackInSlot(var9) != null)
			{
				NBTTagCompound var10 = new NBTTagCompound();
				var10.setByte("Slot3", (byte)var9);
				this.chemicalCraftingInvNBT.getStackInSlot(var9).writeToNBT(var10);
				var8.appendTag(var10);
			}
		}
		par1.setTag("Items3", var8);
	}


	@Override
	public Packet getDescriptionPacket() {
		return PacketHandler.getPacket(this);
	}


	public void readPacket(ByteArrayDataInput data) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void writePacket(DataOutputStream dos){
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
