package pcc.chemicraft.core.tileentity;

import java.io.DataOutputStream;
import java.util.Iterator;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableFuel;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableMaterial;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableResult;
import pcc.chemicraft.core.system.PacketHandler;

import com.google.common.io.ByteArrayDataInput;

public class TileEntityPyrolysisTable extends TileEntity {

	private InventoryPyrolysisTableMaterial invm = new InventoryPyrolysisTableMaterial();
	private InventoryPyrolysisTableResult invr = new InventoryPyrolysisTableResult();
	private InventoryPyrolysisTableFuel invf = new InventoryPyrolysisTableFuel();

	/**
	 * 熱量
	 */
	private float heat = 0;

	/**
	 * 素材の加熱時間
	 */
	private float burnTime = 0;

	/**
	 * 燃料の残り燃焼時間
	 */
	private float fuelRestTime = 0;

	/**
	 * 最大熱量
	 */
	private static final int MAX_HEAT = 1700;

	/**
	 * 燃料が燃え始める最低熱量
	 */
	private static final int MIN_HEAT = 400;

	/**
	 * 燃料の燃焼速度比率・素材の燃焼速度比率の中心となる熱量
	 */
	private static final int CENTER_HEAT = 1050;

	/**
	 * 分解までの時間
	 */
	private static final int MAX_BURN_TIME = 2000;

	/**
	 * Random of instance.
	 */
	public static final Random rand = new Random();

	@Override
	public void updateEntity() {
		// boolean var2 = false;

		boolean var1 = this.worldObj.canBlockSeeTheSky(xCoord, yCoord + 1, zCoord);
		if (this.worldObj.getBlockId(xCoord, yCoord-1, zCoord) == Block.fire.blockID) {
			if (this.heat < 1700) {
				if (this.worldObj.isRaining() && var1 && !this.worldObj.isThundering()) {
					this.heat += 0.1F;
				} else if (this.worldObj.isThundering() && var1) {
					this.heat += 0.25F;
				} else {
					this.heat += 0.5F;
				}
			} else {
				this.heat = 1700;
			}
		} else {
			if (this.heat > 0) {
				if(this.worldObj.isRaining() && var1 && !this.worldObj.isThundering()) {
					this.heat -= 0.25F;
				} else if(this.worldObj.isThundering() && var1) {
					this.heat -= 0.5F;
				} else {
					this.heat -= 0.05F;
				}
			} else {
				this.heat = 0;
			}
		}

		if (this.fuelRestTime >= 0) {
			this.burnTime += 10 * (this.heat / CENTER_HEAT);
			this.fuelRestTime -= 10 * (this.heat / CENTER_HEAT);
		}

		if (this.burnTime >= MAX_BURN_TIME) {
			Iterator<ItemStack> itMaterial = ChemiCraftAPI.instance().getPyrolysisRecipeList().keySet().iterator();
			while (itMaterial.hasNext()) {
				ItemStack itemstackMaterial = itMaterial.next();
				ItemStack materialItem = this.invm.getStackInSlot(0);
				if (materialItem != null) {
					if (this.isInItemCheck(itemstackMaterial)) {
						//var2 = true;
						if (itemstackMaterial.itemID == materialItem.itemID && itemstackMaterial.getItemDamage() == materialItem.getItemDamage()) {
							this.inItem(itemstackMaterial);
							if (materialItem.stackSize > 1) {
								this.invm.setInventorySlotContents(0, new ItemStack(materialItem.itemID, --materialItem.stackSize, materialItem.getItemDamage()));
							} else {
								this.invm.setInventorySlotContents(0, null);
							}
							break;
						}
					}
				}
			}
			this.burnTime = 0;
		}

		boolean var3 = false;
		if (this.heat > MIN_HEAT) {
			Iterator<ItemStack> itMaterial = ChemiCraftAPI.instance().getPyrolysisRecipeList().keySet().iterator();
			while (itMaterial.hasNext()) {
				ItemStack itemstackMaterial = itMaterial.next();
				ItemStack materialItem = this.invm.getStackInSlot(0);
				if (materialItem != null) {
					if (this.isInItemCheck(itemstackMaterial)) {
						//var2 = true;
						if (itemstackMaterial.itemID == materialItem.itemID && itemstackMaterial.getItemDamage() == materialItem.getItemDamage()) {
							var3 = true;
							if (this.fuelRestTime <= 0) {
								Iterator<ItemStack> itFuel = ChemiCraftAPI.instance().getPyrolysisFuelList().keySet().iterator();
								while (itFuel.hasNext()) {
									ItemStack itemstackFuel = itFuel.next();
									ItemStack fuelItem = this.invf.getStackInSlot(0);
									if (fuelItem != null) {
										if (itemstackFuel.itemID == fuelItem.itemID && itemstackFuel.getItemDamage() == fuelItem.getItemDamage()) {
											this.fuelRestTime = ChemiCraftAPI.instance().getPyrolysisFuelList().get(itemstackFuel);
											if (fuelItem.stackSize > 1) {
												this.invf.setInventorySlotContents(0, new ItemStack(fuelItem.itemID, --fuelItem.stackSize, fuelItem.getItemDamage()));
												break;
											} else {
												this.invf.setInventorySlotContents(0, null);
												break;
											}
										}
									}
								}
							}
						}
					}
				} else {
					this.burnTime = 0;
					//var2 = false;
				}
			}
			if (!var3) {
				this.burnTime = 0;
			}
		}

		/*
		DebugTick.setDebugData("PyrolysisTable" +
				"(x:" + this.xCoord +
				" y:" + this.yCoord +
				" z:" + this.zCoord + ")",
				new DebugData("Heat", this.heat),
				new DebugData("FuelRestTime", this.fuelRestTime),
				new DebugData("BurnTime", this.burnTime),
				new DebugData("アイテムが入るか", var2)
				);
		 */
	}

	@Override
	public void readFromNBT(NBTTagCompound par1) {
		super.readFromNBT(par1);
		this.heat = par1.getFloat("Heat");
		this.burnTime = par1.getFloat("BurnTime");
		this.fuelRestTime = par1.getFloat("FuelRestTime");

		NBTTagList var2 = par1.getTagList("Items");
		for (int var3 = 0; var3 < var2.tagCount(); ++var3)
		{
			NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
			int var5 = var4.getByte("Slot") & 255;

			if  (var5 >= 0 && var5 < this.invm.getSizeInventory())
			{
				this.invm.setInventorySlotContents(var5, ItemStack.loadItemStackFromNBT(var4));
			}
		}

		NBTTagList var6 = par1.getTagList("Items2");
		for (int var3 = 0; var3 < var6.tagCount(); ++var3)
		{
			NBTTagCompound var7 = (NBTTagCompound)var6.tagAt(var3);
			int var8 = var7.getByte("Slot2") & 255;

			if  (var8 >= 0 && var8 < this.invr.getSizeInventory())
			{
				this.invr.setInventorySlotContents(var8, ItemStack.loadItemStackFromNBT(var7));
			}
		}

		NBTTagList var9 = par1.getTagList("Items3");
		for (int var3 = 0; var3 < var9.tagCount(); ++var3)
		{
			NBTTagCompound var10 = (NBTTagCompound)var9.tagAt(var3);
			int var11 = var10.getByte("Slot3") & 255;

			if  (var11 >= 0 && var11 < this.invf.getSizeInventory())
			{
				this.invf.setInventorySlotContents(var11, ItemStack.loadItemStackFromNBT(var10));
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound par1) {
		super.writeToNBT(par1);
		par1.setFloat("Heat", this.heat);
		par1.setFloat("BurnTime", this.burnTime);
		par1.setFloat("FuelRestTime", this.fuelRestTime);

		NBTTagList var2 = new NBTTagList();
		for (int var3 = 0; var3 < this.invm.getSizeInventory(); ++var3)
		{
			if  (this.invm.getStackInSlot(var3) != null)
			{
				NBTTagCompound var4 = new NBTTagCompound();
				var4.setByte("Slot", (byte)var3);
				this.invm.getStackInSlot(var3).writeToNBT(var4);
				var2.appendTag(var4);
			}
		}
		par1.setTag("Items", var2);

		NBTTagList var5 = new NBTTagList();
		for (int var6 = 0; var6 < this.invr.getSizeInventory(); ++var6)
		{
			if  (this.invr.getStackInSlot(var6) != null)
			{
				NBTTagCompound var7 = new NBTTagCompound();
				var7.setByte("Slot2", (byte)var6);
				this.invr.getStackInSlot(var6).writeToNBT(var7);
				var5.appendTag(var7);
			}
		}
		par1.setTag("Items2", var5);

		NBTTagList var8 = new NBTTagList();
		for (int var9 = 0; var9 < this.invf.getSizeInventory(); ++var9)
		{
			if  (this.invf.getStackInSlot(var9) != null)
			{
				NBTTagCompound var10 = new NBTTagCompound();
				var10.setByte("Slot3", (byte)var9);
				this.invf.getStackInSlot(var9).writeToNBT(var10);
				var8.appendTag(var10);
			}
		}
		par1.setTag("Items3", var8);
	}

	@Override
	public Packet getDescriptionPacket() {
		return PacketHandler.getPacket(this);
	}

	public void readPacket(ByteArrayDataInput data) {
		try {
			this.heat = data.readFloat();
			this.burnTime = data.readInt();
			this.fuelRestTime = data.readFloat();
			for(int i = 0;i < this.invm.getSizeInventory();i++){
				int id = data.readInt();
				int size = data.readByte();
				int damage = data.readInt();
				if (id != 0 && size != 0){
					this.invm.setInventorySlotContents(i, new ItemStack(id, size, damage));
				}else{
					this.invm.setInventorySlotContents(i, null);
				}
			}

			for(int i = 0;i < this.invr.getSizeInventory();i++){
				int id = data.readInt();
				int size = data.readByte();
				int damage = data.readInt();
				if (id != 0 && size != 0){
					this.invr.setInventorySlotContents(i, new ItemStack(id, size, damage));
				}else{
					this.invr.setInventorySlotContents(i, null);
				}
			}

			for(int i = 0;i < this.invf.getSizeInventory();i++){
				int id = data.readInt();
				int size = data.readByte();
				int damage = data.readInt();
				if (id != 0 && size != 0){
					this.invf.setInventorySlotContents(i, new ItemStack(id, size, damage));
				}else{
					this.invf.setInventorySlotContents(i, null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void writePacket(DataOutputStream dos){
		try {
			dos.writeFloat(this.heat);
			dos.writeFloat(this.burnTime);
			dos.writeFloat(this.fuelRestTime);
			for(int i = 0;i < this.invm.getSizeInventory();i++){
				int id = 0;
				int size = 0;
				int damage  = 0;
				ItemStack itemstack = this.invm.getStackInSlot(i);
				if (itemstack != null){
					id = itemstack.itemID;
					size = itemstack.stackSize;
					damage = itemstack.getItemDamage();
					dos.writeInt(id);
					dos.writeByte(size);
					dos.writeInt(damage);
				}else{
					dos.writeInt(0);
					dos.writeByte(0);
					dos.writeInt(0);
				}
			}

			for(int i = 0;i < this.invr.getSizeInventory();i++){
				int id = 0;
				int size = 0;
				int damage  = 0;
				ItemStack itemstack = this.invr.getStackInSlot(i);
				if (itemstack != null){
					id = itemstack.itemID;
					size = itemstack.stackSize;
					damage = itemstack.getItemDamage();
					dos.writeInt(id);
					dos.writeByte(size);
					dos.writeInt(damage);
				}else{
					dos.writeInt(0);
					dos.writeByte(0);
					dos.writeInt(0);
				}
			}

			for(int i = 0;i < this.invf.getSizeInventory();i++){
				int id = 0;
				int size = 0;
				int damage  = 0;
				ItemStack itemstack = this.invf.getStackInSlot(i);
				if (itemstack != null){
					id = itemstack.itemID;
					size = itemstack.stackSize;
					damage = itemstack.getItemDamage();
					dos.writeInt(id);
					dos.writeByte(size);
					dos.writeInt(damage);
				}else{
					dos.writeInt(0);
					dos.writeByte(0);
					dos.writeInt(0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public float getBurnTime() {
		return this.burnTime;
	}

	public float getHeat() {
		return this.heat;
	}

	public InventoryPyrolysisTableMaterial getInvMaterial() {
		return this.invm;
	}

	public InventoryPyrolysisTableResult getInvResult() {
		return this.invr;
	}

	public InventoryPyrolysisTableFuel getInvFuel() {
		return this.invf;
	}

	public boolean isInItemCheck(ItemStack key) {
		ItemStack[] results = ChemiCraftAPI.instance().getPyrolysisRecipeList().get(key).clone();
		ItemStack[] containerResults = new ItemStack[this.invr.getSizeInventory()];
		for (int j = 0; j < this.invr.getSizeInventory(); j++) {
			containerResults[j] = this.invr.getStackInSlot(j);
		}

		for (int i = 0; i < results.length; i++) {
			int var1 = results[i].itemID;
			int var3 = results[i].getItemDamage();
			int var5 = results[i].stackSize;
			for (int j = 0; j < containerResults.length; j++) {
				if (containerResults[j] == null) {
					containerResults[j] = results[i];
					results[i] = null;
					break;
				} else {
					int var2 = containerResults[j].itemID;
					int var4 = containerResults[j].getItemDamage();
					int var6 = containerResults[j].stackSize;
					int var7 = containerResults[j].getMaxStackSize();
					if (var1 == var2 && var3 == var4) {
						if (var5 + var6 <= var7) {
							containerResults[j] = results[i];
							results[i] = null;
							break;
						} else {
							var5 -= var7 - var6;
						}
					}
				}
			}
		}
		for (int i = 0; i < results.length; i++) {
			if (results[i] != null) {
				return false;
			}
		}
		return true;
	}

	public void inItem(ItemStack key) {
		ItemStack[] results = ChemiCraftAPI.instance().getPyrolysisRecipeList().get(key);

		for (int i = 0; i < results.length; i++) {
			ItemStack[] containerResults = new ItemStack[this.invr.getSizeInventory()];
			for (int j = 0; j < this.invr.getSizeInventory(); j++) {
				containerResults[j] = this.invr.getStackInSlot(j);
			}
			int var1 = results[i].itemID;
			int var3 = results[i].getItemDamage();
			int var5 = results[i].stackSize;
			for (int j = 0; j < containerResults.length; j++) {
				if (containerResults[j] == null) {
					ItemStack result = new ItemStack(var1, var5, var3);
					this.invr.setInventorySlotContents(j, result);
					break;
				} else {
					int var2 = containerResults[j].itemID;
					int var4 = containerResults[j].getItemDamage();
					int var6 = containerResults[j].stackSize;
					int var7 = containerResults[j].getMaxStackSize();
					if (var1 == var2 && var3 == var4) {
						if (var5 + var6 <= var7) {
							this.invr.setInventorySlotContents(j, new ItemStack(results[i].itemID, var5 + var6, results[i].getItemDamage()));
							break;
						} else {
							this.invr.setInventorySlotContents(j, new ItemStack(results[i].itemID, var7, results[i].getItemDamage()));
							var5 -= var7 - var6;
						}
					}
				}
			}
		}
	}

	public float getFuelRestTime() {
		return fuelRestTime;
	}

}
