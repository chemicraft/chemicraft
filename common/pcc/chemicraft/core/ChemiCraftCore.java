package pcc.chemicraft.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EntityDamageSource;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.ChemiCraftConfiguration;
import pcc.chemicraft.ChemiCraftLogging;
import pcc.chemicraft.ChemiCraftRegister;
import pcc.chemicraft.EnumLoggingType;
import pcc.chemicraft.core.creativetab.CreativeTabChemiCraft;
import pcc.chemicraft.core.debug.CommandDeleteItem;
import pcc.chemicraft.core.debug.CommandGenDebugRoom;
import pcc.chemicraft.core.debug.CommandSetTile;
import pcc.chemicraft.core.system.CommonProxy;
import pcc.chemicraft.core.system.PacketHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.LoaderState.ModState;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.Mod.ServerStarting;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * ChemiCraft本体
 * @author P.C.C.
 */
@Mod(modid = "ChemiCraftCore", name = "ChemiCraft", version = "Beta1")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, versionBounds = "1.5", channels = "chemicraftcore", packetHandler = PacketHandler.class)
public class ChemiCraftCore extends ChemiCraft{

	/**
	 * 元素名配列
	 */
	public static final String[] ATOMSNAME = {
		"Hydrogen", "Helium", "Lithium", "Beryllium", "Boron", "Carbon", "Nitrogen", "Oxygen", "Fluorine", "Neon", "Sodium", "Magnesium", "Aluminium", "Silicon",
		"Phosphorus", "Sulfur", "Chlorine", "Argon", "Potassium", "Calcium", "Scandium", "Titanium", "Vanadium", "Chromium", "Manganese", "Iron", "Cobalt", "Nickel", "Copper", "Zinc", "Gallium",
		"Germanium", "Arsenic", "Selenium", "Bromine", "Krypton", "Rubidium", "Strontium", "Yttorium", "Zirconium", "Niobium", "Molybdenum", "Technetium", "Ruthenium", "Rhodium", "Palladium",
		"Silver", "Cadmium", "Indium", "Tin", "Antimony", "Tellurium", "Iodine", "Xenon", "Caesium", "Barium", "Lanthanum", "Cerium", "Praseodymium", "Neodymium", "Promethium", "Samarium",
		"Europium", "Gadolinium", "Terbium", "Dysprosium", "Holmium", "Erbium", "Thulium", "Ytterbium", "Lutetium", "Hafnium", "Tantalum", "Tungsten", "Rhenium", "Osmium", "Iridium", "Platinum",
		"Gold", "Mercury", "Thallium", "Lead", "Bismuth", "Polonium", "Astatine", "Radon", "Francium", "Radium", "Actinium", "Thorium", "Protactinium", "Uranium", "Neptunium", "Plutonium",
		"Americium", "Curium", "Berkelium", "Californium", "Einsteinium", "Fermium", "Mendelevium", "Nobelium", "Lawrencium", "Rutherfordium", "Dubnium", "Seaborgium", "Bohrium", "Hassium",
		"Meitnerium", "Darmstadtium", "Roentgenium", "Copernicium", "Ununtrium", "Flerovium", "Ununpentium", "Livermorium", "Ununseptium", "Ununoctium" };

	/**
	 * 元素名日本語版配列
	 */
	public static final String[] ATOMSNAMEJP = {
		"水素", "ヘリウム", "リチウム", "ベリリウム", "ホウ素", "炭素", "窒素", "酸素", "フッ素", "ネオン", "ナトリウム", "マグネシウム", "アルミニウム", "ケイ素", "リン", "硫黄", "塩素", "アルゴン", "カリウム", "カルシウム", "スカンジウム",
		"チタン", "バナジウム", "クロム", "マンガン", "鉄", "コバルト", "ニッケル", "銅", "亜鉛", "ガリウム", "ゲルマニウム", "ヒ素", "セレン", "臭素", "クリプトン", "ルビジウム", "ストロンチウム", "イットリウム", "ジルコニウム", "ニオブ", "モリブデン", "テクネチウム", "ルテニウム",
		"ロジウム", "パラジウム", "銀", "カドミウム", "インジウム", "スズ", "アンチモン", "テルル", "ヨウ素", "キセノン", "セシウム", "バリウム", "ランタン", "セリウム", "プラセオジム", "ネオジム", "プロメチウム", "サマリウム", "ユウロビウム", "ガドリニウム", "テルビウム", "ジスプロニウム",
		"ホルミウム", "エルビウム", "ツリウム", "イッテルビウム", "ルテチウム", "ハフニウム", "タンタル", "タングステン", "レニウム", "オスミウム", "イリジウム", "白金", "金", "水銀", "タリウム", "鉛", "ビスマス", "ポロニウム", "アスタチン", "ラドン", "フランシウム", "ラジウム",
		"アクチニウム", "トリウム", "プロトアクチウム", "ウラン", "ネプツニウム", "プルトニウム", "アメリシウム", "キュリウム", "バークリウム", "カルホルニウム", "アインスタイニウム", "フェルミウム", "メンデレビウム", "ノーベリウム", "ローレンシウム", "ラサホージウム", "ドブニウム", "シーボーギウム",
		"ボーリウム", "ハッシウム", "マイトネリウム", "ダームスタチウム", "レントゲニウム", "コペルニシウム", "ウンウントリウム", "フレロビウム", "ウンウンペンチウム", "リバモリウム", "ウンウンセプチウム", "ウンウンオクチウム" };

	/**
	 * this is ChemiCraft instance.
	 */
	@Instance("ChemiCraftCore")
	public static ChemiCraftCore instance;

	/**
	 * proxy of ChemiCraft.
	 */
	@SidedProxy(clientSide = "pcc.chemicraft.core.client.ClientProxy", serverSide = "pcc.chemicraft.core.system.CommonProxy")
	public static CommonProxy proxy;

	/**
	 * CreativeTab of ChemiCraft.
	 */
	public static final CreativeTabs creativeTabChemiCraft = new CreativeTabChemiCraft("ChemiCraft");

	/**
	 * the ItemID.
	 */
	public int atomsID;
	public int compoundsID;
	public int gasCollectingBottleID;
	public int chemicalCellsID;
	public int pearID;

	/**
	 * the BlockID.
	 */
	public int pyrolysisTableID;
	public int electrolysisTableID;
	public int chemicalConbinationTableID;
	public int toolAndWeaponCraftingTableID;
	public int chemicalCraftingTableID;

	/**
	 * the GUIID.
	 */
	public int guiPyrolysisTableID;
	public int guiElectrolysisTableID;
	public int guiChemicalCombinationTableID;
	public int guiToolAndWeaponCraftingTableID;
	public int guiChemicalCraftingTableID;

	/**
	 * Variables of Block type.
	 */
	public Block blockPyrolysisTable;
	public Block blockElectrolysisTable;
	public Block blockChemicalCombinationTable;
	public Block blockToolAndWeaponCraftingTable;
	public Block blockChemicalCraftingTable;

	/**
	 * Variables of Item type.
	 */
	public Item itemAtoms;
	public Item itemCompounds;
	public Item itemGasCollectingBottle;
	public Item itemChemicalCells;
	public Item itemPear;

	/**
	 * the Register Instances.
	 */
	public ChemiCraftRegister registerItem;
	public ChemiCraftRegister registerBlock;
	public ChemiCraftRegister registerTileEntity;
	public ChemiCraftRegister registerRecipe;

	/**
	 * Logging用のインスタンス
	 */
	public static final ChemiCraftLogging logger = new ChemiCraftLogging(System.getProperty("user.dir"));

	/**
	 * the Textures
	 */
	public static final String CCTABLE = ChemiCraft.TEXTURE + "ChemiCraftTable_";

	public ChemiCraftCore() {
		this.sanboru();
		this.api.getCompoundsName().createHash("en_US");
		this.registerItem = new ChemiCraftRegisterItem(this);
		this.registerBlock = new ChemiCraftRegisterBlock(this);
		this.registerTileEntity = new ChemiCraftRegisterTileEntity(this);
		this.registerRecipe = new ChemiCraftRegisterCoreRecipe(this);
		this.logger.startLogging();
		//this.sinVisualizer  = new SinVisualizer();
	}

	/**
	 * PreInit:
	 * Configをロードします。
	 * @param event アノテーション呼び出しにより呼び出す必要なし
	 */
	@PreInit
	public void chemiPreLoadMethod(FMLPreInitializationEvent event) {
		Configuration cfg = new Configuration(event.getSuggestedConfigurationFile());
		cfg.load();

		Property coreBlockID = cfg.get("BlockID", "Base of Block ID", 2400);
		Property coreItemID = cfg.get("ItemID", "Base of Item ID", 25000);

		ChemiCraftConfiguration ccfgBlock = new ChemiCraftConfiguration(coreBlockID.getInt());
		ChemiCraftConfiguration ccfgItem = new ChemiCraftConfiguration(coreItemID.getInt());

		this.pyrolysisTableID = ccfgBlock.additionID();
		this.electrolysisTableID = ccfgBlock.additionID();
		this.chemicalConbinationTableID = ccfgBlock.additionID();
		this.toolAndWeaponCraftingTableID = ccfgBlock.additionID();
		this.chemicalCraftingTableID = ccfgBlock.additionID();

		this.atomsID = ccfgItem.additionID();
		this.compoundsID = ccfgItem.additionID();
		this.gasCollectingBottleID = ccfgItem.additionID();
		this.chemicalCellsID = ccfgItem.additionID();
		this.pearID = ccfgItem.additionID();

		Property guiPyrolysisTableIDProp = cfg.get("GUI", "GUIPyrolysisID", 1000);
		Property guiElectrolysisTableIDProp = cfg.get("GUI", "GUIElectrolysisTableIDProp", 1001);
		Property guiChemicalCombinationTableIDProp = cfg.get("GUI", "GUIChemicalCombinationTableID", 1002);
		Property guiToolAndWeaponCraftingTableIDProp = cfg.get("GUI", "GUIToolAndWeaponCraftingTableID", 1003);
		Property guiChemicalCraftingTableIDProp = cfg.get("GUI", "GUIChemicalCraftingTableID", 1004);

		this.guiPyrolysisTableID = guiPyrolysisTableIDProp.getInt();
		this.guiElectrolysisTableID = guiElectrolysisTableIDProp.getInt();
		this.guiChemicalCombinationTableID = guiChemicalCombinationTableIDProp.getInt();
		this.guiToolAndWeaponCraftingTableID = guiToolAndWeaponCraftingTableIDProp.getInt();
		this.guiChemicalCraftingTableID = guiChemicalCraftingTableIDProp.getInt();

		cfg.save();
	}



	@ServerStarting
	public void serverStarting(final FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandSetTile());
		event.registerServerCommand(new CommandDeleteItem());
		event.registerServerCommand(new CommandGenDebugRoom());
	}



	@Init
	public void chemiPostLoadMethod(final FMLInitializationEvent event) {
		this.thread = new Thread(this);
		this.event = event;
		this.thread.start();

		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		proxy.registerRenderInformation();
		Thread.yield();
	}



	@Override
	public void run() {
		while (true) {
			if (proxy != null && instance != null) {
				this.settingProcessing((FMLInitializationEvent) event);
				this.debug((FMLInitializationEvent) event);
				break;
			}
		}

		Thread loadCheckThread = new Thread() {
			@Override
			public void run() {
				while (true) {
					List<ModContainer> mod = Loader.instance().getModList();
					ModContainer finalMod = mod.get(mod.size()-1);
					ModState finalModState = Loader.instance().getModState(finalMod);
					if (finalModState == ModState.POSTINITIALIZED) {
						ChemiCraftCore.logger.write("ChemiCraftCore>APIProcessing", EnumLoggingType.INFO);
						apiProcessing((FMLInitializationEvent) event);
						break;
					}
				}
			}
		};
		loadCheckThread.start();

	}



	protected void settingProcessing(final FMLInitializationEvent event) {

		this.registerItem.start();
		this.registerBlock.start();
		this.registerRecipe.start();
		this.registerTileEntity.start();

		// GUIを追加します
		NetworkRegistry.instance().registerGuiHandler(instance, proxy);

		this.api.addPyrolysisDecompositionFuel(new ItemStack(Item.coal), 2000*8);
		this.api.addElectrolysisDecompositionFuel(new ItemStack(this.itemChemicalCells), 1000*8);
	}



	private void apiProcessing(final FMLInitializationEvent event) {
		Iterator<String> langcompoundsItr = this.api.getCompoundsName().keySet().iterator();
		while (langcompoundsItr.hasNext()) {
			String lang = langcompoundsItr.next();
			ArrayList<String> names = this.api.getCompoundsName().get(lang);
			for (int i = 0; i < names.size(); i++) {
				ChemiCraftCore.logger.write("CompoundAddName:" + "Name-" + names.get(i) + "/Damage-" + i + "/Lang-" + lang,
						EnumLoggingType.INFO);

				LanguageRegistry.instance().addNameForObject(
						new ItemStack(this.itemCompounds, 1, i),
						lang,
						names.get(i));
			}
		}

	}



	public static EntityDamageSource getRadiationDamageSource(Entity par1Entity){
		return new EntityDamageSource("radiation", par1Entity){
			@Override
			public String getDeathMessage(EntityLiving par1EntityLiving) {
				if (par1EntityLiving instanceof EntityPlayer) {
					return ((EntityPlayer) (par1EntityLiving)).username + " is dead by radiation.";
				} else {
					return par1EntityLiving.getEntityName() + " is dead by radiation.";
				}
			}
		};
	}



	private void debug(final FMLInitializationEvent event) {
		//this.proxy.registerTickHandler();
	}

}
