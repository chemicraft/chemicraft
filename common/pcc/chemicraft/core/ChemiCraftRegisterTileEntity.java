package pcc.chemicraft.core;

import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * TileEntityを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterTileEntity extends ChemiCraftCoreRegister {

	public ChemiCraftRegisterTileEntity(ChemiCraftCore mod) {
		super(mod);
	}

	@Override
	public void start() {
		GameRegistry.registerTileEntity(TileEntityPyrolysisTable.class, "TileEntityPyrolysisTable");
		GameRegistry.registerTileEntity(TileEntityChemicalCombinationTable.class, "TileEntityChemicalCombinationTable");
		GameRegistry.registerTileEntity(TileEntityToolAndWeaponCraftingTable.class, "TileEntityToolAndWeaponCraftingTable");
		GameRegistry.registerTileEntity(TileEntityChemicalCraftingTable.class, "TileEntityChemicalCraftingTable");
		GameRegistry.registerTileEntity(TileEntityElectrolysisTable.class, "TileEntityElectrolysisTable");
	}

}
