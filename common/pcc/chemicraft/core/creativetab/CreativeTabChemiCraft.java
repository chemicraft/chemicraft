package pcc.chemicraft.core.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import pcc.chemicraft.core.ChemiCraftCore;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class CreativeTabChemiCraft extends CreativeTabs {

	public CreativeTabChemiCraft(String type){
		super(type);
	}



	@Override
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex(){
		return ChemiCraftCore.instance.itemGasCollectingBottle.itemID;
	}



	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel(){
		return "ChemiCraft";
	}

}
