package pcc.chemicraft.core.block;

import java.util.Random;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;

public class BlockChemicalCombinationTable extends BlockContainer {

	public Icon topIcon;
	public Icon bottomIcon;
	public Icon globalIcon;

	private final Random random = new Random();

	public BlockChemicalCombinationTable(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
	}



	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
		par5EntityPlayer.openGui(ChemiCraftCore.instance, ChemiCraftCore.instance.guiChemicalCombinationTableID, par1World, par2, par3, par4);
		return true;
	}



	@Override
	public void registerIcons(IconRegister par1IconRegister) {
		this.topIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "ChemicalCombinationTable");
		this.bottomIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "Bottom");
		this.globalIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "Side");
	}



	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5) {
		if (!par1World.isRemote) {
			boolean var7 = par1World.isBlockIndirectlyGettingPowered(par2, par3, par4);
			if(var7) {
				TileEntityChemicalCombinationTable tileentity;
				try {
					tileentity = (TileEntityChemicalCombinationTable) par1World.getBlockTileEntity(par2, par3, par4);
				} catch(ClassCastException e) {
					return;
				}
				tileentity.setProvidePower(true);
			}
		}
	}



	@Override
	public Icon getBlockTextureFromSideAndMetadata(int par1, int par2) {
		if(par1 == 0){
			return this.bottomIcon;
		}else if(par1 == 1){
			return this.topIcon;
		}else{
			return this.globalIcon;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityChemicalCombinationTable();
	}

}
