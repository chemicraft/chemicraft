package pcc.chemicraft.core.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.debug.DebugTick;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;

public class BlockElectrolysisTable extends BlockContainer {

	public Icon topIcon;
	public Icon bottomIcon;
	public Icon globalIcon;

	public BlockElectrolysisTable(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setCreativeTab(ChemiCraftCore.creativeTabChemiCraft);
	}

	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
		par5EntityPlayer.openGui(ChemiCraftCore.instance, ChemiCraftCore.instance.guiElectrolysisTableID, par1World, par2, par3, par4);
		return true;
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister) {
		this.topIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "ElectrolysisTable");
		this.bottomIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "Bottom");
		this.globalIcon = par1IconRegister.registerIcon(ChemiCraftCore.CCTABLE + "Side");
	}

	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
		TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);
		if (tileentity != null) {
			DebugTick.removeDebugData("ElectrolysisTable" + "(x:" + tileentity.xCoord + " y:" + tileentity.yCoord + " z:" + tileentity.zCoord + ")");
		}
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int par1, int par2) {
		if(par1 == 0){
			return this.bottomIcon;
		}else if(par1 == 1){
			return this.topIcon;
		}else{
			return this.globalIcon;
		}
	}

	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityElectrolysisTable();
	}

}
