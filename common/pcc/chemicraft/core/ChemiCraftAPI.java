package pcc.chemicraft.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import net.minecraft.item.ItemStack;
import pcc.chemicraft.ChemiCraftData;
import pcc.chemicraft.EnumLoggingType;
import pcc.chemicraft.core.nbt.ChemicalNBTRecipe;
import pcc.chemicraft.core.system.ChemiCraftCraftingManager;
import pcc.chemicraft.util.Formula;
import pcc.chemicraft.util.ICompoundHandler;
import pcc.chemicraft.util.ListHash;
import pcc.chemicraft.util.MaterialRecipe;


/**
 * ChemiCraftのAPIを提供するクラスです<br>
 * ChemiCraftのAPIを作成する場合はこのクラスを使用します<br>
 * @author mozipi
 *
 */
public class ChemiCraftAPI {

	/**
	 * APIのインスタンス
	 */
	private static ChemiCraftAPI instance = new ChemiCraftAPI();


	/**
	 * APIのインスタンスを返します
	 * @return APIのインスタンス
	 */
	public static ChemiCraftAPI instance(){
		return instance;
	}


	/**
	 * 化合台の原子の数のリスト
	 */
	private static ArrayList<Integer[]> chemicalCombinationAmounts = new ArrayList<Integer[]>();


	/**
	 * 化合台の原子の種類のリスト
	 */
	private static ArrayList<String[]> chemicalCombinationAtoms = new ArrayList<String[]>();


	/**
	 * 化合台の結果のリスト
	 */
	private static ArrayList<ItemStack> chemicalCombinationResult = new ArrayList<ItemStack>();


	/**
	 * ChemiCraftの化学作業台類のレシピのマネージャー
	 */
	private static ChemiCraftCraftingManager chemiCraftCraftingManager = new ChemiCraftCraftingManager();


	/**
	 * List of item name of handler to compounds.
	 */
	private static ArrayList<String> compoundHandlerItemNames = new ArrayList<String>();


	/**
	 * 化合物の英語名からダメージ値へのマップ
	 */
	private static TreeMap<String, Integer> compoundsDamageToString = new TreeMap<String, Integer>();


	/**
	 * 化合物のテクスチャマップ
	 */
	private static TreeMap<Integer, String> compoundsTexture = new TreeMap<Integer, String>();


	/**
	 * List of compounds handlers.
	 */
	private static ArrayList<ICompoundHandler> compoundHandlers = new ArrayList<ICompoundHandler>();


	/**
	 * 化合物の文字列をダメージ値に変換します。
	 */
	private static HashMap<String, Integer> compoundHash = new HashMap<String, Integer>();


	/**
	 * List of compounds names.
	 */
	private static ListHash<String, String> compoundsNameListHash = new ListHash<String, String>();


	/**
	 * 電気分解燃料のリスト。
	 */
	private static HashMap<ItemStack, Integer> electrolysisFuelList = new HashMap<ItemStack, Integer>();


	/**
	 * 電気分解レシピのリスト
	 */
	private static HashMap<ItemStack, ItemStack[]> electrolysisRecipeList = new HashMap<ItemStack, ItemStack[]>();


	/**
	 * 素材製作台のレシピクラス
	 */
	private static ArrayList<MaterialRecipe> materialRecipe = new ArrayList<MaterialRecipe>();


	/**
	 * 熱分解燃料のリスト。
	 */
	private static HashMap<ItemStack, Integer> pyrolysisFuelList = new HashMap<ItemStack, Integer>();


	/**
	 * 熱分解レシピのリスト
	 */
	private static HashMap<ItemStack, ItemStack[]> pyrolysisRecipeList = new HashMap<ItemStack, ItemStack[]>();


	/**
	 * ツール&武器作成台の素材一覧のリスト
	 */
	private static ArrayList<ItemStack[]> toolAndWeaponMaterials = new ArrayList<ItemStack[]>();


	/**
	 * ツール&武器作成台の結果のリスト
	 */
	private static ArrayList<ItemStack> toolAndWeaponResult = new ArrayList<ItemStack>();


	/**
	 * ツール&武器作成台の不定形であるか
	 */
	private static ArrayList<Boolean> toolAndWeaponSharpless = new ArrayList<Boolean>();



	/**
	 * 化合レシピを追加します。materialの要素数は0<= n <= 16にしてください。
	 * @param material 素材
	 * @param result 結果
	 */
	public static void addChemicalCombinationRecipe(ItemStack result, Formula formula){
		ChemiCraftCore.logger.write("addChemicalCombinationRecipe:" + "Result-" + result.getItemName() + "/Material-" + Arrays.toString(formula.getAtoms())
				, EnumLoggingType.INFO);
		chemicalCombinationAtoms.add(formula.getAtoms());
		chemicalCombinationAmounts.add(formula.getAmonts());
		chemicalCombinationResult.add(result);
	}



	private static int compoundTexturePointer = 0;
	/**
	 * 化合物を追加します
	 * @param name 化合物の名前
	 */
	public static void addCompound(String name){
		ChemiCraftCore.logger.write("addCompound:" + name
				, EnumLoggingType.INFO);

		compoundsDamageToString.put(name, compoundTexturePointer);
		compoundsTexture.put(compoundTexturePointer, "ChemiCraft:CompoundEmpty");
		compoundTexturePointer++;
		compoundsNameListHash.add("en_US", name);
		compoundHash.put(name, compoundHash.size());
	}


	/**
	 * 既に登録された化合物に対して別の言語をを追加します
	 * @param lang 別言語名
	 * @param englishName 英語名
	 * @param langName 別言語への名前
	 */
	public static void addCompound(String lang, String englishName, String langName){
		ChemiCraftCore.logger.write("addCompound(MultiLang):" + "lang-" + lang + "/name-" + englishName + "/langToName-" + langName
				, EnumLoggingType.INFO);
		addCompound(englishName);
		addCompoundLanguage(lang, langName);
	}



	/**
	 * 単体で使用すると異常が発生するため、API以外での使用を固く非推奨する
	 * @param lang 言語名
	 * @param langName 言語への名前
	 */
	@Deprecated
	private static void addCompoundLanguage(String lang, String langName){
		compoundsNameListHash.add(
				lang,
				langName);
	}



	/**
	 * 電気分解台のレシピを追加します
	 * @param material 素材
	 * @param formula 化学式(結果)
	 */
	public static void addElectrolysisDecompositionRecipe(ItemStack material, Formula formula) {
		ChemiCraftCore.logger.write("addElectrolysisRecipe:" + "Material-" + material.getItemName() + "/Result-" + Arrays.toString(formula.getAtoms()),
				EnumLoggingType.INFO);

		ItemStack[] itemstacks =
				new ItemStack[formula.getAtoms().length];
		for (int i = 0; i < itemstacks.length; i++) {
			itemstacks[i] =
					new ItemStack(
							ChemiCraftCore.instance.itemAtoms,
							formula.getAmonts()[i],
							ChemiCraftData.toAtoms(formula.getAtoms()[i]));
		}
		electrolysisRecipeList.put(
				material,
				itemstacks);
	}



	/**
	 * 電気分解台のレシピを追加します
	 * @param material 素材 (すべて結果は同じになります)
	 * @param formula 化学式(結果)
	 */
	public static void addElectrolysisDecompositionRecipe(ArrayList<ItemStack> material, Formula formula) {
		for (ItemStack item : material) {
			if (item != null) {
				addElectrolysisDecompositionRecipe(item, formula);
			}
		}
	}



	/**
	 * 熱分解台のレシピを追加します
	 * @param material 素材
	 * @param formula 化学式(結果)
	 */
	public static void addPyrolysisDecompositionRecipe(ItemStack material, Formula formula) {
		ChemiCraftCore.logger.write("addPyrolysisRecipe:" + "Material-" + material.getItemName() + "/Result-" + Arrays.toString(formula.getAtoms()),
				EnumLoggingType.INFO);

		ItemStack[] itemstacks =
				new ItemStack[formula.getAtoms().length];
		for (int i = 0; i < itemstacks.length; i++) {
			itemstacks[i] =
					new ItemStack(
							ChemiCraftCore.instance.itemAtoms,
							formula.getAmonts()[i],
							ChemiCraftData.toAtoms(formula.getAtoms()[i]));
		}
		pyrolysisRecipeList.put(
				material,
				itemstacks);
	}



	/**
	 * 熱分解台のレシピを追加します
	 * @param material 素材 (すべて結果は同じになります)
	 * @param formula 化学式(結果)
	 */
	public static void addPyrolysisDecompositionRecipe(ArrayList<ItemStack> material, Formula formula) {
		for (ItemStack item : material) {
			if (item != null) {
				addPyrolysisDecompositionRecipe(item, formula);
				return;
			}
		}
	}



	/**
	 * 化合,熱分解,電気分解ができるレシピを追加します
	 * @param materialAndResult 素材(分解)と結果(化合)
	 * @param formula 化学式(分解なら結果に。化合なら素材に)
	 */
	public static void addReversible(ItemStack materialAndResult, Formula formula){
		addChemicalCombinationRecipe(materialAndResult, formula);
		addPyrolysisDecompositionRecipe(materialAndResult, formula);
		addElectrolysisDecompositionRecipe(materialAndResult, formula);
	}



	/**
	 * 化合,電気分解ができるレシピを追加します
	 * @param materialAndResult 素材(分解)と結果(化合)
	 * @param formula 化学式(分解なら結果に。化合なら素材に)
	 */
	public static void addReversibleOfElectrolysis(ItemStack result, Formula formula){
		addChemicalCombinationRecipe(result, formula);
		addElectrolysisDecompositionRecipe(result, formula);
	}



	/**
	 * 化合,熱分解ができるレシピを追加します
	 * @param materialAndResult 素材(分解)と結果(化合)
	 * @param formula 化学式(分解なら結果に。化合なら素材に)
	 */
	public static void addReversibleOfPyrolysis(ItemStack result, Formula formula){
		addChemicalCombinationRecipe(result, formula);
		addPyrolysisDecompositionRecipe(result, formula);
	}



	/**
	 * 電気分解台の燃料を追加します
	 * @param itemstack 燃料のItemStack
	 * @param burnTime 燃焼時間(tick * rate)
	 */
	public static void addElectrolysisDecompositionFuel(ItemStack itemstack, int burnTime) {
		ChemiCraftCore.logger.write("addElectrolysisFuel:" + "Fuel-" + itemstack.getItemName() + "/BurnTime-" + burnTime,
				EnumLoggingType.INFO);

		electrolysisFuelList.put(
				itemstack,
				burnTime);
	}



	/**
	 * 熱分解台の燃料を追加します
	 * @param itemstack 燃料のItemStack
	 * @param burnTime 燃焼時間(tick * rate)
	 */
	public static void addPyrolysisDecompositionFuel(ItemStack itemstack, int burnTime) {
		ChemiCraftCore.logger.write("addPyrolysisFuel:" + "Fuel-" + itemstack.getItemName() + "/BurnTime-" + burnTime,
				EnumLoggingType.INFO);

		pyrolysisFuelList.put(
				itemstack,
				burnTime);
	}



	/**
	 * 素材作成台のレシピを追加します
	 * @param materials 素材
	 * @param result 結果
	 * @param nbtRecipe NBT(Nullの場合はなし)
	 */
	public static void addMaterialRecipe(ItemStack[] materials, ItemStack result, ChemicalNBTRecipe nbtRecipe){
		ChemiCraftCore.logger.write("addMaterialRecipe:" + "Materials-" + Arrays.toString(materials) + "/Result-" + result.getItemName() + "/NBT-" + nbtRecipe,
				EnumLoggingType.INFO);

		materialRecipe.add(
				new MaterialRecipe(
						result,
						materials,
						nbtRecipe,
						false));
	}



	/**
	 * 素材作成台の不定形レシピを追加します
	 * @param materials 素材
	 * @param result 結果
	 * @param nbtRecipe NBT(Nullの場合はなし)
	 */
	public static void addSharplessMaterialRecipe(ItemStack[] materials, ItemStack result, ChemicalNBTRecipe nbtRecipe){
		ChemiCraftCore.logger.write("addMaterialRecipe:" + "Materials-" + Arrays.toString(materials) + "/Result-" + result.getItemName() + "/NBT-" + nbtRecipe,
				EnumLoggingType.INFO);

		materialRecipe.add(
				new MaterialRecipe(
						result,
						materials,
						nbtRecipe,
						true));
	}



	/**
	 * 化合物のハンドラーを設定します。<br>
	 * CompoundHandlerを実装したクラスをcompoundHandlerに入れることによってhandlerItemNameで指定した<br>
	 * 化合物にハンドラーをつけることができます。
	 * @param handlerItemName ハンドラーをつける化合物の英語名
	 * @param compoundHandler ハンドラー
	 */
	public static void settingCompoundHandler(String handlerItemName, ICompoundHandler compoundHandler){
		ChemiCraftCore.logger.write("settingCompoundHandler:" + "Name-" + handlerItemName + "/CompoundHandler-" + compoundHandler,
				EnumLoggingType.INFO);

		if (compoundHandler.getIconIndexHandler() != null) {
			int p = compoundsDamageToString.get(handlerItemName);
			if (p != -1) {
				compoundsTexture.put(p, compoundHandler.getIconIndexHandler());
			} else {
				System.out.println("化合物が存在しません(settingCompoundHandler)");
			}
		}

		compoundHandlers.add(compoundHandler);
		compoundHandlerItemNames.add(handlerItemName);
	}



	/**
	 * ツール&武器作成台のレシピを追加します<br>
	 * 未作成であり、動作しません
	 * @param materials 素材
	 * @param result 結果
	 */
	@Deprecated
	public static void addToolAndWeaponRecipe(ItemStack[] materials, ItemStack result) {
		toolAndWeaponMaterials.add(materials);
		toolAndWeaponResult.add(result);
		toolAndWeaponSharpless.add(false);
	}



	/**
	 * ツール&武器作成台の不定形レシピを追加します<br>
	 * 	 * 未作成であり、動作しません
	 * @param materials 素材
	 * @param result 結果
	 */
	@Deprecated
	public static void addSharplessToolAndWeaponRecipe(ItemStack[] materials, ItemStack result) {
		toolAndWeaponMaterials.add(materials);
		toolAndWeaponResult.add(result);
		toolAndWeaponSharpless.add(true);
	}



	//以下システム関連//////////////////////////////////////////////////////

	public static ArrayList<Integer[]> getChemicalCombinationAmounts() {
		return chemicalCombinationAmounts;
	}



	public static ArrayList<String[]> getChemicalCombinationAtoms() {
		return chemicalCombinationAtoms;
	}



	public static ArrayList<ItemStack> getChemicalCombinationResult() {
		return chemicalCombinationResult;
	}



	public static int getCompound(String key) {
		if (compoundHash.get(key) != null) {
			return compoundHash.get(key);
		} else {
			return -1;
		}
	}



	public static ArrayList<ICompoundHandler> getCompoundHandler() {
		compoundHandlers.trimToSize();
		return compoundHandlers;
	}



	public static ArrayList<String> getCompoundHandlerItemName() {
		compoundHandlerItemNames.trimToSize();
		return compoundHandlerItemNames;
	}



	public static ListHash<String, String> getCompoundsName() {
		return compoundsNameListHash;
	}



	public static ChemiCraftCraftingManager getCraftingManager() {
		return chemiCraftCraftingManager;
	}



	public static HashMap<ItemStack, Integer> getElectrolysisFuelList() {
		return electrolysisFuelList;
	}



	public static HashMap<ItemStack, ItemStack[]> getElectrolysisRecipeList() {
		return electrolysisRecipeList;
	}



	public static ArrayList<MaterialRecipe> getMaterialRecipe() {
		return materialRecipe;
	}



	public static HashMap<ItemStack, Integer> getPyrolysisFuelList() {
		return pyrolysisFuelList;
	}



	public static HashMap<ItemStack, ItemStack[]> getPyrolysisRecipeList() {
		return pyrolysisRecipeList;
	}



	public static ArrayList<ItemStack[]> getToolAndWeaponMaterials() {
		return toolAndWeaponMaterials;
	}



	public static ArrayList<ItemStack> getToolAndWeaponResult() {
		return toolAndWeaponResult;
	}



	public static ArrayList<Boolean> getToolAndWeaponSharpless() {
		return toolAndWeaponSharpless;
	}



	public static ArrayList<String> getCompoundsTexture() {
		Iterator<String> it = compoundsTexture.values().iterator();
		ArrayList<String> li = new ArrayList<String>();
		while (it.hasNext()) {
			li.add(it.next());
		}
		return li;
	}

}
