package pcc.chemicraft.core.other;

import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.WorldChunkManager;
import net.minecraft.world.chunk.IChunkProvider;

public class WorldTypeChemical extends WorldType {

	public WorldTypeChemical(int par1, String par2Str) {
		super(par1, par2Str);
	}

	public WorldChunkManager getChunkManager(World par1) {
		return new WorldChunkManager(par1);
	}

	public IChunkProvider getChunkGenerator(World par1) {
		return new ChunkProviderChemical(par1, par1.getSeed(), par1.getWorldInfo().isMapFeaturesEnabled());
	}

	public int getSeaLevel(World par1) {
		return 64;
	}

	public boolean hasVoidParticles(boolean var1) {
		return false;
	}

	public double voidFadeMagnitude() {
		return 1.0D;
	}

}
