package pcc.chemicraft.core.nbt;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import pcc.chemicraft.ChemiCraftData;
import pcc.chemicraft.core.ChemiCraftCore;

public class NBTRecipeGrenade implements ChemicalNBTRecipe {

	@Override
	public void setNBT(ItemStack[] materials, ItemStack result) {
		if (result.stackTagCompound == null) {
			result.stackTagCompound = new NBTTagCompound();
		}

		if (!result.stackTagCompound.hasKey("Effect")) {
			result.stackTagCompound.setTag("Effect", new NBTTagList("Effect"));
		}

		NBTTagList tagList = (NBTTagList) result.getTagCompound().getTag("Effect");
		NBTTagCompound tag = new NBTTagCompound();

		for (int i = 0;i < materials.length;i++) {
			if (materials[i] != null) {
				if (materials[i].itemID-256 == ChemiCraftCore.instance.atomsID && materials[i].getItemDamage() == ChemiCraftData.HYDROGEN) {
					tag.setString("GrenadeEffect" + i, "Hydrogen");
				}
				if (materials[i].itemID-256 == ChemiCraftCore.instance.atomsID && materials[i].getItemDamage() == ChemiCraftData.CARBON) {
					tag.setString("GrenadeEffect" + i, "Carbon");
				}
				if (materials[i].itemID-256 == ChemiCraftCore.instance.atomsID && materials[i].getItemDamage() == ChemiCraftData.URANIUM) {
					tag.setString("GrenadeEffect" + i, "Uranium");
				}
			}
		}
		tagList.appendTag(tag);

	}

	@Override
	public ItemStack[] getMatchItems(ItemStack[] materials) {
		ItemStack[] var1 = new ItemStack[materials.length];
		for (int i = 0;i < var1.length;i++) {
			if (materials[i] != null) {
				if (materials[i].getItemDamage() == ChemiCraftData.HYDROGEN) {
					var1[i] = materials[i];
				} else if (materials[i].getItemDamage() == ChemiCraftData.CARBON) {
					var1[i] = materials[i];
				} else if (materials[i].getItemDamage() == ChemiCraftData.URANIUM) {
					var1[i] = materials[i];
				} else {
					var1[i] = null;
				}
			} else {
				var1[i] = null;
			}
		}
		return var1;
	}

}
