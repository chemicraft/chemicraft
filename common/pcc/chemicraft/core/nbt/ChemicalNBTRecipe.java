package pcc.chemicraft.core.nbt;

import net.minecraft.item.ItemStack;

public interface ChemicalNBTRecipe {

	public abstract void setNBT(ItemStack[] materials, ItemStack result);

	public abstract ItemStack[] getMatchItems(ItemStack[] materials);

}
