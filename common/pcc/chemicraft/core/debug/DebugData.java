package pcc.chemicraft.core.debug;

public class DebugData {

	private String key;
	private Object value;

	public DebugData(String par1, Object par2) {
		this.key = par1;
		this.value = par2;
	}

	public String getKey() {
		return this.key;
	}

	public Object getValue() {
		return this.value;
	}

}
