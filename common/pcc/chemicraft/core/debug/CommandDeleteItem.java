package pcc.chemicraft.core.debug;

import java.util.Iterator;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

public class CommandDeleteItem extends CommandBase {

	@Override
	public String getCommandName() {
		return "delItem";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processCommand(ICommandSender var1, String[] var2) {
		MinecraftServer server = null;
		EntityPlayerMP player = null;
		try { server = (MinecraftServer)var1; } catch (ClassCastException e) {}
		try{ player = (EntityPlayerMP)var1;} catch(ClassCastException e) {}

		World world;
		List<Entity> list;
		Iterator<Entity> itr;

		if(server != null){
			if(var2.length > 0){
				world = server.worldServerForDimension(Integer.parseInt(var2[0]));
				list = world.loadedEntityList;
				itr = list.iterator();
				while(itr.hasNext()){
					Entity entity = itr.next();
					if(entity instanceof EntityItem){
						entity.setDead();
					}
				}
			}else{
				var1.sendChatToPlayer("Please specify the World.");
			}
		}else if(player != null){
			world = player.worldObj;
			list = world.loadedEntityList;
			itr = list.iterator();
			while(itr.hasNext()){
				Entity entity = itr.next();
				if(entity instanceof EntityItem){
					entity.setDead();
				}
			}
		}else{
			var1.sendChatToPlayer("---Command Error---");
		}

	}

}
