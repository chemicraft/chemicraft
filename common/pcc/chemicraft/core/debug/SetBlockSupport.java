package pcc.chemicraft.core.debug;
import java.util.Random;

import net.minecraft.world.World;

public class SetBlockSupport
{

	static Random rand = new Random();

	/**
	 * X方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param Length 長さ
	 * @param Direction 方向(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object lineX(World par1World,int X,int Y,int Z,int Block,int meta,int Length,int Direction)
	{
		int loop2=0;
		Object DataBox=null;
		for(loop2=0;loop2 < Length;loop2++)
		{
			switch(Direction)
			{
			case -1:
				DataBox = par1World.setBlock(X, Y, Z, Block, meta, 0x02);
				break;
			case 0:
				DataBox = par1World.setBlock(X-(Length-1)+loop2,Y,Z,Block,meta, 0x02);
				break;
			case 1:
				DataBox = par1World.setBlock(X+loop2, Y, Z, Block,meta, 0x02);
				break;
			}
		}
		return DataBox;
	}

	/**
	 * Y方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param Length 長さ
	 * @param Direction 方向(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object lineY(World par1World,int X,int Y,int Z,int Block,int meta,int Length,int Direction)
	{
		int loop2=0;
		Object DataBox=null;
		for(loop2=0;loop2 < Length;loop2++)
		{
			switch(Direction)
			{
			case -1:
				DataBox = par1World.setBlock(X, Y-loop2, Z, Block,meta, 0x02);
				break;
			case 0:
				DataBox = par1World.setBlock(X,Y-(Length-1)+loop2,Z,Block,meta, 0x02);
				break;
			case 1:
				DataBox = par1World.setBlock(X, Y+loop2, Z, Block,meta, 0x02);
				break;
			}
		}
		return DataBox;
	}

	/**
	 * Z方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param Length 長さ
	 * @param Direction 方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object lineZ(World par1World,int X,int Y,int Z,int Block,int meta,int Length,int Direction)
	{
		int loop2=0;
		Object DataBox=null;
		for(loop2=0;loop2 < Length;loop2++)
		{
			switch(Direction)
			{
			case -1:
				DataBox = par1World.setBlock(X, Y, Z-loop2, Block,meta, 0x02);
				break;
			case 0:
				DataBox = par1World.setBlock(X,Y,Z-(Length-1)+loop2,Block,meta, 0x02);
				break;
			case 1:
				DataBox = par1World.setBlock(X, Y, Z+loop2, Block,meta, 0x02);
				break;
			}
		}
		return DataBox;
	}

	/**
	 * XY方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param XLength X方向の長さ
	 * @param YLength Y方向の長さ
	 * @param XDirection X方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @param YDirection Y方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object  lineXY(World par1World,int X,int Y,int Z,int Block,int meta,int XLength,int YLength,int XDirection,int YDirection)
	{
		Object DataBox=null;
		DataBox = lineX(par1World,X,Y,Z,Block,meta,XLength,XDirection);
		DataBox = lineY(par1World,X,Y,Z,Block,YLength,meta,YDirection);
		return DataBox;
	}

	/**
	 * XZ方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param XLength X方向の長さ
	 * @param ZLength Y方向の長さ
	 * @param XDirection X方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @param ZDirection Z方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object  lineXZ(World par1World,int X,int Y,int Z,int Block,int meta,int XLength,int ZLength,int XDirection,int ZDirection)
	{
		Object DataBox=null;
		DataBox = lineX(par1World,X,Y,Z,Block,meta,XLength,XDirection);
		DataBox = lineZ(par1World,X,Y,Z,Block,meta,ZLength,ZDirection);
		return DataBox;
	}

	/**
	 * YZ方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param YLength X方向の長さ
	 * @param ZLength Y方向の長さ
	 * @param YDirection Y方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @param ZDirection Z方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object  lineYZ(World par1World,int X,int Y,int Z,int Block,int meta,int YLength,int ZLength,int YDirection,int ZDirection)
	{
		Object DataBox=null;
		DataBox = lineY(par1World,X,Y,Z,Block,meta,YLength,YDirection);
		DataBox = lineZ(par1World,X,Y,Z,Block,meta,ZLength,ZDirection);
		return DataBox;
	}

	/**
	 * XYZ方向に線状にBlockを配置します。
	 * Directionが0の場合、特殊な計算式で
	 * +,-方向:loop-1の長さになります。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param XLength X方向の長さ
	 * @param YLength Y方向の長さ
	 * @param ZLength Z方向の長さ
	 * @param XDirection X方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @param YDirection Y方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @param ZDirection Z方向指定(+方向:1,-方向:-1,中心から-+の方向:0)
	 * @return Object
	 */
	public static Object  lineXYZ(World par1World,int X,int Y,int Z,int Block,int meta,int XLength,int YLength,int ZLength,int XDirection,int YDirection,int ZDirection)
	{
		Object DataBox=null;
		DataBox = lineX(par1World,X,Y,Z,Block,meta,XLength,XDirection);
		DataBox = lineY(par1World,X,Y,Z,Block,meta,YLength,YDirection);
		DataBox = lineZ(par1World,X,Y,Z,Block,meta,ZLength,ZDirection);
		return DataBox;
	}

	/**
	 * X方向の壁を作成します。
	 * Directionは1で固定です。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param Length 幅
	 * @param height 高さ
	 * @return Object
	 */
	public static Object wallX(World par1World,int X,int Y,int Z,int Block,int meta,int Length,int height)
	{
		Object DataBox = null;
		for(int loop = 0;loop < Length;loop++){
			lineY(par1World, X - (Length/2) + loop, Y, Z, Block,meta, height, 1);
		}
		return DataBox;
	}

	/**
	 * Z方向の壁を作成します。
	 * Directionは1で固定です。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param Length 幅
	 * @param height 高さ
	 * @return Object
	 */
	public static Object wallZ(World par1World,int X,int Y,int Z,int Block,int meta,int Length,int height)
	{
		Object DataBox = null;
		for(int loop = 0;loop < Length;loop++){
			lineY(par1World, X, Y, Z - (Length/2) + loop, Block,meta, height, 1);
		}
		return DataBox;
	}

	/**
	 * 辺だけの四角を作成します。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param LengthX X方向の長さ
	 * @param LengthZ  Z方向の長さ
	 * @return Object
	 */
	public static Object spuareFrame(World par1World,int X,int Y,int Z,int Block,int meta,int LengthX,int LengthZ)
	{
		Object DataBox = null;
		DataBox = lineX(par1World, X, Y, Z + (LengthZ/2), Block,meta, LengthX/2+1, 0);
		DataBox = lineX(par1World, X, Y, Z - (LengthZ/2), Block,meta, LengthX/2+1, 0);
		DataBox = lineZ(par1World, X + (LengthX/2), Y, Z, Block,meta, LengthZ/2+1, 0);
		DataBox = lineZ(par1World, X - (LengthX/2), Y, Z, Block,meta, LengthZ/2+1, 0);
		return DataBox;
	}

	/**
	 * 辺だけの箱を作成します。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param LengthX X方向の長さ
	 * @param LengthZ  Z方向の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object boxFrame(World par1World,int X,int Y,int Z,int Block,int meta,int LengthX,int LengthZ,int height)
	{
		Object DataBox=null;
		DataBox = lineX(par1World, X, Y, Z + (LengthZ/2), Block,meta, LengthX/2, 0);
		DataBox = lineX(par1World, X, Y, Z - (LengthZ/2), Block,meta, LengthX/2, 0);
		DataBox = lineX(par1World, X, Y + height-1, Z + (LengthZ/2), Block,meta, LengthX/2, 0);
		DataBox = lineX(par1World, X, Y + height-1, Z - (LengthZ/2), Block,meta, LengthX/2, 0);

		DataBox = lineY(par1World,X +(LengthX/2),Y,Z +(LengthZ/2),Block,meta,height,1);
		DataBox = lineY(par1World,X -(LengthX/2),Y,Z +(LengthZ/2),Block,meta,height,1);
		DataBox = lineY(par1World,X +(LengthX/2),Y,Z - (LengthZ/2),Block,meta,height,1);
		DataBox = lineY(par1World,X -(LengthX/2),Y,Z - (LengthZ/2),Block,meta,height,1);

		DataBox = lineZ(par1World, X + (LengthX/2), Y, Z, Block,meta, LengthZ/2, 0);
		DataBox = lineZ(par1World, X - (LengthX/2), Y, Z, Block,meta, LengthZ/2, 0);
		DataBox = lineZ(par1World, X + (LengthX/2), Y + height-1, Z, Block,meta, LengthZ/2, 0);
		DataBox = lineZ(par1World, X - (LengthX/2), Y + height-1, Z, Block,meta, LengthZ/2, 0);
		return DataBox;
	}

	/**
	 * 四角を作成します。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param LengthX X方向の長さ
	 * @param LengthZ  Z方向の長さ
	 * @return Object
	 */
	public static Object square(World par1World,int X,int Y,int Z,int Block,int meta,int LengthX,int LengthZ)
	{
		Object DataBox = null;
		int loop1 = 0;
		for(loop1=0; loop1 < LengthZ; loop1++){
			DataBox = lineX(par1World,X - (LengthX/2),Y,Z - (LengthZ/2) + loop1,Block,meta,LengthX,1);
		}
		return DataBox;
	}

	/**
	 * 箱を生成します。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param LengthX X方向の長さ
	 * @param LengthZ  Z方向の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object box(World par1World,int X,int Y,int Z,int Block,int meta,int LengthX,int LengthZ,int height)
	{
		Object DataBox = null;
		DataBox = square(par1World, X, Y, Z, Block, meta,LengthX,LengthZ);
		DataBox = square(par1World,X,Y+height,Z,Block,meta,LengthX,LengthZ);
		DataBox = wallX(par1World,X,Y,Z-(LengthZ/2),Block,meta,LengthX,height);
		DataBox = wallZ(par1World,X-(LengthX/2),Y,Z,Block,meta,LengthZ,height);
		DataBox = wallX(par1World,X,Y,Z+(LengthZ/2),Block,meta,LengthX,height);
		DataBox = wallZ(par1World,X+(LengthX/2),Y,Z,Block,meta,LengthZ,height);
		return DataBox;
	}

	/**
	 * Blockで埋め尽くされた箱を生成します。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param LengthX X方向の長さ
	 * @param LengthZ  Z方向の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object fillBox(World par1World , int X,int Y,int Z,int Block,int meta,int LengthX,int LengthZ,int height)
	{
		Object DataBox = null;

		for(int i = 0; i < height; i++)
		{
			DataBox = square(par1World,X,Y+i,Z,Block,meta,LengthX,LengthZ);
		}

		return DataBox;
	}

	/**
	 * 座標ごとにデータを指定して配置します。
	 * 配列になっているので、順番に指定してください。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param XPosition 配置する相対X座標
	 * @param YPosition 配置する相対Y座標
	 * @param ZPosition 配置する相対Z座標
	 * @param Option
	 * @return Object
	 */
	public static Object fineSetBlock(World par1World,int X,int Y,int Z,int[] Block,int[] meta,int[] XPosition,int[] YPosition,int[] ZPosition,Object[] Option){
		Object DataBox = null;
		for(int i=0;i<Block.length;i++){
			DataBox = par1World.setBlock(X+XPosition[i], Y+YPosition[i], Z+ZPosition[i], Block[i],meta[i], 0x02);
		}

		if(Option!=null){
			for(int j=0;j<Option.length;j++){
				DataBox = Option[j];
			}
		}
		return DataBox;
	}

	/**
	 * X方向に台形を作成します。
	 * 必ずbottom/(upper*3)*(height/upper)かupper/(bottom*3)*(height/buttom)が整数になるようにしてください。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param upperSide 上辺の長さ
	 * @param bottomSide 下辺の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object TrapezoidX(World par1World,int X,int Y,int Z,int Block,int meta,int upperSide,int bottomSide,int height){
		Object DataBox = null;
		int Rate=0;
		int Math=0;
		int XRate=0;
		int YRate = 0;
		for(int i=0;i<height;i++){
			YRate++;
			Math++;
			XRate++;
			if(upperSide < bottomSide){
				if(Math == bottomSide/(upperSide*3)*(height/upperSide)){
					Rate++;
					Math=0;
				}
			}

			if(bottomSide < upperSide){
				if(Math == upperSide/(bottomSide*3)*(height/bottomSide)){
					Rate--;
					Math=0;
				}
			}

			DataBox=lineZ(par1World, X+XRate-1, Y+YRate, Z, Block, meta, upperSide+Rate, 0);
		}
		return DataBox;
	}

	/**
	 * Y方向のXラインに台形を作成します。
	 * 必ずbottom/(upper*3)*(height/upper)かupper/(bottom*3)*(height/buttom)が整数になるようにしてください。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param upperSide 上辺の長さ
	 * @param bottomSide 下辺の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object TrapezoidY_X(World par1World,int X,int Y,int Z,int Block,int meta,int upperSide,int bottomSide,int height){
		Object DataBox = null;
		int Rate=0;
		int Math=0;
		int YRate = 0;
		for(int i=0;i<height;i++){
			YRate++;
			Math++;
			if(upperSide < bottomSide){
				if(Math == bottomSide/(upperSide*3)*(height/upperSide)){
					Rate++;
					Math=0;
				}
			}

			if(bottomSide < upperSide){
				if(Math == upperSide/(bottomSide*3)*(height/bottomSide)){
					Rate--;
					Math=0;
				}
			}

			DataBox=lineZ(par1World, X, Y+YRate, Z, Block, meta, upperSide+Rate, 0);
		}
		return DataBox;
	}

	/**
	 * Y方向のZラインに台形を作成します。
	 * 必ずbottom/(upper*3)*(height/upper)かupper/(bottom*3)*(height/buttom)が整数になるようにしてください。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param upperSide 上辺の長さ
	 * @param bottomSide 下辺の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object TrapezoidY_Z(World par1World,int X,int Y,int Z,int Block,int meta,int upperSide,int bottomSide,int height){
		Object DataBox = null;
		int Rate=0;
		int Math=0;
		int YRate = 0;
		for(int i=0;i<height;i++){
			YRate++;
			Math++;
			if(upperSide < bottomSide){
				if(Math == bottomSide/(upperSide*3)*(height/upperSide)){
					Rate++;
					Math=0;
				}
			}

			if(bottomSide < upperSide){
				if(Math == upperSide/(bottomSide*3)*(height/bottomSide)){
					Rate--;
					Math=0;
				}
			}

			DataBox=lineX(par1World, X, Y+YRate, Z, Block, meta, upperSide+Rate, 0);
		}
		return DataBox;
	}

	/**
	 * Z方向に台形を作成します。
	 * 必ずbottom/(upper*3)*(height/upper)かupper/(bottom*3)*(height/buttom)が整数になるようにしてください。
	 * @param par1World
	 * @param X 基準X座標
	 * @param Y 基準Y座標
	 * @param Z 基準Z座標
	 * @param Block 配置するBlock
	 * @param meta 配置するMetadata
	 * @param upperSide 上辺の長さ
	 * @param bottomSide 下辺の長さ
	 * @param height 高さ
	 * @return Object
	 */
	public static Object TrapezoidZ(World par1World,int X,int Y,int Z,int Block,int meta,int upperSide,int bottomSide,int height){
		Object DataBox = null;
		int Rate=0;
		int Math=0;
		int ZRate=0;
		for(int i=0;i<height;i++){
			Math++;
			ZRate++;
			if(upperSide < bottomSide){
				if(Math == bottomSide/(upperSide*3)*(height/upperSide)){
					Rate++;
					Math=0;
				}
			}

			if(bottomSide < upperSide){
				if(Math == upperSide/(bottomSide*3)*(height/bottomSide)){
					Rate--;
					Math=0;
				}
			}

			DataBox=lineX(par1World, X, Y, Z+ZRate, Block, meta, upperSide+Rate, 0);
		}
		return DataBox;
	}

}