package pcc.chemicraft.core.debug;

import net.minecraft.block.Block;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class CommandSetTile extends CommandBase {

	@Override
	public String getCommandName() {
		return "setTile";
	}

	@Override
	public void processCommand(ICommandSender var1, String[] var2) {
		try{
			EntityPlayer player = (EntityPlayer)var1;
			World wolrd = player.worldObj;
			if(var2.length > 0){
				try{
					if(Integer.parseInt(var2[0]) < 4096 && Block.blocksList[Integer.parseInt(var2[0])] != null || Integer.parseInt(var2[0]) == 0){
						if(var2.length == 1){
							SetBlockSupport.square(wolrd, (int)player.posX, (int)player.posY, (int)player.posZ, Integer.parseInt(var2[0]), Integer.parseInt(var2[1]), 12, 12);
						}else if(var2.length > 2){
							SetBlockSupport.square(wolrd, (int)player.posX, (int)player.posY, (int)player.posZ, Integer.parseInt(var2[0]), Integer.parseInt(var2[1]), Integer.parseInt(var2[2]), Integer.parseInt(var2[2]));
						}else{
							SetBlockSupport.square(wolrd, (int)player.posX, (int)player.posY, (int)player.posZ, Integer.parseInt(var2[0]), 0, 12, 12);
						}
					}else{
						var1.sendChatToPlayer("\u00a7cBlockが存在しないか指定された数が4096を超えています。");
					}
				}catch(NumberFormatException e){
					var1.sendChatToPlayer("\u00a7c指定された文字は数値ではありません。");
				}
			}else{
				var1.sendChatToPlayer("\u00a7cBlockIDを指定してください。(Metadata, Sizeも可能)");
			}
		}catch(ClassCastException e){

		}
	}

}
