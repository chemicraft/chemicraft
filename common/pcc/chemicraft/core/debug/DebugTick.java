package pcc.chemicraft.core.debug;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class DebugTick extends JPanel implements ITickHandler {

	private static HashMap<String, DebugData[]> displayDataList = new HashMap<String, DebugData[]>();

	public DebugTick() {
		JFrame frame = new JFrame();
		frame.setSize(640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(this);
		frame.setVisible(true);

	}

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {

	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
		this.repaint();
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT, TickType.CLIENT);
	}

	@Override
	public String getLabel() {
		return null;
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.clearRect(0, 0, this.getWidth(), this.getHeight());

		int fontPos = 20;
		Iterator<String> itKey = displayDataList.keySet().iterator();
		while (itKey.hasNext()) {
			String key = itKey.next();
			DebugData[] datas = displayDataList.get(key);
			g2.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 16));
			g2.setColor(Color.RED);
			g2.drawString(key + ":", 10, fontPos);
			g2.setFont(new Font("ＭＳ ゴシック", Font.PLAIN, 12));
			g2.setColor(Color.BLACK);

			fontPos += 16;

			for (int i = 0; i < datas.length; i++) {
				g2.drawString(String.valueOf(datas[i].getKey()) + ":" + String.valueOf(datas[i].getValue()), 10, fontPos);
				fontPos += 12;
			}

			fontPos += 4;

		}
	}

	public static void setDebugData(String key, DebugData... value) {
		displayDataList.put(key, value);
	}

	public static void removeDebugData(String key) {
		displayDataList.remove(key);
	}

}
