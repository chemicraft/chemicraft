package pcc.chemicraft.core.item;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.ChemiCraftCore;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemCompounds extends Item {

	private Icon[] icons;

	public ItemCompounds(int par1){
		super(par1);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(ChemiCraftCore.instance.creativeTabChemiCraft);
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
		try {
			for(int i=0;i < ChemiCraftAPI.instance().getCompoundHandlerItemName().size();i++){
				if(ChemiCraftAPI.instance().getCompoundsName().getKeyList(par1ItemStack.getItemDamage()).equals(ChemiCraftAPI.instance().getCompoundHandlerItemName().get(i))){
					return ChemiCraftAPI.instance().getCompoundHandler().get(i).onItemUseHandler(par1ItemStack, par2EntityPlayer, par3World, par4, par5, par6, par7, par8, par9, par10);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			par1ItemStack.stackSize = 0;
		}
		return false;
	}


	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		try {
			for(int i=0;i < ChemiCraftAPI.instance().getCompoundHandlerItemName().size();i++){
				if(ChemiCraftAPI.instance().getCompoundsName().getKeyList(par1ItemStack.getItemDamage()).equals(ChemiCraftAPI.instance().getCompoundHandlerItemName().get(i))){
					ChemiCraftAPI.instance().getCompoundHandler().get(i).onItemRightClickHandler(par1ItemStack, par2World, par3EntityPlayer);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			par1ItemStack.stackSize = 0;
		}
		return par1ItemStack;
	}



	@Override
	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5) {
		try {
			for(int i=0;i < ChemiCraftAPI.instance().getCompoundHandlerItemName().size();i++){
				if(ChemiCraftAPI.instance().getCompoundsName().getKeyList(par1ItemStack.getItemDamage()).equals(ChemiCraftAPI.instance().getCompoundHandlerItemName().get(i))){
					ChemiCraftAPI.instance().getCompoundHandler().get(i).onUpdateHandler(par1ItemStack, par2World, par3Entity, par4, par5);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			par1ItemStack.stackSize = 0;
			par1ItemStack = null;
		}
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List){
		for(int type = 0; type < ChemiCraftAPI.instance().getCompoundsName().get("en_US").size(); type++) {
			par3List.add(new ItemStack(par1, 1, type));
		}
	}



	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack){
		return super.getUnlocalizedName() + "." + ChemiCraftCore.instance.api.getCompoundsName().get("en_US", par1ItemStack.getItemDamage());
	}



	@Override
	@SideOnly(Side.CLIENT)
	public void updateIcons(IconRegister par1IconRegister) {
		ArrayList<String> textures = ChemiCraftAPI.getCompoundsTexture();
		this.icons = new Icon[textures.size()];
		for (int i = 0; i < textures.size(); i++) {
			this.icons[i] = par1IconRegister.registerIcon(textures.get(i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return this.icons[par1];
	}

}
