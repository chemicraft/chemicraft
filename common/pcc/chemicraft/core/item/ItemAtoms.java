package pcc.chemicraft.core.item;

import java.util.List;
import java.util.Random;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.util.AtomInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemAtoms extends Item {

	@SideOnly(Side.CLIENT)
	private Icon[] icons;

	public ItemAtoms(int par1){
		super(par1);
		this.maxStackSize = 64;
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(ChemiCraftCore.instance.creativeTabChemiCraft);
	}



	@Override
	public void onUpdate(ItemStack par1ItemStack, World par2World, Entity par3Entity, int par4, boolean par5)
	{
		if (AtomInfo.isRI(par1ItemStack.getItemDamage() + 1)){
			Random random = new Random();
			if (random.nextInt(100) == 0){
				if (par3Entity instanceof EntityLiving){
					EntityLiving entity = (EntityLiving)par3Entity;
					entity.attackEntityFrom(ChemiCraftCore.getRadiationDamageSource(entity), 1);
				}
				if (par3Entity instanceof EntityPlayer){
					EntityPlayer player = (EntityPlayer)par3Entity;
					ItemStack itemstack = new ItemStack(
							par1ItemStack.itemID,
							1,
							par1ItemStack.getItemDamage());
					itemstack.setItemDamage(AtomInfo.collapseUraniumSeries(itemstack.getItemDamage() + 1) - 1);
					if (--par1ItemStack.stackSize <= 0) {
						player.inventory.clearInventory(par1ItemStack.itemID, par1ItemStack.getItemDamage());
					}
					player.inventory.addItemStackToInventory(itemstack);
				}
			}
		}
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List){
		for(int type = 0; type < ChemiCraftCore.instance.ATOMSNAME.length; type++)
		{
			par3List.add(new ItemStack(par1, 1, type));
		}
	}



	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return this.icons[par1];
	}



	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack){
		return super.getUnlocalizedName() + "." + ChemiCraftCore.instance.ATOMSNAME[par1ItemStack.getItemDamage()];
	}



	@Override
	@SideOnly(Side.CLIENT)
	public void updateIcons(IconRegister par1IconRegister) {
		this.icons = new Icon[ChemiCraftCore.instance.ATOMSNAME.length];

		for (int i = 0; i < ChemiCraftCore.instance.ATOMSNAME.length; i++) {
			this.icons[i] = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "atoms_" + ChemiCraftCore.instance.ATOMSNAME[i]);
		}
	}

}