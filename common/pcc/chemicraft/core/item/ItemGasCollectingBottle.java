package pcc.chemicraft.core.item;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFire;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.ChemiCraftData;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.util.Auxiliary.Probability;

public class ItemGasCollectingBottle extends ItemAtomInfoContainer
{

	public Probability probability = new Probability();

	public ItemGasCollectingBottle(int id)
	{
		super(id);
		this.setMaxStackSize(1);
		this.setMaxDamage(4);
		this.setCreativeTab(ChemiCraftCore.instance.creativeTabChemiCraft);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		par1ItemStack.damageItem(1, par3EntityPlayer);

		this.atomInfo.update(par2World, par3EntityPlayer);

		int heightValue = par2World.getHeightValue((int)par3EntityPlayer.posX, (int)par3EntityPlayer.posZ);

		int result = probability.getProbability(78.0D, 21.0D, 1.0D, 0.032D, 0.0018D, 0.000012D, 0.00052D, 0.000114D, 0.000087D);
		if (this.atomInfo.isOverY(heightValue)) {
			if (result == 0) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.NITROGEN), par3EntityPlayer);
			} else if (result == 1) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.OXYGEN), par3EntityPlayer);
			} else if (result == 2) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.ARGON), par3EntityPlayer);
			} else if (result == 3) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, ChemiCraftAPI.instance().getCompound("CarbonDioxide")), par3EntityPlayer);
			} else if (result == 4) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.NEON), par3EntityPlayer);
			} else if (result == 5) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, ChemiCraftAPI.instance().getCompound("CarbonMonoxide")), par3EntityPlayer);
			} else if (result == 6) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.LITHIUM), par3EntityPlayer);
			} else if (result == 7) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.KRYPTON), par3EntityPlayer);
			} else {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemAtoms, 1, ChemiCraftData.XENON), par3EntityPlayer);
			}
		}

		return par1ItemStack;
	}

	protected void isStackOrDrop(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer)
	{
		if (!par2EntityPlayer.inventory.addItemStackToInventory(par1ItemStack)) {
			par2EntityPlayer.dropPlayerItem(par1ItemStack);
		}
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
	{
		int blockID = par3World.getBlockId(par4, par5, par6);
		Block block = Block.blocksList[blockID];

		int uniqueBlockID = par3World.getBlockId(par4, par5 + 1, par6);
		Block uniqueBlock = Block.blocksList[uniqueBlockID];

		if (uniqueBlock instanceof BlockFire) {
			par1ItemStack.damageItem(1, par2EntityPlayer);

			int result1 = probability.getProbability(99, 1);

			if (result1 == 0) {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, ChemiCraftAPI.instance().getCompound("CarbonDioxide")), par2EntityPlayer);
			} else {
				this.isStackOrDrop(new ItemStack(ChemiCraftCore.instance.itemCompounds, 1, ChemiCraftAPI.instance().getCompound("CarbonMonoxide")), par2EntityPlayer);
			}

			return true;
		}
		return false;
	}

	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "gas_collecting_bottle");
	}

}
