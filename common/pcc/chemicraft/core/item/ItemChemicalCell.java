package pcc.chemicraft.core.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.core.ChemiCraftCore;


public class ItemChemicalCell extends Item
{

	public ItemChemicalCell(int par1)
	{
		super(par1);
		this.maxStackSize = 64;
		this.setCreativeTab(ChemiCraftCore.instance.creativeTabChemiCraft);
	}



	@Override
	public void updateIcons(IconRegister par1IconRegister){
		this.iconIndex = par1IconRegister.registerIcon(ChemiCraft.TEXTURE + "chemical_cell");
	}

}
