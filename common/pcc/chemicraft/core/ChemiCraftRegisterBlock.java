package pcc.chemicraft.core;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import pcc.chemicraft.core.block.BlockChemicalCombinationTable;
import pcc.chemicraft.core.block.BlockChemicalCraftingTable;
import pcc.chemicraft.core.block.BlockElectrolysisTable;
import pcc.chemicraft.core.block.BlockPyrolysisTable;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * Blockを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterBlock extends ChemiCraftCoreRegister {

	public ChemiCraftRegisterBlock(ChemiCraftCore mod) {
		super(mod);
	}

	@Override
	public void start() {
		//Blockを変数に代入
		this.mod.blockPyrolysisTable = new BlockPyrolysisTable(this.mod.pyrolysisTableID, Material.ground).
				setHardness(2.0F).
				setResistance(0.0F).
				setStepSound(Block.soundStoneFootstep).
				setUnlocalizedName("PyrolysisTable");
		this.mod.blockElectrolysisTable = new BlockElectrolysisTable(this.mod.electrolysisTableID, Material.ground).
				setHardness(2.0F).
				setResistance(0.0F).
				setStepSound(Block.soundStoneFootstep).
				setUnlocalizedName("ElectrolysisTable");
		this.mod.blockChemicalCombinationTable = new BlockChemicalCombinationTable(this.mod.chemicalConbinationTableID, Material.ground).
				setHardness(2.0F).
				setResistance(0.0F).
				setStepSound(Block.soundStoneFootstep).
				setUnlocalizedName("ChemicalCombinationTable");
		/*
		this.mod.blockToolAndWeaponCraftingTable = new BlockToolAndWeaponCraftingTable(this.mod.toolAndWeaponCraftingTableID, 3, Material.ground).
				setHardness(2.0F).
				setResistance(0.0F).
				setStepSound(Block.soundStoneFootstep).
				setUnlocalizedName("ToolAndWeaponCraftingTable");
		*/
		this.mod.blockChemicalCraftingTable = new BlockChemicalCraftingTable(this.mod.chemicalCraftingTableID, Material.ground).
				setHardness(2.0F).
				setResistance(0.0F).
				setStepSound(Block.soundStoneFootstep).
				setUnlocalizedName("ChemicalCraftingTable");

		//Minecraftに登録
		GameRegistry.registerBlock(this.mod.blockPyrolysisTable, "BlockPyrolysisTable");
		GameRegistry.registerBlock(this.mod.blockChemicalCombinationTable, "BlockChemicalCombinationTable");
		// GameRegistry.registerBlock(this.mod.blockToolAndWeaponCraftingTable, "BlockToolAndWeaponCraftingTable");
		GameRegistry.registerBlock(this.mod.blockChemicalCraftingTable, "BlockMaterialCraftingTable");
		GameRegistry.registerBlock(this.mod.blockElectrolysisTable, "BlockElectrolysisTable");

		//名前登録
		this.mod.nameAuxiliary.addName(this.mod.blockPyrolysisTable, "PyrolysisTable");
		this.mod.nameAuxiliary.addName(this.mod.blockPyrolysisTable, "ja_JP", "熱分解台");
		this.mod.nameAuxiliary.addName(this.mod.blockChemicalCombinationTable, "ChemicalCombinationTable");
		this.mod.nameAuxiliary.addName(this.mod.blockChemicalCombinationTable, "ja_JP", "化合台");
		// this.mod.nameAuxiliary.addName(this.mod.blockToolAndWeaponCraftingTable, "ToolAndWeaponCraftingTable");
		// this.mod.nameAuxiliary.addName(this.mod.blockToolAndWeaponCraftingTable, "ja_JP", "ツール&武器製作台");
		this.mod.nameAuxiliary.addName(this.mod.blockChemicalCraftingTable, "ChemicalCraftingTable");
		this.mod.nameAuxiliary.addName(this.mod.blockChemicalCraftingTable, "ja_JP", "素材製作台");
		this.mod.nameAuxiliary.addName(this.mod.blockElectrolysisTable, "ElectrolysisTable");
		this.mod.nameAuxiliary.addName(this.mod.blockElectrolysisTable, "ja_JP", "電気分解台");

	}

}
