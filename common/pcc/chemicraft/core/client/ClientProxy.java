package pcc.chemicraft.core.client;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.core.container.ContainerElectrolysisTable;
import pcc.chemicraft.core.debug.DebugTick;
import pcc.chemicraft.core.gui.GuiChemicalCombinationTable;
import pcc.chemicraft.core.gui.GuiChemicalCraftingTable;
import pcc.chemicraft.core.gui.GuiElectrolysisTable;
import pcc.chemicraft.core.gui.GuiPyrolysisTable;
import pcc.chemicraft.core.gui.GuiToolAndWeaponCraftingTable;
import pcc.chemicraft.core.system.CommonProxy;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerRenderInformation() {
		MinecraftForgeClient.preloadTexture(ChemiCraft.GUI_PYROLYSIS_TEXTURE);
		MinecraftForgeClient.preloadTexture(ChemiCraft.GUI_ELECTROLYSIS_TEXTURE);
		MinecraftForgeClient.preloadTexture(ChemiCraft.GUI_CHEMICALCOMBINATION_TEXTURE);
		MinecraftForgeClient.preloadTexture(ChemiCraft.GUI_CHEMICALCRAFTING_TEXTURE);
		MinecraftForgeClient.preloadTexture(ChemiCraft.GUI_TOOLANDWEAPONCRAFTING_TEXTURE);
		MinecraftForgeClient.preloadTexture(ChemiCraft.ENTITY_PARTICLE_TEXRURE);
	}


	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (!world.blockExists(x, y, z))
			return null;

		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityPyrolysisTable) {
			return new GuiPyrolysisTable(player, (TileEntityPyrolysisTable) tileEntity);
		}else if(tileEntity instanceof TileEntityElectrolysisTable){
			return new GuiElectrolysisTable(player, (TileEntityElectrolysisTable) tileEntity);
		}else if(tileEntity instanceof TileEntityChemicalCombinationTable){
			return new GuiChemicalCombinationTable(player, (TileEntityChemicalCombinationTable) tileEntity);
		}else if(tileEntity instanceof TileEntityToolAndWeaponCraftingTable){
			return new GuiToolAndWeaponCraftingTable(player, (TileEntityToolAndWeaponCraftingTable) tileEntity);
		}else if(tileEntity instanceof TileEntityChemicalCraftingTable){
			return new GuiChemicalCraftingTable(player, (TileEntityChemicalCraftingTable) tileEntity);
		} else if (tileEntity instanceof TileEntityElectrolysisTable) {
			return new ContainerElectrolysisTable(player, (TileEntityElectrolysisTable) tileEntity);
		}
		return null;
	}


	@Override
	public World getClientWorld() {
		return FMLClientHandler.instance().getClient().theWorld;
	}

	@Override
	public void registerTickHandler() {
		TickRegistry.registerTickHandler(new DebugTick(), Side.CLIENT);
	}

}
