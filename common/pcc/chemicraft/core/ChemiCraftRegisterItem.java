package pcc.chemicraft.core;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import pcc.chemicraft.ChemiCraft;
import pcc.chemicraft.core.item.ItemAtoms;
import pcc.chemicraft.core.item.ItemChemicalCell;
import pcc.chemicraft.core.item.ItemCompounds;
import pcc.chemicraft.core.item.ItemGasCollectingBottle;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * アイテムを追加します
 * @author mozipi,ponkotate
 */
public class ChemiCraftRegisterItem extends ChemiCraftCoreRegister {

	public ChemiCraftRegisterItem(ChemiCraftCore mod) {
		super(mod);
	}

	@Override
	public void start() {
		//Itemを変数に代入
		this.mod.itemAtoms = new ItemAtoms(this.mod.atomsID).setUnlocalizedName(ChemiCraft.TEXTURE + "atoms");
		this.mod.itemCompounds = new ItemCompounds(this.mod.compoundsID).setUnlocalizedName("compounds");
		this.mod.itemGasCollectingBottle = new ItemGasCollectingBottle(this.mod.gasCollectingBottleID).setUnlocalizedName("gasCollectingBottle");
		this.mod.itemChemicalCells = new ItemChemicalCell(this.mod.chemicalCellsID).setUnlocalizedName("chemicalCell");
		this.mod.itemPear = new ItemFood(this.mod.pearID, 4, 0.4F, false){
			@Override
			@SideOnly(Side.CLIENT)
			public void updateIcons(IconRegister par1IconRegister) {
				this.iconIndex = par1IconRegister.registerIcon("appleGold");
			}
			@Override
			public int getColorFromItemStack(ItemStack par1ItemStack,int par2){
				return 0x66FF66;
				}
			}.setCreativeTab(CreativeTabs.tabFood).setUnlocalizedName(ChemiCraft.TEXTURE + "Pear");

		//名前登録&Minecraftに登録
		this.mod.nameAuxiliary.addName(this.mod.itemAtoms, ChemiCraftCore.ATOMSNAME);
		this.mod.nameAuxiliary.addName(this.mod.itemAtoms, "ja_JP", ChemiCraftCore.ATOMSNAMEJP);
		this.mod.nameAuxiliary.addName(this.mod.itemGasCollectingBottle, "GasCollectingBottle");
		this.mod.nameAuxiliary.addName(this.mod.itemGasCollectingBottle, "ja_JP", "集気瓶");
		this.mod.nameAuxiliary.addName(this.mod.itemChemicalCells, "ChemicalCell");
		this.mod.nameAuxiliary.addName(this.mod.itemChemicalCells, "ja_JP",  "化学電池");
		this.mod.nameAuxiliary.addName(this.mod.itemPear, "Pear");
		this.mod.nameAuxiliary.addName(this.mod.itemPear, "ja_JP",  "梨");

	}

}
