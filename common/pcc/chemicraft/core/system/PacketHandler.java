package pcc.chemicraft.core.system;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

/**
 * サーバーとクライアントのデータの同期に必要なクラス
 *
 * @author Lilly
 *
 */
public class PacketHandler implements IPacketHandler {

	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
		if (packet.channel.equals("chemicraftcore")) {
			ByteArrayDataInput badi = ByteStreams.newDataInput(packet.data);

			World worldClient;
			World worldServer;
			TileEntity var1;

			// TileEntityのx, y, z座標
			int x, y, z;

			// 座標読み込み
			x = badi.readInt();
			y = badi.readInt();
			z = badi.readInt();

			worldClient = ChemiCraftCore.proxy.getClientWorld();
			worldServer = ((EntityPlayer)player).worldObj;
			if(worldClient != null && worldServer == null){
				var1 = worldClient.getBlockTileEntity(x, y, z);
				if(var1 instanceof TileEntityChemicalCombinationTable) ((TileEntityChemicalCombinationTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityChemicalCraftingTable) ((TileEntityChemicalCraftingTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityPyrolysisTable) ((TileEntityPyrolysisTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityToolAndWeaponCraftingTable) ((TileEntityToolAndWeaponCraftingTable)var1).readPacket(badi);
				return;
			}
			if(worldServer != null){
				var1 = worldServer.getBlockTileEntity(x, y, z);
				if(var1 instanceof TileEntityChemicalCombinationTable) ((TileEntityChemicalCombinationTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityChemicalCraftingTable) ((TileEntityChemicalCraftingTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityPyrolysisTable) ((TileEntityPyrolysisTable)var1).readPacket(badi);
				if(var1 instanceof TileEntityToolAndWeaponCraftingTable) ((TileEntityToolAndWeaponCraftingTable)var1).readPacket(badi);
			}
			return;
		}
	}


	public static Packet getPacket(TileEntityPyrolysisTable tileEntity) {
		ByteArrayOutputStream var1 = new ByteArrayOutputStream(128);
		DataOutputStream var2 = new DataOutputStream(var1);

		// Blockのx, y, z座標
		int x, y, z;

		// x, y, z座標を代入
		x = tileEntity.xCoord;
		y = tileEntity.yCoord;
		z = tileEntity.zCoord;

		try {
			// 座標書き込み
			var2.writeInt(x);
			var2.writeInt(y);
			var2.writeInt(z);
			tileEntity.writePacket(var2);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// パケットの作成
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "chemicraftcore";
		packet.data = var1.toByteArray();
		packet.length = var1.size();
		packet.isChunkDataPacket = true;
		return packet;
	}

	public static Packet getPacket(TileEntityChemicalCombinationTable tileEntity) {
		ByteArrayOutputStream var1 = new ByteArrayOutputStream(128);
		DataOutputStream var2 = new DataOutputStream(var1);

		// Blockのx, y, z座標
		int x, y, z;

		// x, y, z座標を代入
		x = tileEntity.xCoord;
		y = tileEntity.yCoord;
		z = tileEntity.zCoord;

		try {
			// 座標書き込み
			var2.writeInt(x);
			var2.writeInt(y);
			var2.writeInt(z);
			tileEntity.writePacket(var2);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// パケットの作成
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "chemicraftcore";
		packet.data = var1.toByteArray();
		packet.length = var1.size();
		packet.isChunkDataPacket = true;
		return packet;
	}

	public static Packet getPacket(TileEntityToolAndWeaponCraftingTable tileEntity) {
		ByteArrayOutputStream var1 = new ByteArrayOutputStream(128);
		DataOutputStream var2 = new DataOutputStream(var1);

		// Blockのx, y, z座標
		int x, y, z;

		// x, y, z座標を代入
		x = tileEntity.xCoord;
		y = tileEntity.yCoord;
		z = tileEntity.zCoord;

		try {
			// 座標書き込み
			var2.writeInt(x);
			var2.writeInt(y);
			var2.writeInt(z);
			tileEntity.writePacket(var2);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// パケットの作成
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "chemicraftcore";
		packet.data = var1.toByteArray();
		packet.length = var1.size();
		packet.isChunkDataPacket = true;
		return packet;
	}

	public static Packet getPacket(TileEntityChemicalCraftingTable tileEntity) {
		ByteArrayOutputStream var1 = new ByteArrayOutputStream(128);
		DataOutputStream var2 = new DataOutputStream(var1);

		// Blockのx, y, z座標
		int x, y, z;

		// x, y, z座標を代入
		x = tileEntity.xCoord;
		y = tileEntity.yCoord;
		z = tileEntity.zCoord;

		try {
			// 座標書き込み
			var2.writeInt(x);
			var2.writeInt(y);
			var2.writeInt(z);
			tileEntity.writePacket(var2);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// パケットの作成
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "chemicraftcore";
		packet.data = var1.toByteArray();
		packet.length = var1.size();
		packet.isChunkDataPacket = true;
		return packet;
	}


	public static Packet getPacket(TileEntityElectrolysisTable tileEntity)
	{
		ByteArrayOutputStream var1 = new ByteArrayOutputStream(128);
		DataOutputStream var2 = new DataOutputStream(var1);

		// Blockのx, y, z座標
		int x, y, z;

		// x, y, z座標を代入
		x = tileEntity.xCoord;
		y = tileEntity.yCoord;
		z = tileEntity.zCoord;

		try {
			// 座標書き込み
			var2.writeInt(x);
			var2.writeInt(y);
			var2.writeInt(z);
			tileEntity.writePacket(var2);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// パケットの作成
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "chemicraftcore";
		packet.data = var1.toByteArray();
		packet.length = var1.size();
		packet.isChunkDataPacket = true;
		return packet;
	}

}
