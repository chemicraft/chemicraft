package pcc.chemicraft.core.system;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import pcc.chemicraft.core.container.ContainerChemicalCombinationTable;
import pcc.chemicraft.core.container.ContainerChemicalCraftingTable;
import pcc.chemicraft.core.container.ContainerElectrolysisTable;
import pcc.chemicraft.core.container.ContainerPyrolysisTable;
import pcc.chemicraft.core.container.ContainerToolAndWeaponCraftingTable;
import pcc.chemicraft.core.debug.DebugTick;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;
import pcc.chemicraft.core.tileentity.TileEntityElectrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

public class CommonProxy implements IGuiHandler {

	public void registerTextures(){

	}

	public void registerRenderInformation() {
	}


	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (!world.blockExists(x, y, z))
			return null;

		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityPyrolysisTable) {
			return new ContainerPyrolysisTable(player, (TileEntityPyrolysisTable) tileEntity);
		}else if(tileEntity instanceof TileEntityElectrolysisTable){
			return new ContainerElectrolysisTable(player, (TileEntityElectrolysisTable) tileEntity);
		}else if(tileEntity instanceof TileEntityChemicalCombinationTable){
			return new ContainerChemicalCombinationTable(player, (TileEntityChemicalCombinationTable) tileEntity);
		}else if(tileEntity instanceof TileEntityToolAndWeaponCraftingTable){
			return new ContainerToolAndWeaponCraftingTable(player, (TileEntityToolAndWeaponCraftingTable) tileEntity);
		}else if(tileEntity instanceof TileEntityChemicalCraftingTable){
			return new ContainerChemicalCraftingTable(player, (TileEntityChemicalCraftingTable) tileEntity);
		} else if (tileEntity instanceof TileEntityElectrolysisTable) {
			return new ContainerElectrolysisTable(player, (TileEntityElectrolysisTable) tileEntity);
		}
		return null;
	}


	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}

	public World getClientWorld(){
		return null;
	}

	public void registerTickHandler() {
		TickRegistry.registerTickHandler(new DebugTick(), Side.SERVER);
	}

}