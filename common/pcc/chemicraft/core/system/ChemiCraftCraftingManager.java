package pcc.chemicraft.core.system;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingMaterial;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingNBT;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingResult;
import pcc.chemicraft.core.inventory.InventoryToolAndWeaponCraftingTableMaterial;
import pcc.chemicraft.core.nbt.ChemicalNBTRecipe;
import pcc.chemicraft.util.ComparatorFormulaPart;
import pcc.chemicraft.util.ComparatorItemStack;
import pcc.chemicraft.util.FormulaPart;


public class ChemiCraftCraftingManager {

	public ItemStack getToolAndWeaponCraftingResult(InventoryToolAndWeaponCraftingTableMaterial par1IInventory){
		ChemiCraftAPI api = ChemiCraftAPI.instance();
		ArrayList<ItemStack[]> materials = api.getToolAndWeaponMaterials();
		ArrayList<ItemStack> result = api.getToolAndWeaponResult();
		ArrayList<Boolean> sharpless = api.getToolAndWeaponSharpless();

		ItemStack[] slotItems = new ItemStack[par1IInventory.getSizeInventory()];
		for (int i = 0; i < par1IInventory.getSizeInventory(); i++) {
			slotItems[i] = (par1IInventory.getStackInSlot(i));
		}

		label1 :
			for (int i = 0; i < materials.size(); i++) {
				if (sharpless.get(i)) {
					slotItems = ChemiCraftCore.instance.arrayAuxiliary.deleteNull(slotItems);
					Collections.sort(materials, new ComparatorItemStack());
					Arrays.sort(slotItems, new ComparatorItemStack());
					if (materials.size() != slotItems.length) continue label1;

					for (int j = 0; j < materials.size(); j++) {
						if (materials.get(i)[j].itemID != slotItems[j].itemID) {
							if (materials.get(i)[j].getItemDamage() != slotItems[j].getItemDamage()) {
								continue label1;
							}
						}
					}
					return result.get(i);
				} else {
					slotItems = ChemiCraftCore.instance.arrayAuxiliary.deleteNull(slotItems);
					if (materials.get(i).length != slotItems.length) continue label1;

					for (int j = 0; j < materials.size(); j++) {
						if (materials.get(i)[j].itemID != slotItems[j].itemID) {
							if (materials.get(i)[j].getItemDamage() != slotItems[j].getItemDamage()) {
								continue label1;
							}
						}
					}
					return result.get(i);
				}
			}
		return null;
	}



	public ItemStack getChemicalCombinationResult(ArrayList<String> atomsList, ArrayList<Integer> atomsAmountList){
		ChemiCraftAPI api = ChemiCraftAPI.instance();
		recipeSize :
			for (int i = 0; i < api.getChemicalCombinationAtoms().size(); i++) {
				FormulaPart[] var1 = new FormulaPart[atomsList.size()];
				FormulaPart[] var2 = new FormulaPart[api.getChemicalCombinationAtoms().get(i).length];
				for (int j = 0; j < atomsList.size(); j++) {
					var1[j] = new FormulaPart(atomsList.get(j), atomsAmountList.get(j));
				}
				for (int j = 0; j < api.getChemicalCombinationAtoms().get(i).length; j++) {
					var2[j] = new FormulaPart(api.getChemicalCombinationAtoms().get(i)[j], api.getChemicalCombinationAmounts().get(i)[j]);
				}
				Arrays.sort(var1, new ComparatorFormulaPart());
				Arrays.sort(var2, new ComparatorFormulaPart());
				if (var1.length != var2.length) {
					continue recipeSize;
				}
				for (int j = 0; j < var1.length; j++) {
					if (!var1[j].equals(var2[j])) {
						continue recipeSize;
					}
				}
				return api.getChemicalCombinationResult().get(i);
			}
		return null;
	}



	public ChemicalNBTRecipe chemicalCrafting(InventoryChemicalCraftingMaterial par1IInventory, InventoryChemicalCraftingResult par2IInventory, InventoryChemicalCraftingNBT par3IInventory){
		ChemiCraftAPI api = ChemiCraftAPI.instance();
		ChemicalNBTRecipe returnObj = null;
		for(int i = 0;i < api.getMaterialRecipe().size();i++){
			ItemStack result = api.getMaterialRecipe().get(i).match(par1IInventory);
			ItemStack resultArg = null;
			if(result != null){
				resultArg = new ItemStack(result.itemID, result.stackSize, result.getItemDamage());
				if(api.getMaterialRecipe().get(i).nbtMatch(par1IInventory) != null){
					ItemStack[] nbtInv = new ItemStack[par3IInventory.getSizeInventory()];
					for(int j = 0;j < par3IInventory.getSizeInventory();j++){
						nbtInv[j] = par3IInventory.getStackInSlot(j);
					}
					api.getMaterialRecipe().get(i).nbtMatch(par1IInventory).setNBT(nbtInv, resultArg);
				}
				returnObj = api.getMaterialRecipe().get(i).nbtMatch(par1IInventory);
				par2IInventory.setInventorySlotContents(0, resultArg);
				return returnObj;
			}
			par2IInventory.setInventorySlotContents(0, resultArg);
		}
		return returnObj;
	}



	private void clearResults(IInventory par1IInventory, int par2){
		/*
		 * Commented by mozipi.
		((InventoryDecomposition)par1IInventory).setFlag(false);
		 */
		for(int i = 0;i < par2;i++){
			par1IInventory.setInventorySlotContents(i, null);
		}
	}

}
