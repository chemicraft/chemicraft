package pcc.chemicraft.core.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.inventory.InventoryChemicalCombinationTableMaterial;
import pcc.chemicraft.core.inventory.InventoryChemicalCombinationTableResult;
import pcc.chemicraft.core.slot.SlotAtoms;
import pcc.chemicraft.core.slot.SlotResult;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCombinationTable;
import cpw.mods.fml.common.network.PacketDispatcher;

public class ContainerChemicalCombinationTable extends Container {

	/**
	 * the world.
	 */
	private World worldObj;

	/**
	 * the TileEntity of Position.
	 */
	private int posX;
	private int posY;
	private int posZ;

	/**
	 * the TileEntity.
	 */
	private TileEntityChemicalCombinationTable tileentity;

	/**
	 * the Inventory's.
	 */
	private InventoryChemicalCombinationTableMaterial invm;
	private InventoryChemicalCombinationTableResult invr;

	/**
	 * the EntityPlayer.
	 */
	private EntityPlayer entityplayer;

	public ContainerChemicalCombinationTable(EntityPlayer par1EntityPlayer, TileEntityChemicalCombinationTable par2) {
		super();
		this.entityplayer = par1EntityPlayer;
		this.tileentity = par2;
		this.worldObj = tileentity.worldObj;
		this.posX = tileentity.xCoord;
		this.posY = tileentity.yCoord;
		this.posZ = tileentity.zCoord;
		this.invm = this.tileentity.getInvMaterial();
		this.invr = this.tileentity.getInvResult();

		for(int i = 0;i < 5;i++){
			for(int j = 0;j < 3;j++){
				this.addSlotToContainer(new SlotAtoms(this.invm, i * 3 + j, i * 18 + 16, j * 18 + 15));
			}
		}
		this.addSlotToContainer(new SlotResult(invr, 0, 123, 34));


		int var3;
		for (var3 = 0; var3 < 3; ++var3)
		{
			for (int var4 = 0; var4 < 9; ++var4)
			{
				this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var4 + var3 * 9 + 9, 8 + var4 * 18, 121 + var3 * 18));
			}
		}

		for (var3 = 0; var3 < 9; ++var3)
		{
			this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var3, 8 + var3 * 18, 179));
		}

		this.invm.setEventHandler(this);
		this.onCraftMatrixChanged(invm);
	}



	@Override
	public void onCraftMatrixChanged(IInventory par1IInventory){
		this.invr.setInventorySlotContents(0, ChemiCraftAPI.instance().getCraftingManager().getChemicalCombinationResult(this.tileentity.getAtomsList(), this.tileentity.getAtomsAmountList()));
		if (par1IInventory instanceof InventoryChemicalCombinationTableResult) {
			PacketDispatcher.sendPacketToServer(this.tileentity.getDescriptionPacket());
		}
	}

	public void onCraftGuiClosed(EntityPlayer par1EntityPlayer)
	{
		super.onCraftGuiClosed(par1EntityPlayer);
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer){
		return this.worldObj.getBlockId(this.posX, this.posY, this.posZ) != ChemiCraftCore.instance.chemicalConbinationTableID ? false : par1EntityPlayer.getDistanceSq((double)this.posX + 0.5D, (double)this.posY + 0.5D, (double)this.posZ + 0.5D) <= 64.0D;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2) {
		ItemStack var3 = null;
		Slot var4 = (Slot)this.inventorySlots.get(par2);
		Slot var6 = (Slot)this.inventorySlots.get(0);

		if (var4 != null && var4.getHasStack()) {
			ItemStack var5 = var4.getStack();
			var3 = var5.copy();
			if (par2 >= 0 && par2 < 16) {
				if (!this.mergeItemStack(var5, 16, 52, false)) {
					return null;
				}
			} else if (par2 >= 16 && par2 < 53) {
				if (var6.isItemValid(var3)) {
					if (!this.mergeItemStack(var5, 0, 15, false)) {
						return null;
					}
				}
			} else if(par2 == 16) {
				if (!this.mergeItemStack(var5, 16, 52, false)) {
					return null;
				}
			}

			if (var5.stackSize == 0) {
				var4.putStack((ItemStack)null);
			} else {
				var4.onSlotChanged();
			}

			if (var5.stackSize == var3.stackSize) {
				return null;
			}

			var4.onPickupFromSlot(par1EntityPlayer, var5);
		}

		return var3;
	}

	@Override
	public ItemStack slotClick(int par1, int par2, int par3, EntityPlayer par4EntityPlayer) {
		if (par1 == 15) {
			if (this.invr.getStackInSlot(0) != null) {
				this.tileentity.emptytoAtomsList();
			}
		}
		return super.slotClick(par1, par2, par3, par4EntityPlayer);
	}

}
