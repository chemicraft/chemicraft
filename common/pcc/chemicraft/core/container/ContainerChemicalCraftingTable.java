package pcc.chemicraft.core.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftAPI;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingMaterial;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingNBT;
import pcc.chemicraft.core.inventory.InventoryChemicalCraftingResult;
import pcc.chemicraft.core.nbt.ChemicalNBTRecipe;
import pcc.chemicraft.core.slot.SlotAtom;
import pcc.chemicraft.core.slot.SlotResult;
import pcc.chemicraft.core.tileentity.TileEntityChemicalCraftingTable;

public class ContainerChemicalCraftingTable extends Container {

	/**
	 * Worldのインスタンス
	 */
	private World worldObj;


	/**
	 * BlockのX, Y, Z座標
	 */
	private int posX;
	private int posY;
	private int posZ;


	/**
	 * the TileEntity.
	 */
	private TileEntityChemicalCraftingTable tileEntity;


	private InventoryChemicalCraftingMaterial invm;
	private InventoryChemicalCraftingResult invr;
	private InventoryChemicalCraftingNBT invn;

	private ChemicalNBTRecipe useNBT;



	public ContainerChemicalCraftingTable(EntityPlayer par1EntityPlayer, TileEntityChemicalCraftingTable par2)
	{
		super();
		this.worldObj = par2.worldObj;
		this.posX = par2.xCoord;
		this.posY = par2.yCoord;
		this.posZ = par2.zCoord;
		this.tileEntity = par2;
		this.invm = (InventoryChemicalCraftingMaterial) this.tileEntity.chemicalCraftingInvMaterial;
		this.invr = (InventoryChemicalCraftingResult) this.tileEntity.chemicalCraftingInvResult;
		this.invn = (InventoryChemicalCraftingNBT) this.tileEntity.chemicalCraftingInvNBT;
		//GenerateInventory
		this.generateSlots(this.invm, this.invr, this.invn);
		//GeneratePlayerInventory
		int var3;

		for (var3 = 0; var3 < 3; ++var3)
		{
			for (int var4 = 0; var4 < 9; ++var4)
			{
				this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var4 + var3 * 9 + 9, 8 + var4 * 18, 85 + var3 * 18));
			}
		}

		for (var3 = 0; var3 < 9; ++var3)
		{
			this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var3, 8 + var3 * 18, 143));
		}

		this.invm.setEventHandler(this);
		this.invr.setEventHandler(this);
		this.invn.setEventHandler(this);
		this.onCraftMatrixChanged(invm);
	}



	@Override
	public void onCraftMatrixChanged(IInventory par1IInventory){
		this.useNBT = ChemiCraftAPI.instance().getCraftingManager().chemicalCrafting(this.invm, this.invr, this.invn);
	}

	@Override
	public void onCraftGuiClosed(EntityPlayer par1EntityPlayer)
	{
		super.onCraftGuiClosed(par1EntityPlayer);

		if (!this.worldObj.isRemote)
		{
			for (int i = 0; i < 9; ++i)
			{
				ItemStack itemstack = this.invm.getStackInSlotOnClosing(i);

				if (itemstack != null)
				{
					par1EntityPlayer.dropPlayerItem(itemstack);
				}
			}
		}
	}

	@Override
	public ItemStack slotClick(int par1, int par2, int par3, EntityPlayer par4EntityPlayer)
	{
		if(par1 == 12){
			if(this.invr.getStackInSlot(0) == null) return super.slotClick(par1, par2, par3, par4EntityPlayer);
			ItemStack var1 = par4EntityPlayer.inventory.getItemStack();
			if(var1 != null){
				if(var1.stackSize + this.invr.getStackInSlot(0).stackSize >= var1.getMaxStackSize()) return super.slotClick(par1, par2, par3, par4EntityPlayer);
			}
			for(int i = 0;i < this.invm.getSizeInventory();i++){
				if(this.invm.getStackInSlot(i) != null){
					if(this.invm.getStackInSlot(i).stackSize > 1){
						this.invm.getStackInSlot(i).stackSize--;
					}else{
						this.invm.setInventorySlotContents(i, null);
					}
				}
			}
			ItemStack[] matchNBT = new ItemStack[this.invn.getSizeInventory()];
			for(int i = 0;i < this.invn.getSizeInventory();i++){
				matchNBT[i] = this.invn.getStackInSlot(i);
			}
			if(this.useNBT == null) return super.slotClick(par1, par2, par3, par4EntityPlayer);
			ItemStack[] useItems = this.useNBT.getMatchItems(matchNBT);
			for(int i = 0;i < useItems.length;i++){
				if(useItems[i] != null){
					this.invn.setInventorySlotContents(i, null);
				}
			}
		}
		return super.slotClick(par1, par2, par3, par4EntityPlayer);
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer){
		return this.worldObj.getBlockId(this.posX, this.posY, this.posZ) != ChemiCraftCore.instance.chemicalCraftingTableID ? false : par1EntityPlayer.getDistanceSq((double)this.posX + 0.5D, (double)this.posY + 0.5D, (double)this.posZ + 0.5D) <= 64.0D;
	}



	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2) {
		ItemStack var3 = null;
		Slot var4 = (Slot)this.inventorySlots.get(par2);
		Slot var6 = (Slot)this.inventorySlots.get(0);

		if (var4 != null && var4.getHasStack()) {
			ItemStack var5 = var4.getStack();
			var3 = var5.copy();
			if (par2 >= 0 && par2 < 3) {
				if (!this.mergeItemStack(var5, 13,49, false)) {
					return null;
				}
			}
			if (par2 >= 3 && par2 < 13) {
				if (!this.mergeItemStack(var5, 13, 49, false)) {
					return null;
				}
			} else if (par2 >= 13 && par2 < 49) {
				if (var6.isItemValid(var3)) {
					if (!this.mergeItemStack(var5, 0, 2, false)) {
						return null;
					}
				} else if (!this.mergeItemStack(var5, 3, 13, false)) {
					return null;
				}
			}

			if (var5.stackSize == 0) {
				var4.putStack((ItemStack)null);
			} else {
				var4.onSlotChanged();
			}

			if (var5.stackSize == var3.stackSize) {
				return null;
			}

			var4.onPickupFromSlot(par1EntityPlayer, var5);
		}

		return var3;
	}



	private void generateSlots(IInventory materialInv, IInventory resultInv, IInventory nbtInv){
		for(int i = 0;i < 3;i++){
			this.addSlotToContainer(new SlotAtom(invn, i, 30 + 18 * i, 7));
		}
		for(int i = 0;i < 3;i++){
			for(int j = 0;j < 3;j++){
				this.addSlotToContainer(new Slot(invm, i * 3 + j, 30 + 18 * j, 27 + 18 * i));
			}
		}
		this.addSlotToContainer(new SlotResult(invr, 0, 123, 33));
	}
}
