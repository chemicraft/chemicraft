package pcc.chemicraft.core.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import pcc.chemicraft.core.ChemiCraftCore;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableFuel;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableMaterial;
import pcc.chemicraft.core.inventory.InventoryPyrolysisTableResult;
import pcc.chemicraft.core.slot.SlotPyrolysisTableFuel;
import pcc.chemicraft.core.slot.SlotResult;
import pcc.chemicraft.core.tileentity.TileEntityPyrolysisTable;


public class ContainerPyrolysisTable extends Container {

	private World worldObj;

	private int posX;
	private int posY;
	private int posZ;

	private TileEntityPyrolysisTable tileentity;

	private InventoryPyrolysisTableMaterial invm;
	private InventoryPyrolysisTableResult invr;
	private InventoryPyrolysisTableFuel invf;

	public ContainerPyrolysisTable(EntityPlayer par1EntityPlayer, TileEntityPyrolysisTable par2){
		super();
		this.tileentity = par2;
		this.worldObj = par2.worldObj;
		this.posX = par2.xCoord;
		this.posY = par2.yCoord;
		this.posZ = par2.zCoord;
		this.invm = par2.getInvMaterial();
		this.invr = par2.getInvResult();
		this.invf = par2.getInvFuel();
		this.invm.setEventHandler(this);
		this.invr.setEventHandler(this);
		this.invf.setEventHandler(this);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				this.addSlotToContainer(new SlotResult(this.tileentity.getInvResult(), i*4 + j, 88+i*18, 17+j*18));
			}
		}
		this.addSlotToContainer(new Slot(this.tileentity.getInvMaterial(), 0, 27, 20));
		this.addSlotToContainer(new SlotPyrolysisTableFuel(this.tileentity.getInvFuel(), 0, 27, 63));

		int var3;
		for (var3 = 0; var3 < 3; ++var3)
		{
			for (int var4 = 0; var4 < 9; ++var4)
			{
				this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var4 + var3 * 9 + 9, 8 + var4 * 18, 121 + var3 * 18));
			}
		}

		for (var3 = 0; var3 < 9; ++var3)
		{
			this.addSlotToContainer(new Slot(par1EntityPlayer.inventory, var3, 8 + var3 * 18, 179));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer par1EntityPlayer) {
		return this.worldObj.getBlockId(this.posX, this.posY, this.posZ) != ChemiCraftCore.instance.pyrolysisTableID ? false : par1EntityPlayer.getDistanceSq((double)this.posX + 0.5D, (double)this.posY + 0.5D, (double)this.posZ + 0.5D) <= 64.0D;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack var3 = null;
		Slot var4 = (Slot)this.inventorySlots.get(par2);

		if (var4 != null && var4.getHasStack())
		{
			ItemStack var5 = var4.getStack();
			var3 = var5.copy();

			if (par2 < 18 ){
				if (!this.mergeItemStack(var5, 18, this.inventorySlots.size(), true))
				{
					return null;
				}
			}
			else if (!this.mergeItemStack(var5, 16, 18, false))
			{
				return null;
			}

			if (var5.stackSize == 0)
			{
				var4.putStack((ItemStack)null);
			}
			else
			{
				var4.onSlotChanged();
			}
		}

		return var3;
	}

}
