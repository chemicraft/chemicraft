package pcc.chemicraft.core.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import pcc.chemicraft.core.tileentity.TileEntityToolAndWeaponCraftingTable;

public class ContainerToolAndWeaponCraftingTable extends Container {

	public ContainerToolAndWeaponCraftingTable(EntityPlayer par1EntityPlayer, TileEntityToolAndWeaponCraftingTable par2){
		super();
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1) {
		return false;
	}

}
