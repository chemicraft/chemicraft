package pcc.chemicraft.core.slot;

import java.util.Iterator;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import pcc.chemicraft.core.ChemiCraftAPI;

public class SlotElectrolysisFuel extends Slot
{

	public SlotElectrolysisFuel(IInventory par1iInventory, int par2, int par3, int par4) {
		super(par1iInventory, par2, par3, par4);
	}

	@Override
	public boolean isItemValid(ItemStack par1ItemStack) {
		Iterator<ItemStack> itFuel = ChemiCraftAPI.getElectrolysisFuelList().keySet().iterator();
		while (itFuel.hasNext()) {
			ItemStack fuel = itFuel.next();
			if (par1ItemStack != null && par1ItemStack.itemID == fuel.itemID) {
				return true;
			}
		}
		return false;
	}

}
