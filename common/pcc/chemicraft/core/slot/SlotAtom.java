package pcc.chemicraft.core.slot;

import net.minecraft.inventory.IInventory;

public class SlotAtom extends SlotAtoms {

	public SlotAtom(IInventory par1iInventory, int par2, int par3, int par4) {
		super(par1iInventory, par2, par3, par4);
	}

	@Override
	public int getSlotStackLimit() {
		return 1;
	}

}
