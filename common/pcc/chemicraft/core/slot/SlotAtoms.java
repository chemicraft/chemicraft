package pcc.chemicraft.core.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import pcc.chemicraft.core.ChemiCraftCore;


public class SlotAtoms extends Slot {

	public SlotAtoms(IInventory par1iInventory, int par2, int par3, int par4) {
		super(par1iInventory, par2, par3, par4);
	}

	@Override
	public boolean isItemValid(ItemStack par1ItemStack){
		return par1ItemStack == null ? false : par1ItemStack.itemID == ChemiCraftCore.instance.atomsID + 256;
	}

}
