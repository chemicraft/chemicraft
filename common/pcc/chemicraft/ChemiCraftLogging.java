package pcc.chemicraft;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class ChemiCraftLogging {

	private FileWriter outStream;
	private String directoryPath;
	private File file;

	public ChemiCraftLogging(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	public void startLogging() {
		this.file = new File(this.directoryPath + "/ChemiCraft.log");
		if (!this.file.exists()) {
			try {
				this.file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			this.outStream = new FileWriter(this.file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String writeStr) {
		String s = writeStr;
		try {
			this.outStream.write(writeStr);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String writeStr, EnumLoggingType type) {
		String s = writeStr;
		switch (type) {
		case NORMAL:
			break;
		case ERROR:
			s = "[Error]" + s;
			break;
		case WARNING:
			s = "[Warning]" + s;
			break;
		case INFO:
			s = "[Info]" + s;
			break;
		default:
			throw new IllegalStateException();
		}
		try {
			this.outStream.write(s + "\n");
			this.outStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
